$(function() {
        "use strict";
        var t, i = window.console || {
                log: function() {}
            },
            e = window.URL || window.webkitURL,
            a = $("#image"),
            o = $("#dataX"),
            n = $("#dataY"),
            h = $("#dataHeight"),
            s = $("#dataWidth"),
            r = $("#dataRotate"),
            d = $("#dataScaleX"),
            c = $("#dataScaleY"),
            l = {
                aspectRatio: 1/1,
                preview: ".img-preview",
        viewMode: 1,
        dragMode: 'move',
        autoCropArea: 1,
        restore: false,
        modal: false,
        guides: false,
        highlight: false,
        cropBoxMovable: false,
        cropBoxResizable: false,
                crop: function(t) {
                    o.val(Math.round(t.x)), n.val(Math.round(t.y)), h.val(Math.round(t.height)), s.val(Math.round(t.width)), r.val(t.rotate), d.val(t.scaleX), c.val(t.scaleY)
                }
            },
            p = a.attr("src"),
            g = "image/jpeg";
  //   $("input:file").change(function() {
  // });
        $('[data-toggle="tooltip"]').tooltip(), a.on({
            ready: function(t) {
                i.log(t.type)
            },
            cropstart: function(t) {
                i.log(t.type, t.action)
            },
            cropmove: function(t) {
                i.log(t.type, t.action), $("#reswidth").val(t.width), $("#resheight").val(t.height), $("#x_axis").val(t.x), $("#y_axis").val(t.y)
            },
            cropend: function(t) {
                i.log(t.type, t.action)
            },
            crop: function(t) {
                i.log(t.type, t.x, t.y, t.width, t.height, t.rotate, t.scaleX, t.scaleY), $("#reswidth").val(t.width), $("#resheight").val(t.height), $("#x_axis").val(t.x), $("#y_axis").val(t.y), $("#upfotoga").val(1)
            },
            zoom: function(t) {
                i.log(t.type, t.ratio)
            }
        }).cropper(l), $.isFunction(document.createElement("canvas").getContext) || $('button[data-method="getCroppedCanvas"]').prop("disabled", !0), void 0 === document.createElement("cropper").style.transition && ($('button[data-method="rotate"]').prop("disabled", !0), $('button[data-method="scale"]').prop("disabled", !0)), $(".docs-toggles").on("change", "input", function() {
            var t, i, e = $(this),
                o = e.attr("name"),
                n = e.prop("type");
            a.data("cropper") && ("checkbox" === n ? (l[o] = e.prop("checked"), t = a.cropper("getCropBoxData"), i = a.cropper("getCanvasData"), l.ready = function() {
                a.cropper("setCropBoxData", t), a.cropper("setCanvasData", i)
            }) : "radio" === n && (l[o] = e.val()), a.cropper("destroy").cropper(l))
        }), $(".docs-buttons").on("click", "[data-method]", function() {
            var o, n, h, s = $(this),
                r = s.data(),
                d = a.data("cropper");
            if (!s.prop("disabled") && !s.hasClass("disabled") && d && r.method) {
                if (void 0 !== (r = $.extend({}, r)).target && (n = $(r.target), void 0 === r.option)) try {
                    r.option = JSON.parse(n.val())
                } catch (t) {
                    i.log(t.message)
                }
                switch (o = d.cropped, r.method) {
                    case "rotate":
                        o && l.viewMode > 0 && a.cropper("clear");
                        break;
                    case "getCroppedCanvas":
                        "image/jpeg" === g && (r.option || (r.option = {}), r.option.fillColor = "#fff")
                }
                switch (h = a.cropper(r.method, r.option, r.secondOption), r.method) {
                    case "rotate":
                        o && l.viewMode > 0 && a.cropper("crop");
                        break;
                    case "scaleX":
                    case "scaleY":
                        $(this).data("option", -r.option);
                        break;
                    case "getCroppedCanvas":
                        h && $("#image_preview").html(h);
                        break;
                    case "destroy":
                        t && (e.revokeObjectURL(t), t = "", a.attr("src", p))
                }
                if ($.isPlainObject(h) && n) try {
                    n.val(JSON.stringify(h))
                } catch (t) {
                    i.log(t.message)
                }
            }
        }), $(document.body).on("keydown", function(t) {
            // if (a.data("cropper") && !(this.scrollTop > 300)) switch (t.which) {
            //     case 37:
            //         t.preventDefault(), a.cropper("move", -1, 0);
            //         break;
            //     case 38:
            //         t.preventDefault(), a.cropper("move", 0, -1);
            //         break;
            //     case 39:
            //         t.preventDefault(), a.cropper("move", 1, 0);
            //         break;
            //     case 40:
            //         t.preventDefault(), a.cropper("move", 0, 1)
            // }
        });
        var m = $("#inputImage");
        e ? m.change(function() {
            var T=new FileReader;T.readAsDataURL(this.files[0]);this.files;$("#nofoto").attr("hidden","hidden"),$("#btn-modalFoto").removeAttr("disabled"),a.data("cropper")&&(T.onload=function(T){a.cropper("destroy").attr("src",this.result).cropper(options),console.log(this.result)})
        }): m.prop("disabled", !0).parent().addClass("disabled")
    }),
    function(t, i) {
        "object" == typeof exports && "undefined" != typeof module ? i(require("jquery")) : "function" == typeof define && define.amd ? define(["jquery"], i) : i(t.jQuery)
    }(this, function(t) {
        "use strict";
        t = t && t.hasOwnProperty("default") ? t.default : t;
        var i = "undefined" != typeof window ? window : {},
            e = "cropper",
            a = "all",
            o = "crop",
            n = "move",
            h = "zoom",
            s = "e",
            r = "w",
            d = "s",
            c = "n",
            l = "ne",
            p = "nw",
            g = "se",
            m = "sw",
            u = e + "-crop",
            f = e + "-disabled",
            v = e + "-hidden",
            w = e + "-hide",
            x = e + "-modal",
            b = e + "-move",
            y = "action",
            C = "preview",
            M = "crop",
            $ = "crop",
            k = "cropend",
            B = "cropmove",
            D = "cropstart",
            W = "dblclick",
            Y = "load",
            T = i.PointerEvent ? "pointerdown" : "touchstart mousedown",
            X = i.PointerEvent ? "pointermove" : "touchmove mousemove",
            H = i.PointerEvent ? "pointerup pointercancel" : "touchend touchcancel mouseup",
            O = "wheel mousewheel DOMMouseScroll",
            R = "zoom",
            E = /^(e|w|s|n|se|sw|ne|nw|all|crop|move|zoom)$/,
            N = /^data:/,
            L = /^data:image\/jpeg;base64,/,
            z = /^(img|canvas)$/i,
            P = {
                viewMode: 0,
                dragMode: M,
                aspectRatio: NaN,
                data: null,
                preview: "",
                responsive: !0,
                restore: !0,
                checkCrossOrigin: !0,
                checkOrientation: !0,
                modal: !0,
                guides: !0,
                center: !0,
                highlight: !0,
                background: !0,
                autoCrop: !0,
                autoCropArea: .8,
                movable: !0,
                rotatable: !0,
                scalable: !0,
                zoomable: !0,
                zoomOnTouch: !0,
                zoomOnWheel: !0,
                wheelZoomRatio: .1,
                cropBoxMovable: !0,
                cropBoxResizable: !0,
                toggleDragModeOnDblclick: !0,
                minCanvasWidth: 0,
                minCanvasHeight: 0,
                minCropBoxWidth: 0,
                minCropBoxHeight: 0,
                minContainerWidth: 200,
                minContainerHeight: 100,
                ready: null,
                cropstart: null,
                cropmove: null,
                cropend: null,
                crop: null,
                zoom: null
            },
            U = function(t, i) {
                if (!(t instanceof i)) throw new TypeError("Cannot call a class as a function")
            },
            I = function() {
                function t(t, i) {
                    for (var e = 0; e < i.length; e++) {
                        var a = i[e];
                        a.enumerable = a.enumerable || !1, a.configurable = !0, "value" in a && (a.writable = !0), Object.defineProperty(t, a.key, a)
                    }
                }
                return function(i, e, a) {
                    return e && t(i.prototype, e), a && t(i, a), i
                }
            }(),
            j = function(t) {
                if (Array.isArray(t)) {
                    for (var i = 0, e = Array(t.length); i < t.length; i++) e[i] = t[i];
                    return e
                }
                return Array.from(t)
            };
        var A = Number.isNaN || i.isNaN;

        function F(t) {
            return "number" == typeof t && !A(t)
        }

        function S(t) {
            return void 0 === t
        }

        function q(t, i) {
            for (var e = arguments.length, a = Array(e > 2 ? e - 2 : 0), o = 2; o < e; o++) a[o - 2] = arguments[o];
            return function() {
                for (var e = arguments.length, o = Array(e), n = 0; n < e; n++) o[n] = arguments[n];
                return t.apply(i, a.concat(o))
            }
        }
        var Q = Object.keys || function(i) {
                var e = [];
                return t.each(i, function(t) {
                    e.push(t)
                }), e
            },
            _ = /\.\d*(?:0|9){12}\d*$/i;

        function J(t) {
            var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 1e11;
            return _.test(t) ? Math.round(t * i) / i : t
        }
        var K = i.location,
            Z = /^(https?:)\/\/([^:/?#]+):?(\d*)/i;

        function V(t) {
            var i = t.match(Z);
            return i && (i[1] !== K.protocol || i[2] !== K.hostname || i[3] !== K.port)
        }

        function G(t) {
            var i = "timestamp=" + (new Date).getTime();
            return t + (-1 === t.indexOf("?") ? "?" : "&") + i
        }

        function tt(t) {
            var i = t.rotate,
                e = t.scaleX,
                a = t.scaleY,
                o = t.translateX,
                n = t.translateY,
                h = [];
            return F(o) && 0 !== o && h.push("translateX(" + o + "px)"), F(n) && 0 !== n && h.push("translateY(" + n + "px)"), F(i) && 0 !== i && h.push("rotate(" + i + "deg)"), F(e) && 1 !== e && h.push("scaleX(" + e + ")"), F(a) && 1 !== a && h.push("scaleY(" + a + ")"), h.length ? h.join(" ") : "none"
        }
        var it = i.navigator,
            et = it && /(Macintosh|iPhone|iPod|iPad).*AppleWebKit/i.test(it.userAgent);

        function at(i, e) {
            var a = i.pageX,
                o = i.pageY,
                n = {
                    endX: a,
                    endY: o
                };
            return e ? n : t.extend({
                startX: a,
                startY: o
            }, n)
        }
        var ot = Number.isFinite || i.isFinite;

        function nt(t) {
            var i = t.aspectRatio,
                e = t.height,
                a = t.width,
                o = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : "contain",
                n = function(t) {
                    return ot(t) && t > 0
                };
            if (n(a) && n(e)) {
                var h = e * i;
                "contain" === o && h > a || "cover" === o && h < a ? e = a / i : a = e * i
            } else n(a) ? e = a / i : n(e) && (a = e * i);
            return {
                width: a,
                height: e
            }
        }
        var ht = String.fromCharCode;
        var st = /^data:.*,/;

        function rt(t) {
            var i = new DataView(t),
                e = void 0,
                a = void 0,
                o = void 0,
                n = void 0;
            if (255 === i.getUint8(0) && 216 === i.getUint8(1))
                for (var h = i.byteLength, s = 2; s < h;) {
                    if (255 === i.getUint8(s) && 225 === i.getUint8(s + 1)) {
                        o = s;
                        break
                    }
                    s += 1
                }
            if (o) {
                var r = o + 10;
                if ("Exif" === function(t, i, e) {
                        var a = "",
                            o = void 0;
                        for (e += i, o = i; o < e; o += 1) a += ht(t.getUint8(o));
                        return a
                    }(i, o + 4, 4)) {
                    var d = i.getUint16(r);
                    if (((a = 18761 === d) || 19789 === d) && 42 === i.getUint16(r + 2, a)) {
                        var c = i.getUint32(r + 4, a);
                        c >= 8 && (n = r + c)
                    }
                }
            }
            if (n) {
                var l = i.getUint16(n, a),
                    p = void 0,
                    g = void 0;
                for (g = 0; g < l; g += 1)
                    if (p = n + 12 * g + 2, 274 === i.getUint16(p, a)) {
                        p += 8, e = i.getUint16(p, a), i.setUint16(p, 1, a);
                        break
                    }
            }
            return e
        }
        var dt = {
                render: function() {
                    this.initContainer(), this.initCanvas(), this.initCropBox(), this.renderCanvas(), this.cropped && this.renderCropBox()
                },
                initContainer: function() {
                    var t = this.$element,
                        i = this.options,
                        e = this.$container,
                        a = this.$cropper;
                    a.addClass(v), t.removeClass(v), a.css(this.container = {
                        width: Math.max(e.width(), Number(i.minContainerWidth) || 200),
                        height: Math.max(e.height(), Number(i.minContainerHeight) || 100)
                    }), t.addClass(v), a.removeClass(v)
                },
                initCanvas: function() {
                    var i = this.container,
                        e = this.image,
                        a = this.options.viewMode,
                        o = Math.abs(e.rotate) % 180 == 90,
                        n = o ? e.naturalHeight : e.naturalWidth,
                        h = o ? e.naturalWidth : e.naturalHeight,
                        s = n / h,
                        r = i.width,
                        d = i.height;
                    i.height * s > i.width ? 3 === a ? r = i.height * s : d = i.width / s : 3 === a ? d = i.width / s : r = i.height * s;
                    var c = {
                        aspectRatio: s,
                        naturalWidth: n,
                        naturalHeight: h,
                        width: r,
                        height: d
                    };
                    c.left = (i.width - r) / 2, c.top = (i.height - d) / 2, c.oldLeft = c.left, c.oldTop = c.top, this.canvas = c, this.limited = 1 === a || 2 === a, this.limitCanvas(!0, !0), this.initialImage = t.extend({}, e), this.initialCanvas = t.extend({}, c)
                },
                limitCanvas: function(t, i) {
                    var e = this.options,
                        a = this.container,
                        o = this.canvas,
                        n = this.cropBox,
                        h = e.viewMode,
                        s = o.aspectRatio,
                        r = this.cropped && n;
                    if (t) {
                        var d = Number(e.minCanvasWidth) || 0,
                            c = Number(e.minCanvasHeight) || 0;
                        h > 0 && (h > 1 ? (d = Math.max(d, a.width), c = Math.max(c, a.height), 3 === h && (c * s > d ? d = c * s : c = d / s)) : d ? d = Math.max(d, r ? n.width : 0) : c ? c = Math.max(c, r ? n.height : 0) : r && (d = n.width, (c = n.height) * s > d ? d = c * s : c = d / s));
                        var l = nt({
                            aspectRatio: s,
                            width: d,
                            height: c
                        });
                        d = l.width, c = l.height, o.minWidth = d, o.minHeight = c, o.maxWidth = 1 / 0, o.maxHeight = 1 / 0
                    }
                    if (i)
                        if (h > 0) {
                            var p = a.width - o.width,
                                g = a.height - o.height;
                            o.minLeft = Math.min(0, p), o.minTop = Math.min(0, g), o.maxLeft = Math.max(0, p), o.maxTop = Math.max(0, g), r && this.limited && (o.minLeft = Math.min(n.left, n.left + n.width - o.width), o.minTop = Math.min(n.top, n.top + n.height - o.height), o.maxLeft = n.left, o.maxTop = n.top, 2 === h && (o.width >= a.width && (o.minLeft = Math.min(0, p), o.maxLeft = Math.max(0, p)), o.height >= a.height && (o.minTop = Math.min(0, g), o.maxTop = Math.max(0, g))))
                        } else o.minLeft = -o.width, o.minTop = -o.height, o.maxLeft = a.width, o.maxTop = a.height
                },
                renderCanvas: function(t, i) {
                    var e = this.canvas,
                        a = this.image;
                    if (i) {
                        var o = function(t) {
                                var i = t.width,
                                    e = t.height,
                                    a = t.degree;
                                if (90 == (a = Math.abs(a) % 180)) return {
                                    width: e,
                                    height: i
                                };
                                var o = a % 90 * Math.PI / 180,
                                    n = Math.sin(o),
                                    h = Math.cos(o),
                                    s = i * h + e * n,
                                    r = i * n + e * h;
                                return a > 90 ? {
                                    width: r,
                                    height: s
                                } : {
                                    width: s,
                                    height: r
                                }
                            }({
                                width: a.naturalWidth * Math.abs(a.scaleX || 1),
                                height: a.naturalHeight * Math.abs(a.scaleY || 1),
                                degree: a.rotate || 0
                            }),
                            n = o.width,
                            h = o.height,
                            s = e.width * (n / e.naturalWidth),
                            r = e.height * (h / e.naturalHeight);
                        e.left -= (s - e.width) / 2, e.top -= (r - e.height) / 2, e.width = s, e.height = r, e.aspectRatio = n / h, e.naturalWidth = n, e.naturalHeight = h, this.limitCanvas(!0, !1)
                    }(e.width > e.maxWidth || e.width < e.minWidth) && (e.left = e.oldLeft), (e.height > e.maxHeight || e.height < e.minHeight) && (e.top = e.oldTop), e.width = Math.min(Math.max(e.width, e.minWidth), e.maxWidth), e.height = Math.min(Math.max(e.height, e.minHeight), e.maxHeight), this.limitCanvas(!1, !0), e.left = Math.min(Math.max(e.left, e.minLeft), e.maxLeft), e.top = Math.min(Math.max(e.top, e.minTop), e.maxTop), e.oldLeft = e.left, e.oldTop = e.top, this.$canvas.css({
                        width: e.width,
                        height: e.height,
                        transform: tt({
                            translateX: e.left,
                            translateY: e.top
                        })
                    }), this.renderImage(t), this.cropped && this.limited && this.limitCropBox(!0, !0)
                },
                renderImage: function(i) {
                    var e = this.canvas,
                        a = this.image,
                        o = a.naturalWidth * (e.width / e.naturalWidth),
                        n = a.naturalHeight * (e.height / e.naturalHeight);
                    t.extend(a, {
                        width: o,
                        height: n,
                        left: (e.width - o) / 2,
                        top: (e.height - n) / 2
                    }), this.$clone.css({
                        width: a.width,
                        height: a.height,
                        transform: tt(t.extend({
                            translateX: a.left,
                            translateY: a.top
                        }, a))
                    }), i && this.output()
                },
                initCropBox: function() {
                    var i = this.options,
                        e = this.canvas,
                        a = i.aspectRatio,
                        o = Number(i.autoCropArea) || .8,
                        n = {
                            width: e.width,
                            height: e.height
                        };
                    a && (e.height * a > e.width ? n.height = n.width / a : n.width = n.height * a), this.cropBox = n, this.limitCropBox(!0, !0), n.width = Math.min(Math.max(n.width, n.minWidth), n.maxWidth), n.height = Math.min(Math.max(n.height, n.minHeight), n.maxHeight), n.width = Math.max(n.minWidth, n.width * o), n.height = Math.max(n.minHeight, n.height * o), n.left = e.left + (e.width - n.width) / 2, n.top = e.top + (e.height - n.height) / 2, n.oldLeft = n.left, n.oldTop = n.top, this.initialCropBox = t.extend({}, n)
                },
                limitCropBox: function(t, i) {
                    var e = this.options,
                        a = this.container,
                        o = this.canvas,
                        n = this.cropBox,
                        h = this.limited,
                        s = e.aspectRatio;
                    if (t) {
                        var r = Number(e.minCropBoxWidth) || 0,
                            d = Number(e.minCropBoxHeight) || 0,
                            c = Math.min(a.width, h ? o.width : a.width),
                            l = Math.min(a.height, h ? o.height : a.height);
                        r = Math.min(r, a.width), d = Math.min(d, a.height), s && (r && d ? d * s > r ? d = r / s : r = d * s : r ? d = r / s : d && (r = d * s), l * s > c ? l = c / s : c = l * s), n.minWidth = Math.min(r, c), n.minHeight = Math.min(d, l), n.maxWidth = c, n.maxHeight = l
                    }
                    i && (h ? (n.minLeft = Math.max(0, o.left), n.minTop = Math.max(0, o.top), n.maxLeft = Math.min(a.width, o.left + o.width) - n.width, n.maxTop = Math.min(a.height, o.top + o.height) - n.height) : (n.minLeft = 0, n.minTop = 0, n.maxLeft = a.width - n.width, n.maxTop = a.height - n.height))
                },
                renderCropBox: function() {
                    var t = this.options,
                        i = this.container,
                        e = this.cropBox;
                    (e.width > e.maxWidth || e.width < e.minWidth) && (e.left = e.oldLeft), (e.height > e.maxHeight || e.height < e.minHeight) && (e.top = e.oldTop), e.width = Math.min(Math.max(e.width, e.minWidth), e.maxWidth), e.height = Math.min(Math.max(e.height, e.minHeight), e.maxHeight), this.limitCropBox(!1, !0), e.left = Math.min(Math.max(e.left, e.minLeft), e.maxLeft), e.top = Math.min(Math.max(e.top, e.minTop), e.maxTop), e.oldLeft = e.left, e.oldTop = e.top, t.movable && t.cropBoxMovable && this.$face.data(y, e.width >= i.width && e.height >= i.height ? n : a), this.$cropBox.css({
                        width: e.width,
                        height: e.height,
                        transform: tt({
                            translateX: e.left,
                            translateY: e.top
                        })
                    }), this.cropped && this.limited && this.limitCanvas(!0, !0), this.disabled || this.output()
                },
                output: function() {
                    this.preview(), this.completed && this.trigger($, this.getData())
                }
            },
            ct = {
                initPreview: function() {
                    var i = this.crossOrigin,
                        e = i ? this.crossOriginUrl : this.url,
                        a = document.createElement("img");
                    i && (a.crossOrigin = i), a.src = e;
                    var o = t(a);
                    this.$preview = t(this.options.preview), this.$clone2 = o, this.$viewBox.html(o), this.$preview.each(function(a, o) {
                        var n = t(o),
                            h = document.createElement("img");
                        n.data(C, {
                            width: n.width(),
                            height: n.height(),
                            html: n.html()
                        }), i && (h.crossOrigin = i), h.src = e, h.style.cssText = 'display:block;width:100%;height:auto;min-width:0!important;min-height:0!important;max-width:none!important;max-height:none!important;image-orientation:0deg!important;"', n.html(h)
                    })
                },
                resetPreview: function() {
                    this.$preview.each(function(i, e) {
                        var a = t(e),
                            o = a.data(C);
                        a.css({
                            width: o.width,
                            height: o.height
                        }).html(o.html).removeData(C)
                    })
                },
                preview: function() {
                    var i = this.image,
                        e = this.canvas,
                        a = this.cropBox,
                        o = a.width,
                        n = a.height,
                        h = i.width,
                        s = i.height,
                        r = a.left - e.left - i.left,
                        d = a.top - e.top - i.top;
                    this.cropped && !this.disabled && (this.$clone2.css({
                        width: h,
                        height: s,
                        transform: tt(t.extend({
                            translateX: -r,
                            translateY: -d
                        }, i))
                    }), this.$preview.each(function(e, a) {
                        var c = t(a),
                            l = c.data(C),
                            p = l.width,
                            g = l.height,
                            m = p,
                            u = g,
                            f = 1;
                        o && (u = n * (f = p / o)), n && u > g && (m = o * (f = g / n), u = g), c.css({
                            width: m,
                            height: u
                        }).find("img").css({
                            width: h * f,
                            height: s * f,
                            transform: tt(t.extend({
                                translateX: -r * f,
                                translateY: -d * f
                            }, i))
                        })
                    }))
                }
            },
            lt = {
                bind: function() {
                    var i = this.$element,
                        e = this.options,
                        a = this.$cropper;
                    t.isFunction(e.cropstart) && i.on(D, e.cropstart), t.isFunction(e.cropmove) && i.on(B, e.cropmove), t.isFunction(e.cropend) && i.on(k, e.cropend), t.isFunction(e.crop) && i.on($, e.crop), t.isFunction(e.zoom) && i.on(R, e.zoom), a.on(T, q(this.cropStart, this)), e.zoomable && e.zoomOnWheel && a.on(O, q(this.wheel, this)), e.toggleDragModeOnDblclick && a.on(W, q(this.dblclick, this)), t(this.element.ownerDocument).on(X, this.onCropMove = q(this.cropMove, this)).on(H, this.onCropEnd = q(this.cropEnd, this)), e.responsive && t(window).on("resize", this.onResize = q(this.resize, this))
                },
                unbind: function() {
                    var i = this.$element,
                        e = this.options,
                        a = this.$cropper;
                    t.isFunction(e.cropstart) && i.off(D, e.cropstart), t.isFunction(e.cropmove) && i.off(B, e.cropmove), t.isFunction(e.cropend) && i.off(k, e.cropend), t.isFunction(e.crop) && i.off($, e.crop), t.isFunction(e.zoom) && i.off(R, e.zoom), a.off(T, this.cropStart), e.zoomable && e.zoomOnWheel && a.off(O, this.wheel), e.toggleDragModeOnDblclick && a.off(W, this.dblclick), t(this.element.ownerDocument).off(X, this.onCropMove).off(H, this.onCropEnd), e.responsive && t(window).off("resize", this.onResize)
                }
            },
            pt = {
                resize: function() {
                    var i = this.options,
                        e = this.$container,
                        a = this.container,
                        o = Number(i.minContainerWidth) || 200,
                        n = Number(i.minContainerHeight) || 100;
                    if (!(this.disabled || a.width <= o || a.height <= n)) {
                        var h = e.width() / a.width;
                        if (1 !== h || e.height() !== a.height) {
                            var s = void 0,
                                r = void 0;
                            i.restore && (s = this.getCanvasData(), r = this.getCropBoxData()), this.render(), i.restore && (this.setCanvasData(t.each(s, function(t, i) {
                                s[t] = i * h
                            })), this.setCropBoxData(t.each(r, function(t, i) {
                                r[t] = i * h
                            })))
                        }
                    }
                },
                dblclick: function() {
                    this.disabled || "none" === this.options.dragMode || this.setDragMode(this.$dragBox.hasClass(u) ? "move" : M)
                },
                wheel: function(t) {
                    var i = this,
                        e = t.originalEvent || t,
                        a = Number(this.options.wheelZoomRatio) || .1;
                    if (!this.disabled && (t.preventDefault(), !this.wheeling)) {
                        this.wheeling = !0, setTimeout(function() {
                            i.wheeling = !1
                        }, 50);
                        var o = 1;
                        e.deltaY ? o = e.deltaY > 0 ? 1 : -1 : e.wheelDelta ? o = -e.wheelDelta / 120 : e.detail && (o = e.detail > 0 ? 1 : -1), this.zoom(-o * a, t)
                    }
                },
                cropStart: function(i) {
                    if (!this.disabled) {
                        var e = this.options,
                            a = this.pointers,
                            n = i.originalEvent,
                            s = void 0;
                        n && n.changedTouches ? t.each(n.changedTouches, function(t, i) {
                            a[i.identifier] = at(i)
                        }) : a[n && n.pointerId || 0] = at(n || i), s = Q(a).length > 1 && e.zoomable && e.zoomOnTouch ? h : t(i.target).data(y), E.test(s) && (this.trigger(D, {
                            originalEvent: n,
                            action: s
                        }).isDefaultPrevented() || (i.preventDefault(), this.action = s, this.cropping = !1, s === o && (this.cropping = !0, this.$dragBox.addClass(x))))
                    }
                },
                cropMove: function(i) {
                    var e = this.action;
                    if (!this.disabled && e) {
                        var a = this.pointers,
                            o = i.originalEvent;
                        i.preventDefault(), this.trigger(B, {
                            originalEvent: o,
                            action: e
                        }).isDefaultPrevented() || (o && o.changedTouches ? t.each(o.changedTouches, function(i, e) {
                            t.extend(a[e.identifier], at(e, !0))
                        }) : t.extend(a[o && o.pointerId || 0], at(o || i, !0)), this.change(i))
                    }
                },
                cropEnd: function(i) {
                    if (!this.disabled) {
                        var e = this.action,
                            a = this.pointers,
                            o = i.originalEvent;
                        o && o.changedTouches ? t.each(o.changedTouches, function(t, i) {
                            delete a[i.identifier]
                        }) : delete a[o && o.pointerId || 0], e && (i.preventDefault(), Q(a).length || (this.action = ""), this.cropping && (this.cropping = !1, this.$dragBox.toggleClass(x, this.cropped && this.options.modal)), this.trigger(k, {
                            originalEvent: o,
                            action: e
                        }))
                    }
                }
            },
            gt = {
                change: function(i) {
                    var e = this.options,
                        u = this.pointers,
                        f = this.container,
                        w = this.canvas,
                        x = this.cropBox,
                        b = this.action,
                        y = e.aspectRatio,
                        C = x.left,
                        M = x.top,
                        $ = x.width,
                        k = x.height,
                        B = C + $,
                        D = M + k,
                        W = 0,
                        Y = 0,
                        T = f.width,
                        X = f.height,
                        H = !0,
                        O = void 0;
                    !y && i.shiftKey && (y = $ && k ? $ / k : 1), this.limited && (W = x.minLeft, Y = x.minTop, T = W + Math.min(f.width, w.width, w.left + w.width), X = Y + Math.min(f.height, w.height, w.top + w.height));
                    var R, E, N, L = u[Q(u)[0]],
                        z = {
                            x: L.endX - L.startX,
                            y: L.endY - L.startY
                        },
                        P = function(t) {
                            switch (t) {
                                case s:
                                    B + z.x > T && (z.x = T - B);
                                    break;
                                case r:
                                    C + z.x < W && (z.x = W - C);
                                    break;
                                case c:
                                    M + z.y < Y && (z.y = Y - M);
                                    break;
                                case d:
                                    D + z.y > X && (z.y = X - D)
                            }
                        };
                    switch (b) {
                        case a:
                            C += z.x, M += z.y;
                            break;
                        case s:
                            if (z.x >= 0 && (B >= T || y && (M <= Y || D >= X))) {
                                H = !1;
                                break
                            }
                            P(s), $ += z.x, y && (k = $ / y, M -= z.x / y / 2), $ < 0 && (b = r, $ = 0);
                            break;
                        case c:
                            if (z.y <= 0 && (M <= Y || y && (C <= W || B >= T))) {
                                H = !1;
                                break
                            }
                            P(c), k -= z.y, M += z.y, y && ($ = k * y, C += z.y * y / 2), k < 0 && (b = d, k = 0);
                            break;
                        case r:
                            if (z.x <= 0 && (C <= W || y && (M <= Y || D >= X))) {
                                H = !1;
                                break
                            }
                            P(r), $ -= z.x, C += z.x, y && (k = $ / y, M += z.x / y / 2), $ < 0 && (b = s, $ = 0);
                            break;
                        case d:
                            if (z.y >= 0 && (D >= X || y && (C <= W || B >= T))) {
                                H = !1;
                                break
                            }
                            P(d), k += z.y, y && ($ = k * y, C -= z.y * y / 2), k < 0 && (b = c, k = 0);
                            break;
                        case l:
                            if (y) {
                                if (z.y <= 0 && (M <= Y || B >= T)) {
                                    H = !1;
                                    break
                                }
                                P(c), k -= z.y, M += z.y, $ = k * y
                            } else P(c), P(s), z.x >= 0 ? B < T ? $ += z.x : z.y <= 0 && M <= Y && (H = !1) : $ += z.x, z.y <= 0 ? M > Y && (k -= z.y, M += z.y) : (k -= z.y, M += z.y);
                            $ < 0 && k < 0 ? (b = m, k = 0, $ = 0) : $ < 0 ? (b = p, $ = 0) : k < 0 && (b = g, k = 0);
                            break;
                        case p:
                            if (y) {
                                if (z.y <= 0 && (M <= Y || C <= W)) {
                                    H = !1;
                                    break
                                }
                                P(c), k -= z.y, M += z.y, $ = k * y, C += z.y * y
                            } else P(c), P(r), z.x <= 0 ? C > W ? ($ -= z.x, C += z.x) : z.y <= 0 && M <= Y && (H = !1) : ($ -= z.x, C += z.x), z.y <= 0 ? M > Y && (k -= z.y, M += z.y) : (k -= z.y, M += z.y);
                            $ < 0 && k < 0 ? (b = g, k = 0, $ = 0) : $ < 0 ? (b = l, $ = 0) : k < 0 && (b = m, k = 0);
                            break;
                        case m:
                            if (y) {
                                if (z.x <= 0 && (C <= W || D >= X)) {
                                    H = !1;
                                    break
                                }
                                P(r), $ -= z.x, C += z.x, k = $ / y
                            } else P(d), P(r), z.x <= 0 ? C > W ? ($ -= z.x, C += z.x) : z.y >= 0 && D >= X && (H = !1) : ($ -= z.x, C += z.x), z.y >= 0 ? D < X && (k += z.y) : k += z.y;
                            $ < 0 && k < 0 ? (b = l, k = 0, $ = 0) : $ < 0 ? (b = g, $ = 0) : k < 0 && (b = p, k = 0);
                            break;
                        case g:
                            if (y) {
                                if (z.x >= 0 && (B >= T || D >= X)) {
                                    H = !1;
                                    break
                                }
                                P(s), k = ($ += z.x) / y
                            } else P(d), P(s), z.x >= 0 ? B < T ? $ += z.x : z.y >= 0 && D >= X && (H = !1) : $ += z.x, z.y >= 0 ? D < X && (k += z.y) : k += z.y;
                            $ < 0 && k < 0 ? (b = p, k = 0, $ = 0) : $ < 0 ? (b = m, $ = 0) : k < 0 && (b = l, k = 0);
                            break;
                        case n:
                            this.move(z.x, z.y), H = !1;
                            break;
                        case h:
                            this.zoom((R = u, E = t.extend({}, R), N = [], t.each(R, function(i, e) {
                                delete E[i], t.each(E, function(t, i) {
                                    var a = Math.abs(e.startX - i.startX),
                                        o = Math.abs(e.startY - i.startY),
                                        n = Math.abs(e.endX - i.endX),
                                        h = Math.abs(e.endY - i.endY),
                                        s = Math.sqrt(a * a + o * o),
                                        r = (Math.sqrt(n * n + h * h) - s) / s;
                                    N.push(r)
                                })
                            }), N.sort(function(t, i) {
                                return Math.abs(t) < Math.abs(i)
                            }), N[0]), i.originalEvent), H = !1;
                            break;
                        case o:
                            if (!z.x || !z.y) {
                                H = !1;
                                break
                            }
                            O = this.$cropper.offset(), C = L.startX - O.left, M = L.startY - O.top, $ = x.minWidth, k = x.minHeight, z.x > 0 ? b = z.y > 0 ? g : l : z.x < 0 && (C -= $, b = z.y > 0 ? m : p), z.y < 0 && (M -= k), this.cropped || (this.$cropBox.removeClass(v), this.cropped = !0, this.limited && this.limitCropBox(!0, !0))
                    }
                    H && (x.width = $, x.height = k, x.left = C, x.top = M, this.action = b, this.renderCropBox()), t.each(u, function(t, i) {
                        i.startX = i.endX, i.startY = i.endY
                    })
                }
            },
            mt = {
                crop: function() {
                    this.ready && !this.disabled && (this.cropped || (this.cropped = !0, this.limitCropBox(!0, !0), this.options.modal && this.$dragBox.addClass(x), this.$cropBox.removeClass(v)), this.setCropBoxData(this.initialCropBox))
                },
                reset: function() {
                    this.ready && !this.disabled && (this.image = t.extend({}, this.initialImage), this.canvas = t.extend({}, this.initialCanvas), this.cropBox = t.extend({}, this.initialCropBox), this.renderCanvas(), this.cropped && this.renderCropBox())
                },
                clear: function() {
                    this.cropped && !this.disabled && (t.extend(this.cropBox, {
                        left: 0,
                        top: 0,
                        width: 0,
                        height: 0
                    }), this.cropped = !1, this.renderCropBox(), this.limitCanvas(!0, !0), this.renderCanvas(), this.$dragBox.removeClass(x), this.$cropBox.addClass(v))
                },
                replace: function(t, i) {
                    !this.disabled && t && (this.isImg && this.$element.attr("src", t), i ? (this.url = t, this.$clone.attr("src", t), this.ready && this.$preview.find("img").add(this.$clone2).attr("src", t)) : (this.isImg && (this.replaced = !0), this.options.data = null, this.load(t)))
                },
                enable: function() {
                    this.ready && (this.disabled = !1, this.$cropper.removeClass(f))
                },
                disable: function() {
                    this.ready && (this.disabled = !0, this.$cropper.addClass(f))
                },
                destroy: function() {
                    var t = this.$element;
                    this.loaded ? (this.isImg && this.replaced && t.attr("src", this.originalUrl), this.unbuild(), t.removeClass(v)) : this.isImg ? t.off(Y, this.start) : this.$clone && this.$clone.remove(), t.removeData(e)
                },
                move: function(t, i) {
                    var e = this.canvas,
                        a = e.left,
                        o = e.top;
                    this.moveTo(S(t) ? t : a + Number(t), S(i) ? i : o + Number(i))
                },
                moveTo: function(t, i) {
                    var e = this.canvas,
                        a = !1;
                    S(i) && (i = t), t = Number(t), i = Number(i), this.ready && !this.disabled && this.options.movable && (F(t) && (e.left = t, a = !0), F(i) && (e.top = i, a = !0), a && this.renderCanvas(!0))
                },
                zoom: function(t, i) {
                    var e = this.canvas;
                    t = (t = Number(t)) < 0 ? 1 / (1 - t) : 1 + t, this.zoomTo(e.width * t / e.naturalWidth, i)
                },
                zoomTo: function(i, e) {
                    var a, o, n, h, s = this.options,
                        r = this.pointers,
                        d = this.canvas,
                        c = d.width,
                        l = d.height,
                        p = d.naturalWidth,
                        g = d.naturalHeight;
                    if ((i = Number(i)) >= 0 && this.ready && !this.disabled && s.zoomable) {
                        var m = p * i,
                            u = g * i,
                            f = void 0;
                        if (e && (f = e.originalEvent), this.trigger(R, {
                                originalEvent: f,
                                oldRatio: c / p,
                                ratio: m / p
                            }).isDefaultPrevented()) return;
                        if (f) {
                            var v = this.$cropper.offset(),
                                w = r && Q(r).length ? (a = r, o = 0, n = 0, h = 0, t.each(a, function(t, i) {
                                    var e = i.startX,
                                        a = i.startY;
                                    o += e, n += a, h += 1
                                }), {
                                    pageX: o /= h,
                                    pageY: n /= h
                                }) : {
                                    pageX: e.pageX || f.pageX || 0,
                                    pageY: e.pageY || f.pageY || 0
                                };
                            d.left -= (m - c) * ((w.pageX - v.left - d.left) / c), d.top -= (u - l) * ((w.pageY - v.top - d.top) / l)
                        } else d.left -= (m - c) / 2, d.top -= (u - l) / 2;
                        d.width = m, d.height = u, this.renderCanvas(!0)
                    }
                },
                rotate: function(t) {
                    this.rotateTo((this.image.rotate || 0) + Number(t))
                },
                rotateTo: function(t) {
                    F(t = Number(t)) && this.ready && !this.disabled && this.options.rotatable && (this.image.rotate = t % 360, this.renderCanvas(!0, !0))
                },
                scaleX: function(t) {
                    var i = this.image.scaleY;
                    this.scale(t, F(i) ? i : 1)
                },
                scaleY: function(t) {
                    var i = this.image.scaleX;
                    this.scale(F(i) ? i : 1, t)
                },
                scale: function(t) {
                    var i = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : t,
                        e = this.image,
                        a = !1;
                    t = Number(t), i = Number(i), this.ready && !this.disabled && this.options.scalable && (F(t) && (e.scaleX = t, a = !0), F(i) && (e.scaleY = i, a = !0), a && this.renderCanvas(!0, !0))
                },
                getData: function() {
                    var i = arguments.length > 0 && void 0 !== arguments[0] && arguments[0],
                        e = this.options,
                        a = this.image,
                        o = this.canvas,
                        n = this.cropBox,
                        h = void 0;
                    if (this.ready && this.cropped) {
                        h = {
                            x: n.left - o.left,
                            y: n.top - o.top,
                            width: n.width,
                            height: n.height
                        };
                        var s = a.width / a.naturalWidth;
                        t.each(h, function(t, e) {
                            e /= s, h[t] = i ? Math.round(e) : e
                        })
                    } else h = {
                        x: 0,
                        y: 0,
                        width: 0,
                        height: 0
                    };
                    return e.rotatable && (h.rotate = a.rotate || 0), e.scalable && (h.scaleX = a.scaleX || 1, h.scaleY = a.scaleY || 1), h
                },
                setData: function(i) {
                    var e = this.options,
                        a = this.image,
                        o = this.canvas,
                        n = {};
                    if (t.isFunction(i) && (i = i.call(this.element)), this.ready && !this.disabled && t.isPlainObject(i)) {
                        var h = !1;
                        e.rotatable && F(i.rotate) && i.rotate !== a.rotate && (a.rotate = i.rotate, h = !0), e.scalable && (F(i.scaleX) && i.scaleX !== a.scaleX && (a.scaleX = i.scaleX, h = !0), F(i.scaleY) && i.scaleY !== a.scaleY && (a.scaleY = i.scaleY, h = !0)), h && this.renderCanvas(!0, !0);
                        var s = a.width / a.naturalWidth;
                        F(i.x) && (n.left = i.x * s + o.left), F(i.y) && (n.top = i.y * s + o.top), F(i.width) && (n.width = i.width * s), F(i.height) && (n.height = i.height * s), this.setCropBoxData(n)
                    }
                },
                getContainerData: function() {
                    return this.ready ? t.extend({}, this.container) : {}
                },
                getImageData: function() {
                    return this.loaded ? t.extend({}, this.image) : {}
                },
                getCanvasData: function() {
                    var i = this.canvas,
                        e = {};
                    return this.ready && t.each(["left", "top", "width", "height", "naturalWidth", "naturalHeight"], function(t, a) {
                        e[a] = i[a]
                    }), e
                },
                setCanvasData: function(i) {
                    var e = this.canvas,
                        a = e.aspectRatio;
                    t.isFunction(i) && (i = i.call(this.$element)), this.ready && !this.disabled && t.isPlainObject(i) && (F(i.left) && (e.left = i.left), F(i.top) && (e.top = i.top), F(i.width) ? (e.width = i.width, e.height = i.width / a) : F(i.height) && (e.height = i.height, e.width = i.height * a), this.renderCanvas(!0))
                },
                getCropBoxData: function() {
                    var t = this.cropBox;
                    return this.ready && this.cropped ? {
                        left: t.left,
                        top: t.top,
                        width: t.width,
                        height: t.height
                    } : {}
                },
                setCropBoxData: function(i) {
                    var e = this.cropBox,
                        a = this.options.aspectRatio,
                        o = void 0,
                        n = void 0;
                    t.isFunction(i) && (i = i.call(this.$element)), this.ready && this.cropped && !this.disabled && t.isPlainObject(i) && (F(i.left) && (e.left = i.left), F(i.top) && (e.top = i.top), F(i.width) && i.width !== e.width && (o = !0, e.width = i.width), F(i.height) && i.height !== e.height && (n = !0, e.height = i.height), a && (o ? e.height = e.width / a : n && (e.width = e.height * a)), this.renderCropBox())
                },
                getCroppedCanvas: function() {
                    var i = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
                    if (!this.ready || !window.HTMLCanvasElement) return null;
                    var e, a, o, n, h, s, r, d, c, l, p, g, m, u, f, v, w, x, b, y, C, M, $, k, B, D, W, Y, T, X, H, O, R, E, N = this.canvas,
                        L = (e = this.$clone[0], a = this.image, o = N, n = i, h = a.rotate, s = void 0 === h ? 0 : h, r = a.scaleX, d = void 0 === r ? 1 : r, c = a.scaleY, l = void 0 === c ? 1 : c, p = o.aspectRatio, g = o.naturalWidth, m = o.naturalHeight, u = n.fillColor, f = void 0 === u ? "transparent" : u, v = n.imageSmoothingEnabled, w = void 0 === v || v, x = n.imageSmoothingQuality, b = void 0 === x ? "low" : x, y = n.maxWidth, C = void 0 === y ? 1 / 0 : y, M = n.maxHeight, $ = void 0 === M ? 1 / 0 : M, k = n.minWidth, B = void 0 === k ? 0 : k, D = n.minHeight, W = void 0 === D ? 0 : D, Y = document.createElement("canvas"), T = Y.getContext("2d"), X = nt({
                            aspectRatio: p,
                            width: C,
                            height: $
                        }), H = nt({
                            aspectRatio: p,
                            width: B,
                            height: W
                        }, "cover"), O = Math.min(X.width, Math.max(H.width, g)), R = Math.min(X.height, Math.max(H.height, m)), E = [-O / 2, -R / 2, O, R], Y.width = J(O), Y.height = J(R), T.fillStyle = f, T.fillRect(0, 0, O, R), T.save(), T.translate(O / 2, R / 2), T.rotate(s * Math.PI / 180), T.scale(d, l), T.imageSmoothingEnabled = w, T.imageSmoothingQuality = b, T.drawImage.apply(T, [e].concat(j(t.map(E, function(t) {
                            return Math.floor(J(t))
                        })))), T.restore(), Y);
                    if (!this.cropped) return L;
                    var z = this.getData(),
                        P = z.x,
                        U = z.y,
                        I = z.width,
                        A = z.height,
                        F = L.width / Math.floor(N.naturalWidth);
                    1 !== F && (P *= F, U *= F, I *= F, A *= F);
                    var S = I / A,
                        q = nt({
                            aspectRatio: S,
                            width: i.maxWidth || 1 / 0,
                            height: i.maxHeight || 1 / 0
                        }),
                        Q = nt({
                            aspectRatio: S,
                            width: i.minWidth || 0,
                            height: i.minHeight || 0
                        }, "cover"),
                        _ = nt({
                            aspectRatio: S,
                            width: i.width || (1 !== F ? L.width : I),
                            height: i.height || (1 !== F ? L.height : A)
                        }),
                        K = _.width,
                        Z = _.height;
                    K = Math.min(q.width, Math.max(Q.width, K)), Z = Math.min(q.height, Math.max(Q.height, Z));
                    var V = document.createElement("canvas"),
                        G = V.getContext("2d");
                    V.width = J(K), V.height = J(Z), G.fillStyle = i.fillColor || "transparent", G.fillRect(0, 0, K, Z);
                    var tt = i.imageSmoothingEnabled,
                        it = void 0 === tt || tt,
                        et = i.imageSmoothingQuality;
                    G.imageSmoothingEnabled = it, et && (G.imageSmoothingQuality = et);
                    var at = L.width,
                        ot = L.height,
                        ht = P,
                        st = U,
                        rt = void 0,
                        dt = void 0,
                        ct = void 0,
                        lt = void 0,
                        pt = void 0,
                        gt = void 0;
                    ht <= -I || ht > at ? (ht = 0, rt = 0, ct = 0, pt = 0) : ht <= 0 ? (ct = -ht, ht = 0, pt = rt = Math.min(at, I + ht)) : ht <= at && (ct = 0, pt = rt = Math.min(I, at - ht)), rt <= 0 || st <= -A || st > ot ? (st = 0, dt = 0, lt = 0, gt = 0) : st <= 0 ? (lt = -st, st = 0, gt = dt = Math.min(ot, A + st)) : st <= ot && (lt = 0, gt = dt = Math.min(A, ot - st));
                    var mt = [ht, st, rt, dt];
                    if (pt > 0 && gt > 0) {
                        var ut = K / I;
                        mt.push(ct * ut, lt * ut, pt * ut, gt * ut)
                    }
                    return G.drawImage.apply(G, [L].concat(j(t.map(mt, function(t) {
                        return Math.floor(J(t))
                    })))), V
                },
                setAspectRatio: function(t) {
                    var i = this.options;
                    this.disabled || S(t) || (i.aspectRatio = Math.max(0, t) || NaN, this.ready && (this.initCropBox(), this.cropped && this.renderCropBox()))
                },
                setDragMode: function(t) {
                    var i = this.options,
                        e = void 0,
                        a = void 0;
                    this.loaded && !this.disabled && (e = t === M, a = i.movable && "move" === t, t = e || a ? t : "none", this.$dragBox.data(y, t).toggleClass(u, e).toggleClass(b, a), i.cropBoxMovable || this.$face.data(y, t).toggleClass(u, e).toggleClass(b, a))
                }
            },
            ut = function() {
                function i(e) {
                    var a = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : {};
                    if (U(this, i), !e || !z.test(e.tagName)) throw new Error("The first argument is required and must be an <img> or <canvas> element.");
                    this.element = e, this.$element = t(e), this.options = t.extend({}, P, t.isPlainObject(a) && a), this.completed = !1, this.cropped = !1, this.disabled = !1, this.isImg = !1, this.limited = !1, this.loaded = !1, this.ready = !1, this.replaced = !1, this.wheeling = !1, this.originalUrl = "", this.canvas = null, this.cropBox = null, this.pointers = {}, this.init()
                }
                return I(i, [{
                    key: "init",
                    value: function() {
                        var t = this.$element,
                            i = void 0;
                        if (t.is("img")) {
                            if (this.isImg = !0, i = t.attr("src") || "", this.originalUrl = i, !i) return;
                            i = t.prop("src")
                        } else t.is("canvas") && window.HTMLCanvasElement && (i = t[0].toDataURL());
                        this.load(i)
                    }
                }, {
                    key: "trigger",
                    value: function(i, e) {
                        var a = t.Event(i, e);
                        return this.$element.trigger(a), a
                    }
                }, {
                    key: "load",
                    value: function(i) {
                        var e = this;
                        if (i) {
                            this.url = i, this.image = {};
                            var a = this.$element,
                                o = this.options;
                            if (o.checkOrientation && window.ArrayBuffer)
                                if (N.test(i)) L.test(i) ? this.read((n = i.replace(st, ""), h = atob(n), s = new ArrayBuffer(h.length), r = new Uint8Array(s), t.each(r, function(t) {
                                    r[t] = h.charCodeAt(t)
                                }), s)) : this.clone();
                                else {
                                    var n, h, s, r, d = new XMLHttpRequest;
                                    d.onerror = function() {
                                        e.clone()
                                    }, d.onload = function() {
                                        e.read(d.response)
                                    }, o.checkCrossOrigin && V(i) && !a.prop("crossOrigin") && (i = G(i)), d.open("get", i), d.responseType = "arraybuffer", d.withCredentials = "use-credentials" === a.prop("crossOrigin"), d.send()
                                } else this.clone()
                        }
                    }
                }, {
                    key: "read",
                    value: function(i) {
                        var e, a, o, n = this.options,
                            h = this.image,
                            s = rt(i),
                            r = 0,
                            d = 1,
                            c = 1;
                        if (s > 1) {
                            this.url = (e = "image/jpeg", a = new Uint8Array(i), o = "", t.each(a, function(t, i) {
                                o += ht(i)
                            }), "data:" + e + ";base64," + btoa(o));
                            var l = function(t) {
                                var i = 0,
                                    e = 1,
                                    a = 1;
                                switch (t) {
                                    case 2:
                                        e = -1;
                                        break;
                                    case 3:
                                        i = -180;
                                        break;
                                    case 4:
                                        a = -1;
                                        break;
                                    case 5:
                                        i = 90, a = -1;
                                        break;
                                    case 6:
                                        i = 90;
                                        break;
                                    case 7:
                                        i = 90, e = -1;
                                        break;
                                    case 8:
                                        i = -90
                                }
                                return {
                                    rotate: i,
                                    scaleX: e,
                                    scaleY: a
                                }
                            }(s);
                            r = l.rotate, d = l.scaleX, c = l.scaleY
                        }
                        n.rotatable && (h.rotate = r), n.scalable && (h.scaleX = d, h.scaleY = c), this.clone()
                    }
                }, {
                    key: "clone",
                    value: function() {
                        var i = this.$element,
                            e = this.options,
                            a = this.url,
                            o = "",
                            n = void 0;
                        e.checkCrossOrigin && V(a) && ((o = i.prop("crossOrigin")) ? n = a : (o = "anonymous", n = G(a))), this.crossOrigin = o, this.crossOriginUrl = n;
                        var h = document.createElement("img");
                        o && (h.crossOrigin = o), h.src = n || a;
                        var s = t(h);
                        this.$clone = s, this.isImg ? this.element.complete ? this.start() : i.one(Y, t.proxy(this.start, this)) : s.one(Y, t.proxy(this.start, this)).one("error", t.proxy(this.stop, this)).addClass(w).insertAfter(i)
                    }
                }, {
                    key: "start",
                    value: function() {
                        var i = this,
                            e = this.$clone,
                            a = this.$element;
                        this.isImg || (e.off("error", this.stop), a = e),
                            function(t, i) {
                                if (!t.naturalWidth || et) {
                                    var e = document.createElement("img");
                                    e.onload = function() {
                                        i(e.width, e.height)
                                    }, e.src = t.src
                                } else i(t.naturalWidth, t.naturalHeight)
                            }(a[0], function(e, a) {
                                t.extend(i.image, {
                                    naturalWidth: e,
                                    naturalHeight: a,
                                    aspectRatio: e / a
                                }), i.loaded = !0, i.build()
                            })
                    }
                }, {
                    key: "stop",
                    value: function() {
                        this.$clone.remove(), this.$clone = null
                    }
                }, {
                    key: "build",
                    value: function() {
                        var i = this;
                        if (this.loaded) {
                            this.ready && this.unbuild();
                            var o = this.$element,
                                n = this.options,
                                h = this.$clone,
                                s = t('<div class="cropper-container"><div class="cropper-wrap-box"><div class="cropper-canvas"></div></div><div class="cropper-drag-box"></div><div class="cropper-crop-box"><span class="cropper-view-box"></span><span class="cropper-dashed dashed-h"></span><span class="cropper-dashed dashed-v"></span><span class="cropper-center"></span><span class="cropper-face"></span><span class="cropper-line line-e" data-action="e"></span><span class="cropper-line line-n" data-action="n"></span><span class="cropper-line line-w" data-action="w"></span><span class="cropper-line line-s" data-action="s"></span><span class="cropper-point point-e" data-action="e"></span><span class="cropper-point point-n" data-action="n"></span><span class="cropper-point point-w" data-action="w"></span><span class="cropper-point point-s" data-action="s"></span><span class="cropper-point point-ne" data-action="ne"></span><span class="cropper-point point-nw" data-action="nw"></span><span class="cropper-point point-sw" data-action="sw"></span><span class="cropper-point point-se" data-action="se"></span></div></div>'),
                                r = s.find("." + e + "-crop-box"),
                                d = r.find("." + e + "-face");
                            this.$container = o.parent(), this.$cropper = s, this.$canvas = s.find("." + e + "-canvas").append(h), this.$dragBox = s.find("." + e + "-drag-box"), this.$cropBox = r, this.$viewBox = s.find("." + e + "-view-box"), this.$face = d, o.addClass(v).after(s), this.isImg || h.removeClass(w), this.initPreview(), this.bind(), n.aspectRatio = Math.max(0, n.aspectRatio) || NaN, n.viewMode = Math.max(0, Math.min(3, Math.round(n.viewMode))) || 0, this.cropped = n.autoCrop, n.autoCrop ? n.modal && this.$dragBox.addClass(x) : r.addClass(v), n.guides || r.find("." + e + "-dashed").addClass(v), n.center || r.find("." + e + "-center").addClass(v), n.cropBoxMovable && d.addClass(b).data(y, a), n.highlight || d.addClass("cropper-invisible"), n.background && s.addClass(e + "-bg"), n.cropBoxResizable || r.find("." + e + "-line,." + e + "-point").addClass(v), this.setDragMode(n.dragMode), this.render(), this.ready = !0, this.setData(n.data), this.completing = setTimeout(function() {
                                t.isFunction(n.ready) && o.one("ready", n.ready), i.trigger("ready"), i.trigger($, i.getData()), i.completed = !0
                            }, 0)
                        }
                    }
                }, {
                    key: "unbuild",
                    value: function() {
                        this.ready && (this.completed || clearTimeout(this.completing), this.ready = !1, this.completed = !1, this.initialImage = null, this.initialCanvas = null, this.initialCropBox = null, this.container = null, this.canvas = null, this.cropBox = null, this.unbind(), this.resetPreview(), this.$preview = null, this.$viewBox = null, this.$cropBox = null, this.$dragBox = null, this.$canvas = null, this.$container = null, this.$cropper.remove(), this.$cropper = null)
                    }
                }], [{
                    key: "setDefaults",
                    value: function(i) {
                        t.extend(P, t.isPlainObject(i) && i)
                    }
                }]), i
            }();
        if (t.extend && t.extend(ut.prototype, dt, ct, lt, pt, gt, mt), t.fn) {
            var ft = t.fn.cropper;
            t.fn.cropper = function(i) {
                for (var a = arguments.length, o = Array(a > 1 ? a - 1 : 0), n = 1; n < a; n++) o[n - 1] = arguments[n];
                var h = void 0;
                return this.each(function(a, n) {
                    var s = t(n),
                        r = s.data(e);
                    if (!r) {
                        if (/destroy/.test(i)) return;
                        var d = t.extend({}, s.data(), t.isPlainObject(i) && i);
                        r = new ut(n, d), s.data(e, r)
                    }
                    if ("string" == typeof i) {
                        var c = r[i];
                        t.isFunction(c) && (h = c.apply(r, o))
                    }
                }), S(h) ? this : h
            }, t.fn.cropper.Constructor = ut, t.fn.cropper.setDefaults = ut.setDefaults, t.fn.cropper.noConflict = function() {
                return t.fn.cropper = ft, this
            }
        }
    });