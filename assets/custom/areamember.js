var x,x1,x2,x3,i,pi,charCode,rgx,pf,intVal,api,pageTotal;$.ajaxSetup({type:"post",cache:!1,dataType:"json"});pi=parseInt;pf=parseFloat;function getFilter(id,format,view,title){$(id).datepicker({format:format,autoclose:!0,viewMode:view,minViewMode:view,title:'Pilih '+title,orientation:'auto bottom'})}
function getDatePicker(id,sd){$(id).datepicker({format:'dd-mm-yyyy',startDate:sd,autoclose:!0})}
function addCommas(nStr){nStr+='';x3=nStr.split('.');x1=x3[0];x2=x3.length>1?'.'+x3[1]:'';rgx=/(\d+)(\d{3})/;while(rgx.test(x1)){x1=x1.replace(rgx,'$1'+','+'$2')}return x1+x2}
function hanyaAngka(evt){charCode=(evt.which)?evt.which:event.keyCode;if(charCode>31&&(charCode<48||charCode>57))return!1;return!0}
function previewIMG(idfile,idprev){$(idfile).change(function(){$(idprev).attr('src',window.URL.createObjectURL(this.files[0]))})}
function removeCommas(str){return(str.replace(/,/g,''))}
function getDataTable(id,url,rownya){$(id).DataTable({"pageLength":10,"ordering":!0,"processing":!0,"serverSide":!0,"order":[],"oLanguage":{"sProcessing":"Sedang memproses...","sLengthMenu":"Tampilkan _MENU_ entri","sZeroRecords":"Tidak ditemukan data yang sesuai","sInfo":"","sInfoEmpty":"","sInfoFiltered":"","sInfoPostFix":"","sSearch":"Cari:","sUrl":"","oPaginate":{"sFirst":"Pertama","sPrevious":"&#8678; Sebelumnya","sNext":"Selanjutnya &#8680;","sLast":"Terakhir"},},"ajax":{"url":"./"+url,"type":"POST"},"columnDefs":[{"targets":[0],"orderable":!1}],"footerCallback":function(row,data,start,end,display){api=this.api(),data;intVal=function(i){return typeof i==='string'?i.replace(/[\$,]/g,'')*1:typeof i==='number'?i:0};total=api.column(rownya).data().reduce(function(a,b){return intVal(a)+intVal(b)},0);pageTotal=api.column(rownya,{page:'current'}).data().reduce(function(a,b){return intVal(a)+intVal(b)},0);$(api.column(rownya).footer()).html('Rp. '+addCommas(pageTotal)+' (Rp. '+addCommas(total)+' total)')}})}
function getDataSSP(id,url,rownya='',filter=null){$(id).dataTable({"oLanguage":{"sProcessing":"Sedang memproses...","sLengthMenu":"Tampilkan _MENU_ entri","sZeroRecords":"Tidak ada data pada periode ini","sInfo":"","sInfoEmpty":"","sInfoFiltered":"","sInfoPostFix":"","sSearch":"Cari:","sUrl":"","oPaginate":{"sFirst":"Pertama","sPrevious":"<i class='far fa-arrow-alt-circle-left'></i> Sebelumnya","sNext":"Selanjutnya <i class='far fa-arrow-alt-circle-right'></i>","sLast":"Terakhir"},},"processing":!0,"serverSide":!0,"ajax":{"data":{"filter":filter},"url":url},"fnCreatedRow":function(row,data,index){$('td',row).eq(0).html(index+1)},"footerCallback":function(row,data,start,end,display){api=this.api(),data;intVal=function(i){return typeof i==='string'?i.replace(/[\$,]/g,'')*1:typeof i==='number'?i:0};total=api.column(rownya).data().reduce(function(a,b){return intVal(a)+intVal(b)},0);pageTotal=api.column(rownya,{page:'current'}).data().reduce(function(a,b){return intVal(a)+intVal(b)},0);$(api.column(rownya).footer()).html('Rp. '+addCommas(pageTotal)+' (Rp. '+addCommas(total)+' total)')}})}
function Toastr(msg,title){toastr.options.timeOut=4000;toastr.options.showDuration=500;toastr.options.closeButton=!0;toastr.options.showMethod="slideDown";toastr.options.positionClass="toast-top-right";toastr.warning(msg,title).css("width","400px")}
function ToastrSukses(msg,title){toastr.options.timeOut=5000;toastr.options.showDuration=500;toastr.options.closeButton=!0;toastr.options.showMethod="slideDown";toastr.options.positionClass="toast-top-right";toastr.success(msg,title).css("width","400px")}

$(document).on("focus","input",function(){
$(this).select();
});
$(document).on("focus","textarea",function(){
$(this).select();
});
// $('textarea').keyup(function(){
// $(this).val($(this).val().toUpperCase());
// });
// $('input').keyup(function(){
// $(this).val($(this).val().toUpperCase());
// });