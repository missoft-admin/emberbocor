<ul class="nav bg blue">
  <li class="nav-header deep-orange">
    <span class="hidden-folded text-sm text-white">
        <b><i class="fa fa-home"></i>
    MENU UTAMA</b></span>
  </li>
  <li <?=menuIsActive('dashboard');if($this->uri->segment(1)==null){echo 'class="active"';}else{echo '';}?>>
    <a href="{base_url}dashboard">
      <span class="nav-icon">
        <i class="fas fa-tachometer-alt"></i>
      </span>
      <span class="nav-text ">Dashboard</span>
    </a>
  </li>
      <li <?=menuActiveUsers('datapribadi')?>>
        <a href="{base_url}u/users/settings/datapribadi">
      <span class="nav-icon">
        <i class="fas fa-user"></i>
      </span>
            <span class="nav-text ">Edit Profil</span>
          </a>
      </li>
      <li <?=menuActiveUsers('datarekening')?>>
        <a href="{base_url}u/users/settings/datarekening">
      <span class="nav-icon">
        <i class="fas fa-credit-card"></i>
      </span>
            <span class="nav-text ">Edit Rekening</span>
          </a>
      </li>
      <li <?=menuActiveUsers('anggotarekomendasi')?>>
        <a href="{base_url}u/users/settings/anggotarekomendasi">
      <span class="nav-icon">
        <i class="fas fa-sitemap"></i>
      </span>
            <span class="nav-text ">Anggota Rekomendasi</span>
          </a>
      </li>
  <li class="nav-header deep-orange">
    <span class="hidden-folded text-sm text-white">
        <b><i class="fa fa-shopping-cart"></i>
      TRANSAKSI</b></span>
  </li> 
      <li <?=menuIsActived('verifikasi-Bayar')?>>
        <a href="{base_url}r/verifikasi-Bayar">
      <span class="nav-icon">
        <i class="fas fa-cloud-upload-alt"></i>
      </span>
            <span class="nav-text ">Upload Bukti Transfer</span>
          </a>
      </li>
      <li <?=menuIsActived('verifikasi-Terima')?>>
        <a href="{base_url}r/verifikasi-Terima">
      <span class="nav-icon">
        <i class="fas fa-handshake"></i>
      </span>
            <span class="nav-text ">Verifikasi Penerimaan</span>
          </a>
      </li>
      <li <?=menuIsActived('rekap-Belanja')?>>
        <a href="{base_url}r/rekap-Belanja">
      <span class="nav-icon">
        <i class="far fa-chart-bar"></i>
      </span>
            <span class="nav-text ">Rekapitulasi Belanja</span>
          </a>
      </li>
</ul>
<ul class="nav bg blue">
  <li class="nav-header deep-orange">
   <span class="hidden-folded text-sm text-white">
        <b><i class="fas fa-donate"></i>
    PELAPORAN</b></span>
  </li>
  <li <?=menuIsActived('sponsor')?>>
    <a href="{base_url}p/sponsor">
      <span class="nav-icon">
        <i class="fa fa-money-bill-alt"></i>
      </span>
      <span class="nav-text ">Cashback Sponsor</span>
    </a>
  </li>
  <li <?=menuIsActived('royalti1')?>>
    <a href="{base_url}p/royalti1">
      <span class="nav-icon">
        <i class="fa fa-money-bill-alt"></i>
      </span>
      <span class="nav-text ">Cashback Royalti 1</span>
    </a>
  </li>
  <li <?=menuIsActived('royalti2')?>>
    <a href="{base_url}p/royalti2">
      <span class="nav-icon">
        <i class="fa fa-money-bill-alt"></i>
      </span>
      <span class="nav-text ">Cashback Royalti 2</span>
    </a>
  </li>
  <li <?=menuIsActived('loyalti')?>>
    <a href="{base_url}p/loyalti">
      <span class="nav-icon">
        <i class="fa fa-money-bill-alt"></i>
      </span>
      <span class="nav-text ">Cashback Loyalti</span>
    </a>
  </li>
  <li <?=menuIsActived('totalcashback')?>>
    <a href="{base_url}p/totalcashback">
      <span class="nav-icon">
        <i class="fa fa-money-bill-alt"></i>
      </span>
      <span class="nav-text ">Total Cashback</span>
    </a>
  </li> 
  <li class="nav-header deep-orange">
    <span class="hidden-folded text-sm text-white">
        <b><i class="fas fa-cogs"></i>
    PENGATURAN</b></span>
  </li>
      <li <?=menuActiveUsers('datapassword')?>>
        <a href="{base_url}u/users/settings/datapassword">
          <span class="nav-icon">
            <i class="fa fa-lock"></i>
          </span>
            <span class="nav-text ">Ganti Password</span>
          </a>
      </li>
</ul>
