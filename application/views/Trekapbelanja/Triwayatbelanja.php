<style type="text/css">
  a.bolder:hover {text-decoration: underline;
    color: #39c;
  }
  a.bolder{
    color: #069;
  }
</style>
<div class="padding">
<div class="row box">
  <div class="col-sm-12">
    <div class="box-header">
      <h5 class="mb-3">{title}</h5>
      <hr>
    </div>
        <div class="block block-bordered light">
          <div class="block-header light">
              <h3 class="block-title">Filtering</h3>
          </div>
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}r/rekap-Belanja" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_fil">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan}" id="bulan_fil" name="bulan_fil">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_fil">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun}" id="tahun_fil" name="tahun_fil">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Filter</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
    <div class="form-group row _500">
    <label class="col-sm-2">TOTAL PEMBELIAN</label>
    <label  style="margin-left: -5%">: Rp.<?=number_format($totalbiaya)?></label>
</div>
<div class=" b-t b-t-warning b-t-3x"></div>
    <div class="box-body">
      <h6 class="mb-3">RIWAYAT TRANSAKSI</h6>
<table width="100%" id="rekapbelanja" class="table table-striped v-middle p-0 m-0 box">
  <thead class="deep-orange">
          <tr>
              <th>NO</th>
              <th>ID TRANSAKSI</th>
              <th>TANGGAL TRANSAKSI</th>
              <th>NAMA PENJUAL</th>
              <th>TOTAL BAYAR</th>
              <th>STATUS</th>
          </tr>
      </thead> 
  </table>
    </div>
    <div class="box-footer">
    </div>
  </div>
  </div>
</div>

        <!-- .modal History -->
        <div id="histori-modal" class="modal black-overlay fade" data-backdrop="false">
          <div class="modal-dialog modal-lg animate " style="width: 100%" id="animate">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Detail Produk Yang di Pesan</h5>
              </div>
              <div class="modal-body">
                <div id="xmlhttpRespone">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn deep-orange p-x-md tutupdong" data-dismiss="modal">Tutup</button>
              </div>
            </div><!-- /.modal-content -->
          </div>
        </div>
        <!-- / .modal History -->

<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
  $(function () {
      getFilter('#bulan_fil','MM','months','Bulan')
      getFilter('#tahun_fil','yyyy','years','Tahun')
 $(document).ready(function() {
     getDataSSP("#rekapbelanja","../r/get-rekap-Belanja?tahun_fil="+$('#tahun_fil').val()+"&&bulan_fil="+$('#bulan_fil').val());
    $(document).on("click", ".bolder", function () {
        detailtrans($(this).closest('tr').find("td:eq(1)").text())
        // $('#histori-modal').modal('show');
        // alert($(this).closest('tr').find("td:eq(1)").text())
    })
            });



    // get detail pengajuan barang
    function detailtrans(id) {
        var obj = document.getElementById("xmlhttpRespone");
        var url = '{base_url}r/get-rekap-Belanja/detailtrans/' + id;
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.open("GET", url);

        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                obj.innerHTML = xmlhttp.responseText;
                // $('.floatTheadpo').floatThead('reflow');
            } else {
                obj.innerHTML = "<div></div>";
            }
        }
        xmlhttp.send(null);
    }
})
  </script> 