<div class="padding"> 
  <?php echo ErrorSuccess($this->session)?>
  <?php if($error != '') echo ErrorMessage($error)?>
  <script type="text/javascript">
    $(document).ready(function(){
<?php
if(!empty($_SESSION['status'])){
    echo $_SESSION['status'];
$_SESSION['status']='';
}else{
$_SESSION['status']='';
    }
    ?>
})
</script>
<div class="row box">
  <div class="col-sm-12">
    <div class="box-header">
      <h5 class="mb-3">{title}</h5>
      <hr>
    </div>
        <div class="block block-bordered light">
          <div class="block-header light">
              <h3 class="block-title">Filtering</h3>
          </div>
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}r/verifikasi-Bayar" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_fil">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan}" id="bulan_fil" name="bulan_fil">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_fil">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun}" id="tahun_fil" name="tahun_fil">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Filter</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
<div class=" b-t b-t-warning b-t-3x"></div>
    <div class="box-body">
      <h6 class="mb-3">TRANSAKSI YANG BELUM SELESAI DIBAYAR</h6>
<table width="100%" id="rebelvbayar" class="table table-striped v-middle p-0 m-0 box">
  <thead class="deep-orange">
          <tr>
              <th>NO</th>
              <th>ID TRANSAKSI</th>
              <th>TANGGAL TRANSAKSI</th>
              <th>NAMA PENJUAL</th>
              <th>TOTAL BAYAR</th>
              <th>STATUS</th>
              <th>AKSI</th>
          </tr>
      </thead> 
  </table>
    </div>
    <div class="box-footer">
    </div>
  </div>
  </div>
</div>
<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
  $(function () {
      getFilter('#bulan_fil','MM','months','Bulan')
      getFilter('#tahun_fil','yyyy','years','Tahun')
 $(document).ready(function() {
     getDataSSP("#rebelvbayar","../r/get-rebel-vbayar?tahun_fil="+$('#tahun_fil').val()+"&&bulan_fil="+$('#bulan_fil').val());
            });
})
  </script> 