<!DOCTYPE html>
<html lang=en>
<head>
<meta charset=utf-8 />
<title>Kulcimart | {title}</title>
<meta name=description content="Responsive, Bootstrap, BS4"/>
<meta name=viewport content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui"/>
<meta http-equiv=X-UA-Compatible content="IE=edge">
<meta name=apple-mobile-web-app-capable content=yes>
<meta name=apple-mobile-web-app-status-barstyle content=black-translucent>
<link rel=apple-touch-icon href="http://member.kulcimart.com/assets/images/logo-default.png">
<meta name=apple-mobile-web-app-title content=Flatkit>
<meta name=mobile-web-app-capable content=yes>
<link rel=stylesheet id=css-main href="{toastr_path}toastr.min.css">
<link rel="shortcut icon" sizes="196x196" href="{img_path}logo-default.png">
<link rel=stylesheet href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel=stylesheet href="{css_path}app.css" type="text/css" />
<link rel=stylesheet href="{css_path}style.css" type="text/css" />
<link rel=stylesheet href="{custom_path}auth.css" type="text/css" />
<script src="{libs_path}jquery/dist/jquery.min.js"></script>
</head>
<body>
<div class="d-flex flex-column flex">
<div class="navbar light bg pos-rlt box-shadow">
<div class=mx-auto>
<a href="{site_url}" class="navbar-brand hover"><figure>
<img src="{img_path}logo-web-landscape.png">
</figure>
</a>
</div>
</div>
<div id=content-body>
<?php
$this->load->view($content);
?>
</div>
</div>
<script src="{toastr_path}toastr.min.js"></script>
<script src="{libs_path}popper.js/dist/umd/popper.min.js"></script>
<script src="{libs_path}bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{libs_path}pace-progress/pace.min.js"></script>
<script src="{libs_path}pjax/pjax.js"></script>
<script src="{scripts_path}lazyload.config.js"></script>
<script src="{scripts_path}lazyload.js"></script>
<script src="{scripts_path}plugin.js"></script>
<script src="{scripts_path}nav.js"></script>
<script src="{scripts_path}scrollto.js"></script>
<script src="{scripts_path}toggleclass.js"></script>
<script src="{scripts_path}theme.js"></script>
<script src="{scripts_path}ajax.js"></script>
<script src="{scripts_path}app.js"></script>
<script type="text/javascript">
function Toastr(msg,title){
        toastr.options.timeOut = 4000;
        toastr.options.showDuration = 500;
        toastr.options.closeButton = true;
        toastr.options.showMethod = "slideDown";
        toastr.options.positionClass = "toast-top-right";
        toastr.warning(msg,title).css("width","400px");
}
function ToastrSukses(msg,title){
        toastr.options.timeOut = 5000;
        toastr.options.showDuration = 500;
        toastr.options.closeButton = true;
        toastr.options.showMethod = "slideDown";
        toastr.options.positionClass = "toast-top-right";
        toastr.success(msg,title).css("width","400px");
}
</script>
</body>
</html>
