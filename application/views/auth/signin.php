<script type="text/javascript">
$(document).ready(function(){
<?php
if(!empty($_SESSION['status'])){
echo $_SESSION['status'];
$_SESSION['status']='';
}else{
$_SESSION['status']='';
}
?>
})

function InvalidMsg(textbox,tipe) {
  var teks;
  if(tipe==1){
    teks='Username';}else{
      teks='Password';}
  if(textbox.validity.valueMissing){
        textbox.setCustomValidity(teks+' tidak boleh kosong');
  }else if(textbox.validity.patternMismatch){
        textbox.setCustomValidity(teks+' tidak boleh pakai spasi');
    }
    else {
        textbox.setCustomValidity('');
    }
    return true;
}
</script>
<div class="py-5 text-center w-100">
<div class="mx-auto w-xxl w-auto-xs">
<div class=px-3>
<form name=form action=signin_proses method=post>
<div class=form-group>
<input type=text name=username id="username" pattern="[A-Za-z0-9][^\s]+" oninvalid="InvalidMsg(this,1)" oninput="this.setCustomValidity('');" class=form-control placeholder=Username required>
</div>
<div class=form-group>
<input type=password name=password oninvalid="InvalidMsg(this,2)" oninput="this.setCustomValidity('')" class=form-control placeholder=password required>
</div>      
<div class=mb-3>
<label class=md-check>
<input type=checkbox><i class=primary></i> Keep me signed in
</label>
</div>
<button type=submit class="btn primary">Sign in</button>
</form>
<div class=my-4>
<a href=Forgot class="text-primary _600">Forgot password?</a>
</div>
<div>
Do not have an account? 
<a href=SignUp class="text-primary _600">Sign up</a>
</div>
</div>
</div>
</div>