 <?php
  $idtrans        =decryptURL($this->uri->segment(3));
  $getdata=getdoublewhere('idtransaksi',$idtrans,'statusproses',4,'tjual');
  if($getdata->num_rows()==0){
    redirect(site_url().'r/verifikasi-Terima');
  }
  // $getdataproduk1  =getwherejoin('t2.idproduk',$getdata->idproduk,'tjual','manggotaproduk t2','t2.idanggota=t1.idanggota')->row();
foreach($getdata->result() as $row){
$idpartner=$row->idanggotapartner;
  $getdata2=getwhere('idtransaksi',$row->idtransaksi,'tjualdetail');
}
  $getdatajoin    =getwhere('idanggota',$idpartner,'manggota')->row();
?>
<div class="padding">
  <?php echo ErrorSuccess($this->session)?>
  <?php if($error != '') echo ErrorMessage($error)?>
<div class="row">
    <div class="col-sm-12">
      <form enctype="multipart/form-data" data-plugin="parsley" action="{site_url}v/verifikasi-Terima/proses" method="post" data-option="{}">
          <input type='hidden' name="idtransaksi" value="<?=$getdata->row()->idtransaksi?>" id="idtransaksi" />
          <input type='hidden' name="idanggota" value="<?=$getdata->row()->idanggota?>" id="idanggota" />
          <input type='hidden' name="periodetransaksi" value="<?=$getdata->row()->periodetransaksi?>" id="periodetransaksi" />
          <input type='hidden' name="tanggaltransaksi" value="<?=$getdata->row()->tanggaltransaksi?>" id="tanggaltransaksi" />
          <input type='hidden' name="idanggotapartner" value="<?=$getdata->row()->idanggotapartner?>" id="idanggotapartner" />

            <div class="row">
        <div class="box col-sm-12">
          <div class="box-header">
            <h2>Penerimaan Produk Yang Dipesan</h2>
          <hr>
          </div>
            <div class="row">
          <!-- <div class="box-body col-sm-12"> -->
            <!-- <p class="text-muted">Berikut adalah detail produk yang anda beli.</p> -->
<div class="col-sm-12 col-lg-12">
  <div class="form-group" style="margin-bottom: 0px">
  <h3 class="chat-content col-sm-12 text-md deep-orange text-left">Penerimaan Produk dari Penjual: <?=$getdatajoin->namaanggota?></h3>
</div>
          <div class="form-group _500" style="margin-bottom: -8px">
            <label style="margin-right: 33px">ID Transaksi</label>
            <label class="col-sm-6">: <?=$getdata->row()->idtransaksi?></label>
          </div>
          <div class="form-group _500" style="margin-bottom: -8px">
            <label style="margin-right: 0px">Tanggal Transaksi</label>
            <label class="col-sm-4">: <?=date('d M Y',strtotime($getdata->row()->tanggaltransaksi))?></label>
          </div>
<br>
<label><b>Detail Produk</b></label>
<div class="b-t b-t-dark b-t-2x"></div> <?php
$tBerat=0;
$tBiaya=0;
  foreach ($getdata2->result() as $key => $row) {
  $perproduk=getdoublewhere('idtransaksi',$idtrans,'idproduk',$row->idproduk,'tjualdetail')->row();
  $getdataproduk  =getwhere('idproduk',$row->idproduk,'manggotaproduk')->row();
$berat=($perproduk->beratpackproduk*$perproduk->jumlahjual);
  ?>
          <input type='hidden' name="idproduk[]" value="<?=$row->idproduk?>"/>
<br>
      <div class="col-lg-12">
          <div class="row">
        <div class="col-lg-3">
          <div class="form-group" style="margin-bottom: 5px">
            <img id="gambar-show" style="max-width: 270px;max-height: 270px;" src="{partner_fotoproduk}besar/<?=$getdataproduk->foto?>">
              <img hidden id="gambar-hide" src="{partner_fotoproduk}besar/<?=$getdataproduk->foto?>">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12"><b><?=$perproduk->namaproduk?> </b><sub><a data-id="<?=$row->idproduk?>" data-pesanan="<?=$perproduk->jumlahjual?>" data-toggle="modal" data-toggle-class="fade-down" data-toggle-class-target="#animate" href="javascript:void(0)" data-target="#hapus-modal" class="text-warning hapus-produk">Hapus Produk</a></sub></label>
          </div>
          <div class="form-group" style="margin-bottom: 8px">
            <label class="col-sm-12"><?=getwhere('idkategoriproduk',$perproduk->tipeproduk,'mkategoriproduk')->row()->namakategori?></label>
          </div>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12">Berat <?=$perproduk->beratpackproduk?>gram x <?=$perproduk->jumlahjual?> = <?=$berat?>gram / <?=($berat/1000)?>kg</label>
          </div>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12" style="margin-bottom: -8px">Deskripsi Produk:</label><br>
            <label class="col-sm-12"><?=$perproduk->deskripsiproduk?></label>
          </div>
        </div>
        <div class="col-sm-4">
<br>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-4">Harga</label>
            <label class="col-sm-7" style="margin-left: -45px">: Rp.<?=number_format($perproduk->hargaproduk)?></label>
          </div>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12">Pesan <?=$perproduk->jumlahjual?> buah</label>
          </div>
          <div class="form-group" style="margin-bottom: -8px;">
            <strong>
              <label class="col-sm-4">Total Bayar</label>
              <label class="col-sm-4" style="margin-left: -45px">: Rp.<?=number_format($perproduk->totaljual)?></label>
            </strong> 
          </div>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12" style="margin-bottom: -8px">Catatan Permintaan:</label><br>
            <label class="col-sm-12 text-xs">&nbsp;&nbsp;<?=($perproduk->catatanupenjual)?$perproduk->catatanupenjual:'-'?></label>
          </div>
          </div>
          </div>
      </div>
<div class="b-t b-t-dark b-t-1x"></div>
  <?php }?>
<br>
            <div class="form-group _500 row">
              <label class="col-sm-2" for="datetimepicker4">Tanggal Terima</label>
          :&nbsp;<input style="margin-top: -4px" type='text' value="<?=date('d-m-Y')?>" name="tanggalterima" class="form-control col-sm-4 text-sm" id='datetimepicker4' />
      </div>
            <div class="form-group _500 row">
              <label class="col-sm-2" for="datetimepicker4">Catatan Transaksi</label>
          :&nbsp;<textarea style="margin-top: -4px" rows="4" name="catatantransaksi" class="form-control col-sm-4 text-sm" id='catatantransaksi'></textarea>
      </div><fieldset class="form-group">
              <div class="row">
                <legend class="col-form-legend col-sm-2">Rating Transaksi</legend>:
                <div class=" ">
                  <div class="form-check">
                    <label class="md-check">
          <input type='radio' value="1" name="ratingtransaksi">
                <i class="indigo"></i>
                Buruk Sekali
              </label>
                  </div>
                  <div class="form-check">
                    <label class="md-check">
          <input type='radio' value="2" name="ratingtransaksi">
                <i class="indigo"></i>
                Buruk
              </label>
                  </div>
                  <div class="form-check">
                   <label class="md-check">
          <input type='radio' value="3" name="ratingtransaksi">
                <i class="indigo"></i>
                Cukup
              </label>
                  </div>
                  <div class="form-check">
                   <label class="md-check">
          <input type='radio' value="4" name="ratingtransaksi">
                <i class="indigo"></i>
                Bagus
              </label>
                  </div>
                  <div class="form-check">
                   <label class="md-check">
          <input type='radio' value="5" name="ratingtransaksi">
                <i class="indigo"></i>
                Bagus Sekali
              </label>
                  </div>
                </div>
              </div>
            </fieldset>
          <br> 
<!-- ############################################################################## -->

            <div class="text-left">
              <button type="submit" class="btn deep-orange faa-parent animated-hover">&nbsp; Ya, Sudah diterima &nbsp;<i class="fa fa-check faa-wrench"></i></button>
            </div>
<hr>
          </div>
    <div class="box-footer">
      <span class="text-muted"><i><font color="red">*</font>
      Laporkan jika anda mengalami masalah di sini.</i></span>
    </div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>
<?php
$awal  = new DateTime($getdata->row()->tanggaltransaksi);
$akhir = new DateTime(); // Waktu sekarang
$diff  = $awal->diff($akhir);
?>
<script type="text/javascript" src="{custom_path}areamember.js"></script>
  <script type="text/javascript">
    $(function () {
      getDatePicker('#datetimepicker4','-<?=$diff->d?>d')
    
    $(document).ready(function(){
$("#filefoto").hover(function(){
    $("#btnfe").css("background-color", "#eee");
    $(this).css("cursor", "pointer");
    }, function(){
    $("#btnfe").css("background-color", "");
});
      previewIMG('#filefoto','#previewing')
            });
    });
  </script>