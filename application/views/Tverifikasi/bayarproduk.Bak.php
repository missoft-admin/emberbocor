<div class="padding">
  <?php echo ErrorSuccess($this->session)?>
  <?php if($error != '') echo ErrorMessage($error)?>
  <script type="text/javascript">
    $(document).ready(function(){
<?php
if(!empty($_SESSION['status'])){
    echo $_SESSION['status'];
$_SESSION['status']='';
}else{
$_SESSION['status']='';
    }
    ?>
})
</script>
  <?php
  $idtrans=decryptURL($this->uri->segment(3));
  $getdata=getwhere('idtransaksi',$idtrans,'tjual');
  $getdata2=getwhere('idtransaksi',$idtrans,'tjualdetail');
  $dataAlamat =getwhere('idanggota',$_SESSION['user_id'],'manggotaalamattujuan')->result();
  ?>
<?php
$hahe=$getdata->row()->totalbayar;
foreach($getdata->result() as $row){
$idpartner=$row->idanggotapartner;
}
?>
<div class="row">
    <div class="col-sm-12">
      <form enctype="multipart/form-data" data-plugin="parsley" action="{site_url}v/verifikasi-Bayar/proses" method="post" data-option="{}">
          <input type='hidden' name="idtransaksi" value="<?=$getdata->row()->idtransaksi?>" id="idtransaksi" />
          <input type='hidden' name="__url__" value="<?=$idtrans?>"/>

            <div class="row">
        <div class="box col-sm-12">
          <div class="box-header">
            <h2>Formulir Verifikasi Pembayaran Produk</h2>
          <hr>
          </div>
            <div class="row">
              <div class="col-sm-12 col-lg-12">
                            <div class="block block-bordered">
                                <div class="block-header light b-t b-t-warning b-l b-l-warning b-b b-b-warning b-r b-r-warning">
                                    <h3 class="block-title">Berikut adalah detail produk yang anda beli</h3>
                                </div>
                                <div class="block-content b-t b-t-warning b-l b-l-warning b-b b-b-warning b-r b-r-warning ">
<h6><table width="100%">
  <tr>
              <th>
            ID Transaksi
        </th>
        <td>&nbsp;</td>
        <th width="85%">: <?=$getdata->row()->idtransaksi?></th>
      </tr>
            <tr>
              <th>
            Tanggal Transaksi
        </th>
        <td>&nbsp;</td>
        <th>: <?=date('d-M-Y',strtotime($getdata->row()->tanggaltransaksi))?>
        </th>
      </tr>
    </table></h6>
<div class="b-t b-t-dark b-t-2x"></div>
<br>
          <!-- <table width="100%" border="0"> -->
            <?php
  foreach ($getdata2->result() as $key => $row) {
  $getdatajoin    =getwhere('idanggota',$idpartner,'manggota')->row();
  $getdataproduk  =getwhere('idproduk',$row->idproduk,'manggotaproduk')->row();
    
            ?>
      <div class="col-lg-12">
          <div class="row">
        <div class="col-lg-5">
          <div class="form-group" style="margin-bottom: 5px">
            <img id="gambar-show" style="max-width: 411px;max-height: 411px;" src="{partner_fotoproduk}besar/<?=$getdataproduk->foto?>">
              <img hidden id="gambar-hide" src="{partner_fotoproduk}besar/<?=$getdataproduk->foto?>">
          </div>
        </div>
        <div class="col-lg-7"><b>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Nama Penjual</label>
            <label class="col-md-7">: <?=$getdatajoin->namaanggota?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Nama Produk</label>
            <label class="col-md-7">: <?=$getdataproduk->namaproduk?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Tipe Produk</label>
            <label class="col-md-7">: <?=getwhere('idkategoriproduk',$getdataproduk->kategoriproduk,'mkategoriproduk')->row()->namakategori?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Berat Pack Produk</label>
            <label class="col-md-7">: <?=$getdataproduk->beratpackproduk?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Jumlah yang dibeli</label>
            <label class="col-md-7">: <?=$getdata2->row()->jumlahjual?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Harga Produk</label>
            <label class="col-md-7">: Rp.<?=number_format($getdataproduk->hargaproduk)?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Deskripsi Produk</label>
            <label class="col-md-7">: <?=$getdataproduk->deskripsiproduk?></label>
          </div></b>
        </div>
          </div>
      </div>
<div class="b-t b-t-grey">&nbsp;</div>
  <?php }?>
          <!-- </table> -->
          <br>
                                </div>
                            </div>
<div class="b-t b-t-dark b-t-3x">&nbsp;</div>
                        </div>
          <div class="box-body col-sm-6">
            <p class="text-muted">Silahkan isi data dibawah ini dengan lengkap</p>

            <div class="form-group">
              <label for="datetimepicker4">Tanggal Bayar</label>
          <input type='text' name="tanggalbayar" class="form-control" id='datetimepicker4' />
      </div>
            <div class="form-group">
              <label for="nominaltransaksi">Nominal Transaksi</label>
               <div class="input-group mr-2">
  <div class="input-group-prepend">
    <div class="white input-group-text" id="nominaltransaksi"><small>Rp.</small></div>
  </div>
  <label class="form-control" aria-describedby="nominaltransaksi"><?=number_format($hahe)?></label>
  <input type="hidden" name="nominaltransaksi" value="<?=number_format($hahe)?>" id="nominaltransaksi">
</div>
            </div>
            <div class="form-group">
                    <label>Nama Bank</label>
                      <select name="namabank" id="namabank" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Bank..">
              <option></option>
              <option <?=($nbank=='BCA')?'selected':''?> value="BCA">BCA</option>
                        <option <?=($nbank=='BRI')?'selected':''?> value="BRI">BRI</option>
                        <option <?=($nbank=='MANDIRI')?'selected':''?> value="MANDIRI">MANDIRI</option>
                        <option <?=($nbank=='BNI')?'selected':''?> value="BNI">BNI</option>
                        <option <?=($nbank=='DANAMON')?'selected':''?> value="DANAMON">DANAMON</option>
                    </select>
                    </div>
                    <div class="form-group">
                    <label>Cabang Bank</label>
                    <label id="test1" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[A-Za-z ]+" id="cabangbank" hidden name="cabangbank" class="form-control" value="">
                    </div>
                    <div class="form-group">
                    <label>Nomor Rekening</label>
                    <label id="test2" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[0-9 ]+" id="norekening" hidden class="form-control" name="norekening" value="">
                    </div>
                    <div class="form-group">
                    <label>Atas Nama</label>
                    <label id="test3" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[A-Za-z ]+" id="atasnama" hidden class="form-control" name="atasnama" value="">
                    </div>

<!-- ############################################################################## -->

          </div>
          <div class="box-body col-sm-6">
            <div style="margin-top: -20px;text-align: center;">
            <font style="font-size: 15px;font-weight: ;">Foto bukti transfer</font>
            </div>
           <center>
            <div class="form-group">
<div style="height: 420px;width: 420px;display: flex;border: 1px solid #ddd;" id="image_preview">
  <img style="cursor:pointer;margin: auto;max-width:420px; max-height:420px;" id="previewing" src="{img_path}no image.png" />
</div>
<br>
<div class="form-file col-sm-10">
<input id="filefoto" name="filefotonya" accept="image/*" type="file" style="display: block;">
<button id="btnfe" class="btn white">Pilih foto bukti transfer..</button>
</div>
</div>
</center>
         </div>
         <div class="col-sm-12 col-lg-12">
<div class="b-t b-t-dark b-t-3x">&nbsp;</div>
                            <div class="block block-bordered">
                                <div class="block-header light b-t b-t-warning b-l b-l-warning b-b b-b-warning b-r b-r-warning">
                                    <h3 class="block-title">Pilih Alamat Tujuan Kirim</h3>
                                </div>
                                <div class="block-content b-t b-t-warning b-l b-l-warning b-b b-b-warning b-r b-r-warning ">
<div class="col-sm-6">
<span><b>Alamat:</b></span>
<div class="b-t b-t-accent b-t-3x"></div>
<br>
<div class="block block-bordered">
<div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
<div class="form-group">
<label>Pilih jenis alamat</label>
               <div class="input-group mr-2">
<select name="jenisalamat" id="jenisalamat" class="form-control" data-plugin="select2" data-option="{}">
<option selected value="1">Default</option>
<?php
$noal=1;
foreach ($dataAlamat as $row) {
?>
<option value="<?=$row->nourut?>">Alamat ke-<?=$noal++?></option>
<?php
}
?>
</select>
</div>
</div>
<div class="block-footer">
&nbsp;
</div>
</div>
</div>
<div class="form-group">
<label>Jalan</label>
                    <label id="inputke-5" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<textarea hidden="hidden" name="alamatanggota" class="form-control" id="alamatanggota" row="6" data-minwords="6"></textarea>
</div>
<div class="form-group">
<label>Provinsi</label>
               <div class="input-group mr-2">
<select name="namaprovinsi" id="namaprovinsi" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Provinsi..">
<option></option>
</select>
</div>
</div>
<div class="form-group">
<label>Kota</label>
               <div class="input-group mr-2">
<select name="namakota" id="namakota" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Kota..">
<option></option>
</select>
</div>
</div>
<div class="form-group">
<label>Kecamatan</label>
               <div class="input-group mr-2">
<select name="namakecamatan" id="namakecamatan" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Kecamatan..">
<option></option>
</select>
</div>
</div>
<div class="form-group">
<label>Desa</label>
               <div class="input-group mr-2">
<select name="namadesa" id="namadesa" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Desa..">
<option></option>
</select>
</div>
</div>
<div class="form-group">
<label>Kode Pos</label>
                    <label id="inputke-6" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" type="text" readonly name="kodepos" id="kodepos" value="" class="form-control">
</div>
</div>
                                </div>
                            </div>
<div class="b-t b-t-dark b-t-3x">&nbsp;</div>
          <div class="text-right">
            <button type="submit" class="btn primary">&nbsp; Verifikasi &nbsp;<i class="fa fa-check"></i></button>
          </div>
</div>
         </div>
<hr>
<div class="box-footer">
  <span class="text-muted"><i><font color="red">*</font>Laporkan jika anda mengalami masalah dengan verifikasi ini.</i></span>
</div>
         </div>
        </div>
      </form>
    </div>
  </div>
  </div>
<input type="hidden" id="idprovinsi-hidden" value="">
<input type="hidden" id="idkota-hidden" value="<?=getwhere('idanggota',$_SESSION['user_id'],'manggota')->row()->kota;?>">
<input type="hidden" id="idkecamatan-hidden" value="<?=getwhere('idanggota',$_SESSION['user_id'],'manggota')->row()->kecamatan;?>">
<input type="hidden" id="iddesa-hidden" value="<?=getwhere('idanggota',$_SESSION['user_id'],'manggota')->row()->desa;?>">
<input type="hidden" id="idkopos-hidden" value="<?=getwhere('idanggota',$_SESSION['user_id'],'manggota')->row()->kodepos;?>">
<input type="hidden" id="idjenisalamat-hidden" value="<?=getwhere('idanggota',$_SESSION['user_id'],'manggotaalamattujuan')->row()->nourut;?>">
<input type="hidden" id="alamatanggota-hidden" value="">
<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript" src="{custom_path}bayar_produk.js"></script>
  <script type="text/javascript">
    $(function () {
      getDatePicker('#datetimepicker4','-30d')
    
    $(document).ready(function(){
    // loadDataRek();
$("#filefoto").hover(function(){
    $("#btnfe").css("background-color", "#eee");
    $(this).css("cursor", "pointer");
    }, function(){
    $("#btnfe").css("background-color", "");
});
      previewIMG('#filefoto','#previewing')
            });
    });
  </script>