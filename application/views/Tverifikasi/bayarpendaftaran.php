      <div class="clearfix">
<div class="padding">
  <?php echo ErrorSuccess($this->session)?>
  <?php if($error != '') echo ErrorMessage($error)?>
<div class="row">
    <div class="col-sm-12">
      <form enctype="multipart/form-data" data-plugin="parsley" action="{site_url}v/verifikasi-Pendaftaran/proses" method="post" data-option="{}">
            <div class="row">
<?php
  if($cekdata <> 0){
$row=getwhere('idanggotamember',$_SESSION['user_id'],'tbayardaftarmember')->row();
    ?>
      <div class="box col-sm-12">
          <div class="box-header">
            <h5 class="mb-3">Formulir Bayar Pendaftaran</h5>
          <hr>
          </div>
          <div class="box-body ">
            <center>
            <h1>Sudah Terkonfirmasi</h1>
            <img src="{img_path}ceklis.png" width="360px">
            <h4>Dengan Kode Daftar Pembeli: <?=$row->idbayardaftarmember;?></h4>
            <h6>Pada tanggal: <?=$row->tanggalbayar?></h6>
          </center>
</div>
<hr>
<div class="box-footer">
  <span class="text-muted"><i><font color="red">*</font>Laporkan jika anda mengalami masalah dengan verifikasi ini.</i></span>
</div>
</div>

<?php 
  }else{
?>
        <div class="box col-sm-12">
          <div class="box-header">
            <h2>Formulir Bayar Pendaftaran</h2>
          </div>
          <hr>
            <div class="row">
          <div class="box-body col-sm-6">
            <p class="text-muted">Silahkan isi data dibawah ini dengan lengkap</p>

            <div class="form-group">
              <label>Tanggal Bayar</label>
          <input type='text' name="tanggalbayar" class="form-control" id='datetimepicker4' />
      </div>
            <div class="form-group">
              <label>Nominal Transaksi</label>
               <div class="input-group mr-2">
  <div class="input-group-prepend">
    <div class="white input-group-text" id="nominaltransaksi"><small>Rp.</small></div>
  </div>
  <label class="form-control" aria-describedby="nominaltransaksi">25,000</label>
  <input type="hidden" name="nominaltransaksi" value="25,000" id="nominaltransaksi">
</div>
            </div>
            <div class="form-group">
                    <label>Nama Bank</label>
                      <select name="namabank" id="namabank" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Bank..">
              <option></option>
              <option <?=($nbank=='BCA')?'selected':''?> value="BCA">BCA</option>
                        <option <?=($nbank=='BRI')?'selected':''?> value="BRI">BRI</option>
                        <option <?=($nbank=='MANDIRI')?'selected':''?> value="MANDIRI">MANDIRI</option>
                        <option <?=($nbank=='BNI')?'selected':''?> value="BNI">BNI</option>
                        <option <?=($nbank=='DANAMON')?'selected':''?> value="DANAMON">DANAMON</option>
                    </select>
                    </div>
                    <div class="form-group">
                    <label>Cabang Bank</label>
                    <label id="test1" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[A-Za-z ]+" id="cabangbank" hidden name="cabangbank" class="form-control" value="">
                    </div>
                    <div class="form-group">
                    <label>Nomor Rekening</label>
                    <label id="test2" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[0-9 ]+" id="norekening" hidden class="form-control" name="norekening" value="">
                    </div>
                    <div class="form-group">
                    <label>Atas Nama</label>
                    <label id="test3" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[A-Za-z ]+" id="atasnama" hidden class="form-control" name="atasnama" value="">
                    </div>

<!-- ############################################################################## -->

          </div>
          <div class="box-body col-sm-6">
            <div style="margin-top: -20px;text-align: center;">
            <font style="font-size: 15px;font-weight: ;">Foto bukti transfer</font>
            </div>
           <center>
            <div class="form-group">
<div style="height: 420px;width: 420px;display: flex;border: 1px solid #ddd;" id="image_preview">
  <img style="cursor:pointer;margin: auto;max-width:420px; max-height:420px;" id="previewing" src="{img_path}no image.png" />
</div>
<br>
<div class="form-file col-sm-10">
<input onmouseover="mouseOver()" onmouseout="mouseOut()" id="filefoto" name="filefotonya" accept="image/*" type="file" style="display: block;">
<button id="btnfe" onmouseover="mouseOver()" onmouseout="mouseOut()" class="btn white">Pilih foto bukti transfer..</button>
</div>
</div>
</center>
         </div>
         </div>
            <div class="text-right">
              <button type="submit" class="btn primary">&nbsp; Verifikasi &nbsp;<i class="fa fa-check"></i></button>
            </div>
<hr>
<div class="box-footer">
  <span class="text-muted"><i><font color="red">*</font>Laporkan jika anda mengalami masalah dengan verifikasi ini.</i></span>
</div>
         </div>
  <?php
}
?>
        </div>
      </form>
    </div>
  </div>
  </div>
  </div>
<script type="text/javascript" src="{custom_path}areamember.js"></script>
  <script type="text/javascript">
    $(function () {
      getDatePicker('#datetimepicker4','-0d')
    
    $(document).ready(function(){
      loadDataRek();
$("#filefoto").hover(function(){
    $("#btnfe").css("background-color", "#eee");
    $(this).css("cursor", "pointer");
    }, function(){
    $("#btnfe").css("background-color", "");
});
      previewIMG('#filefoto','#previewing')
            });

    function loadDataRek(){
      $.ajax({
             url: "../u/users/GETrek",
                method:"POST", 
                beforeSend:function() { 
                    // $('#loading-Gif').empty()
                    // $('#isinya').attr('hidden','hidden')
                    // $('#loading-Gif').append('<img src="../../../assets/custom/gif/ajax-loader.gif">')
                  },
             success: function(data){
                    // $('#loading-Gif').empty()
                    // $('#isinya').removeAttr('hidden')
                    $('#cabangbank').removeAttr('hidden')
                    $('#norekening').removeAttr('hidden')
                    $('#atasnama').removeAttr('hidden')
                    $('#test1').attr('hidden','hidden')
                    $('#test2').attr('hidden','hidden')
                    $('#test3').attr('hidden','hidden')
            // $("#datarekening").empty();
                  for(ii = 0;ii < data.rekening.length; ii++){
                    $('#cabangbank').val(data.rekening[ii].cabangbank)
                    $('#norekening').val(data.rekening[ii].norekening)
                    $('#atasnama').val(data.rekening[ii].atasnama)
                }
                },
                 error: function (xhr, ajaxOptions, thrownError) { 
                    console.log(xhr.responseText);
                }
             });
    }
    });
  </script>