 <?php
  $idtrans        =decryptURL($this->uri->segment(3));
  $getdata=getwhere('idtransaksi',$idtrans,'tjual');
  $getdata2=getwhere('idtransaksi',$idtrans,'tjualdetail');
  // $getdataproduk1  =getwherejoin('t2.idproduk',$getdata->idproduk,'tjual','manggotaproduk t2','t2.idanggota=t1.idanggota')->row();
foreach($getdata->result() as $row){
$idpartner=$row->idanggotapartner;
}
?>
<div class="padding">
  <?php echo ErrorSuccess($this->session)?>
  <?php if($error != '') echo ErrorMessage($error)?>
<div class="row">
    <div class="col-sm-12">
      <form enctype="multipart/form-data" data-plugin="parsley" action="{site_url}v/verifikasi-Terima/proses" method="post" data-option="{}">
          <input type='hidden' name="idtransaksi" value="<?=$getdata->row()->idtransaksi?>" id="idtransaksi" />
          <input type='hidden' name="idanggota" value="<?=$getdata->row()->idanggota?>" id="idanggota" />
          <input type='hidden' name="periodetransaksi" value="<?=$getdata->row()->periodetransaksi?>" id="periodetransaksi" />
          <input type='hidden' name="tanggaltransaksi" value="<?=$getdata->row()->tanggaltransaksi?>" id="tanggaltransaksi" />
          <input type='hidden' name="idanggotapartner" value="<?=$getdata->row()->idanggotapartner?>" id="idanggotapartner" />

            <div class="row">
        <div class="box col-sm-12">
          <div class="box-header">
            <h2>Penerimaan Produk Yang Dipesan</h2>
          <hr>
          </div>
            <div class="row">
          <!-- <div class="box-body col-sm-12"> -->
            <!-- <p class="text-muted">Berikut adalah detail produk yang anda beli.</p> -->
<div class="col-sm-12 col-lg-12">
                            <div class="block block-bordered">
                                <div class="block-header light b-t b-t-warning b-l b-l-warning b-b b-b-warning b-r b-r-warning">
                                    <h3 class="block-title">Berikut adalah detail produk yang anda beli</h3>
                                </div>
                                <div class="block-content b-t b-t-warning b-l b-l-warning b-b b-b-warning b-r b-r-warning ">
<h6>
  <table width="100%">
  <tr>
        <th>
            ID Transaksi
        </th>
        <td>&nbsp;</td>
        <th width="85%">: <?=$getdata->row()->idtransaksi?></th>
      </tr>
            <tr>
              <th>
            Tanggal Transaksi
        </th>
        <td>&nbsp;</td>
        <th>: <?=date('d-M-Y',strtotime($getdata->row()->tanggaltransaksi))?>
        </th>
      </tr>
    </table>
  </h6>
<div class="b-t b-t-dark b-t-2x"></div>
<br>
          <!-- <table width="100%" border="0"> -->
            <?php
  foreach ($getdata2->result() as $key => $row) {
  $getdatajoin    =getwhere('idanggota',$idpartner,'manggota')->row();
  $getdataproduk  =getwhere('idproduk',$row->idproduk,'manggotaproduk')->row();
    
            ?>
      <div class="col-lg-12">
          <div class="row">
        <div class="col-lg-5">
          <div class="form-group" style="margin-bottom: 5px">
            <img id="gambar-show" style="max-width: 411px;max-height: 411px;" src="{partner_fotoproduk}besar/<?=$getdataproduk->foto?>">
              <img hidden id="gambar-hide" src="{partner_fotoproduk}besar/<?=$getdataproduk->foto?>">
          </div>
        </div>
        <div class="col-lg-7"><b>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Nama Penjual</label>
            <label class="col-md-7">: <?=$getdatajoin->namaanggota?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Nama Produk</label>
            <label class="col-md-7">: <?=$getdataproduk->namaproduk?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Tipe Produk</label>
            <label class="col-md-7">: <?=getwhere('idkategoriproduk',$getdataproduk->kategoriproduk,'mkategoriproduk')->row()->namakategori?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Berat Pack Produk</label>
            <label class="col-md-7">: <?=$getdataproduk->beratpackproduk?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Jumlah yang dibeli</label>
            <label class="col-md-7">: <?=$getdata2->row()->jumlahjual?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Harga Produk</label>
            <label class="col-md-7">: Rp.<?=number_format($getdataproduk->hargaproduk)?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Total Bayar</label>
            <label class="col-md-7">: Rp.<?=number_format($getdata->row()->totalbayar)?></label>
          </div>
          <div class="form-group" style="margin-bottom: 5px">
            <label class="col-md-3">Deskripsi Produk</label>
            <label class="col-md-7">: <?=$getdataproduk->deskripsiproduk?></label>
          </div></b>
        </div>
          </div>
      </div>
<div class="b-t b-t-grey">&nbsp;</div>
  <?php }?>
          <!-- </table> -->
          <br>
                                </div>
                            </div>
<!-- ############################################################################## -->

            <div class="text-right">
              <button type="submit" class="btn primary faa-parent animated-hover">&nbsp; Ya, Sudah diterima &nbsp;<i class="fa fa-check faa-wrench"></i></button>
            </div>
<hr>
          </div>
    <div class="box-footer">
      <span class="text-muted"><i><font color="red">*</font>
      Laporkan jika anda mengalami masalah di sini.</i></span>
    </div>
</div>
</div>
</div>
</form>
</div>
</div>
</div>

<script type="text/javascript" src="{custom_path}areamember.js"></script>
  <script type="text/javascript">
    $(function () {
      getDatePicker('#datetimepicker4','-0d')
    
    $(document).ready(function(){
$("#filefoto").hover(function(){
    $("#btnfe").css("background-color", "#eee");
    $(this).css("cursor", "pointer");
    }, function(){
    $("#btnfe").css("background-color", "");
});
      previewIMG('#filefoto','#previewing')
            });
    });
  </script>