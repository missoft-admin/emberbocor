<div class="padding">
  <?php echo ErrorSuccess($this->session)?>
  <?php if($error != '') echo ErrorMessage($error)?>
  <script type="text/javascript">
    $(document).ready(function(){
<?php
if(!empty($_SESSION['status'])){
    echo $_SESSION['status'];
$_SESSION['status']='';
}else{
$_SESSION['status']='';
    }
    ?>
})
</script>
  <?php
  $idtrans=decryptURL($this->uri->segment(3));
  $getdata=getdoublewhere('idtransaksi',$idtrans,'statusproses',1,'tjual');
  if($getdata->num_rows()==0){
    redirect(site_url().'r/verifikasi-Bayar');
  }
  $dataAlamat =getwhere('idanggota',$_SESSION['user_id'],'manggotaalamattujuan')->result();
$hahe=$getdata->row()->totalbayar;
foreach($getdata->result() as $row){
$idpartner=$row->idanggotapartner;
  $getdata2=getwhere('idtransaksi',$row->idtransaksi,'tjualdetail');
  $getdatapem    =getwhere('idanggota',$row->idanggota,'manggota')->row();
  $getdatajoin    =getwhere('idanggota',$row->idanggotapartner,'manggota')->row();
}
$getekspedisi=getdoublewhere('idekspedisi',$getdata->row()->idekspedisi,'idanggota',$idpartner,'manggotaekspedisi')->row();
?>
<div class="row">
    <div class="col-sm-12">
      <form enctype="multipart/form-data" data-plugin="parsley" action="{site_url}v/verifikasi-Bayar/proses" method="post" data-option="{}">
          <input type='hidden' name="idtransaksi" value="<?=$idtrans?>" id="idtransaksi" />
          <input type='hidden' name="__url__" value="<?=$idtrans?>"/>
          <input type='hidden' name="rowTjualDetail" value="<?=$getdata2->num_rows()?>"/>

            <div class="row">
        <div class="box col-sm-12">
          <div class="box-header">
            <h2>Formulir Verifikasi Pembayaran Produk</h2>
          <hr>
          </div>
            <div class="row">
              <div class="col-sm-12 col-lg-12">
  <div class="form-group" style="margin-bottom: 0px">
  <h3 class="chat-content col-sm-12 text-md deep-orange text-left">Pembelian dari Penjual: <?=$getdatajoin->namaanggota?></h3>
</div>
          <div class="form-group _500" style="margin-bottom: -8px">
            <label style="margin-right: 33px">ID Transaksi</label>
            <label class="col-sm-6">: <?=$getdata->row()->idtransaksi?></label>
          </div>
          <div class="form-group _500" style="margin-bottom: -8px">
            <label style="margin-right: 0px">Tanggal Transaksi</label>
            <label class="col-sm-4">: <?=date('d M Y',strtotime($getdata->row()->tanggaltransaksi))?></label>
          </div>
<br>
<label><b>Detail Produk</b></label>
<div class="b-t b-t-dark b-t-2x"></div> <?php
$tBerat=0;
$tBiaya=0;
  foreach ($getdata2->result() as $key => $row) {
  $perproduk=getdoublewhere('idtransaksi',$idtrans,'idproduk',$row->idproduk,'tjualdetail')->row();
  $getdataproduk  =getwhere('idproduk',$row->idproduk,'manggotaproduk')->row();
$berat=($perproduk->beratpackproduk*$perproduk->jumlahjual);
  ?>
          <input type='hidden' name="idproduk[]" value="<?=$row->idproduk?>"/>
<br>
      <div class="col-lg-12">
          <div class="row">
        <div class="col-lg-3">
          <div class="form-group" style="margin-bottom: 5px">
            <img id="gambar-show" style="max-width: 270px;max-height: 270px;" src="{partner_fotoproduk}besar/<?=$getdataproduk->foto?>">
              <img hidden id="gambar-hide" src="{partner_fotoproduk}besar/<?=$getdataproduk->foto?>">
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12"><b><?=$perproduk->namaproduk?> </b><sub><a data-id="<?=$row->idproduk?>" data-pesanan="<?=$perproduk->jumlahjual?>" data-toggle="modal" data-toggle-class="fade-down" data-toggle-class-target="#animate" href="javascript:void(0)" data-target="#hapus-modal" class="text-warning hapus-produk">Hapus Produk</a></sub></label>
          </div>
          <div class="form-group" style="margin-bottom: 8px">
            <label class="col-sm-12"><?=getwhere('idkategoriproduk',$perproduk->tipeproduk,'mkategoriproduk')->row()->namakategori?></label>
          </div>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12">Berat <?=$perproduk->beratpackproduk?>gram x <?=$perproduk->jumlahjual?> = <?=$berat?>gram / <?=($berat/1000)?>kg</label>
          </div>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12" style="margin-bottom: -8px">Deskripsi Produk:</label><br>
            <label class="col-sm-12"><?=$perproduk->deskripsiproduk?></label>
          </div>
        </div>
        <div class="col-sm-4">
<br>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-4">Harga</label>
            <label class="col-sm-7" style="margin-left: -45px">: Rp.<?=number_format($perproduk->hargaproduk)?></label>
          </div>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12">Pesan <?=$perproduk->jumlahjual?> buah</label>
          </div>
          <div class="form-group" style="margin-bottom: -8px;">
            <strong>
              <label class="col-sm-4">Total Bayar</label>
              <label class="col-sm-4" style="margin-left: -45px">: Rp.<?=number_format($perproduk->totaljual)?></label>
            </strong> 
          </div>
          <div class="form-group" style="margin-bottom: -8px">
            <label class="col-sm-12" style="margin-bottom: -8px">Catatan Permintaan:</label><br>
            <textarea name="catatanpermintaan[]" style="margin-left: 20px" class="form-control col-sm-12" id="catatanpermintaan" row="6" data-minwords="6"><?=(!empty($perproduk->catatanupenjual))?$perproduk->catatanupenjual:'-'?></textarea>
          </div>
          </div>
          </div>
      </div>
<div class="b-t b-t-dark b-t-1x"></div>
  <?php
$tBerat+=$berat;
$tBiaya+=$perproduk->totaljual;
}?>
<br>
<!-- <label><b>Detail Produk</b></label> -->
<!-- <div class="b-t b-t-dark b-t-2x">&nbsp;</div> -->
<!-- ############################################################################## -->
<!-- Alamat, ekspedisi, dan harga -->
<div class="row">
<!-- Kanan -->
<div class="col-sm-6">
<div class="form-group">
  <label><b>Alamat Tujuan:</b></label><br>
  <label id="detailalamat-show"></label><br>
  <label style="margin-bottom: 0px" id="detailalamat-show2"></label><br>
    <a data-toggle="modal" data-toggle-class="fade-down" data-toggle-class-target="#animate" href="javascript:void(0)" class="text-warning" data-target="#alamat-modal"><small>Ganti Alamat</small></a>
</div>
<div style="margin-bottom: -10px" class="form-group row">
  <label class="col-sm-4">Total Berat</label>
  <label>: <?=$tBerat?>gram / <?=($tBerat/1000)?>kg</label>
</div>
<div style="margin-bottom: -10px" class="form-group row">
  <label class="col-sm-4">Total Biaya Produk</label>
  <label>: Rp.<?=number_format($tBiaya)?></label>
</div>
<div style="margin-bottom: -10px" class="form-group row">
  <label class="col-sm-4">Biaya Ekspedisi</label>
  <label>:<u> Rp.<?=number_format($getdata->row()->biayaekspedisi)?><?=str_repeat("&nbsp;", 10);?></u></label>
</div>
<?php
$TBiaya=($tBiaya+$getdata->row()->biayaekspedisi);
?>
<div class="form-group row _500">
  <label class="col-sm-4">Total Biaya</label>
  <label>: Rp.<?=number_format($TBiaya)?></label>
  <input type="hidden" name="nominaltransaksi" value="<?=$TBiaya?>">
</div>
</div>
<!-- Kiri -->
<div class="col-sm-6">
  <div class="form-group">
    <label><b>Ekspedisi:</b></label><br>
    <label id="ekspedisi-show"><?=$getekspedisi->namaekspedisi?></label>
    <!-- <input type="hidden" name="idekspedisi" id="ekspedisi-hide" value="<?=$getekspedisi->idekspedisi?>"> -->
    <div id="selectEkspedisi" style="display: none">
      <select name="ekspedisi" style="width: 100%" id="ekspedisi" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Ekspedisi..">
        <option></option>
<?php
$sEkspedisi=getdoublewhere('idanggota',$idpartner,'staktif',1,'manggotaekspedisi');
foreach ($sEkspedisi->result() as $show) {
?>
        <option value="<?=$show->idekspedisi?>"><?=$show->namaekspedisi?></option>
<?php
}
?>
    </select></div>
  </div>
    <a href="javascript:void(0)" class="text-warning" data-target="#ganti-ekspedisi" id="ganti-ekspedisi"><small>Ganti Ekspedisi</small></a>
</div>
</div>
<br>
<label><b>Detail Bukti Transfer</b></label>
<div class="b-t b-t-dark b-t-2x">&nbsp;</div>
                        </div>
          <div class="col-sm-5">
            <div class="form-group">
<div style="height: 400px;width: 400px;display: flex;border: 1px solid #ddd;" tooltip="Klik Untuk Pilih Foto" id="image_preview">
  <img style="cursor:pointer;margin: auto;max-width:400px; max-height:400px;" id="previewing" src="{img_path}no image.png" />
</div>
<br>
<div class="form-file col-sm-10">
<input id="filefoto" name="filefotonya" accept="image/*" type="file" required style="display: block;">
<!-- <button id="btnfe" class="btn white">Pilih foto bukti transfer..</button> -->
</div>
</div> 
         </div>
          <div class="col-sm-6" style="margin-left: -50px">
            <div class="form-group row" style="margin-bottom: 10px">
              <label class="col-sm-3 col-form-label" for="datetimepicker4">Tanggal Bayar</label>
                    <div class="row col-sm-9">
          <input type='text' value="<?=date('d-m-Y')?>" name="tanggalbayar" class="form-control" id='datetimepicker4' />
      </div>
      </div>

            <div class="form-group row" style="margin-bottom: -5px">
                    <label class="col-sm-3">Bank</label>
                    <div class="row col-sm-9 testlagi">
                    <label id="bank-show">Test BCA</label>
                  </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: -5px">
                    <label class="col-sm-3">Nomor Rekening</label>
                    <div class="row col-sm-9 testlagi">
                    <label id="norekening-show">Test BCA</label>
                  </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: -5px">
                    <label class="col-sm-3">Atas Nama</label>
                    <div class="row col-sm-9 testlagi">
                    <label id="atasnama-show">Test BCA</label>
                  </div>
                    </div>
                    <div class="form-group row" style="margin-bottom: 5px">
                    <label class="col-sm-3">Cabang Bank</label>
                    <div class="row col-sm-9 testlagi">
                    <label id="cabangbank-show">Test BCA</label>
                  </div>
                    </div>
    <a href="javascript:void(0)" class="text-warning" data-toggle="modal" data-toggle-class="fade-down" data-toggle-class-target="#animate" data-target="#bank-modal"><small>Ganti Bank</small></a>
<!-- ############################################################################## -->
<?=str_repeat('<br>',11)?>
          <div class="text-left">
            <button type="submit" class="btn deep-orange faa-parent animated-hover">&nbsp; VERIFIKASI BUKTI TRANSFER &nbsp;<i class="fa fa-check faa-wrench"></i></button>
          </div>
          </div>
         </div>
<hr>
<div class="box-footer">
  <span class="text-muted"><i><font color="red">*</font>Laporkan jika anda mengalami masalah dengan verifikasi ini.</i></span>
</div>
         </div>
        </div>

        <!-- .modal alamat -->
        <div id="alamat-modal" class="modal black-overlay fade" data-backdrop="false">
          <div class="modal-dialog modal-lg animate" id="animate">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Pilih Alamat Tujuan Kirim</h5>
              </div>
              <div class="modal-body">
<span><b>Alamat:</b></span>
<div class="b-t b-t-warning b-t-2x"></div>
<br> 
<div class="form-group">
<label>Pilih alamat</label>
               <div class="">
<select name="jenisalamat" id="jenisalamat" class="form-control" data-plugin="select2" style="width:100%"  data-option="{}">
<option selected value="0">Default</option>
<?php
$noal=1;
foreach ($dataAlamat as $row) {
?>
<option value="<?=$row->nourut?>"><?=getrow('id',$row->provinsi,'mprovinsi')->nama.'#'.getrow('id',$row->kota,'mkota')->nama.'#'.getrow('id',$row->kecamatan,'mkecamatan')->nama.'#'.getrow('id',$row->desa,'mdesa')->nama?></option>
<?php
}
?>
</select>
</div>
</div> 
<div class="form-group">
<label>Jalan</label>
                    <label id="inputke-5" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<textarea hidden="hidden" disabled name="alamatanggota" class="form-control" id="alamatanggota" row="6" data-minwords="6"></textarea>
</div>
<div class="row">
<div class="col-sm-6">

<div class="form-group">
<label>Provinsi</label>
               <div class="">
<select name="namaprovinsi" id="namaprovinsi" class="form-control" data-plugin="select2" style="width:100%" disabled data-option="{}" data-placeholder="Pilih Provinsi..">
<option></option>
</select>
</div>
</div>
<div class="form-group">
<label>Kota</label>
               <div class="">
<select name="namakota" id="namakota" class="form-control" data-plugin="select2" style="width:100%" disabled data-option="{}" data-placeholder="Pilih Kota..">
<option></option>
</select>
</div>
</div>
<div class="form-group">
<label>Kecamatan</label>
               <div class="">
<select name="namakecamatan" id="namakecamatan" class="form-control" data-plugin="select2" style="width:100%" disabled data-option="{}" data-placeholder="Pilih Kecamatan..">
<option></option>
</select>
</div>
</div>
</div>
<div class="col-sm-6">
<div class="form-group">
<label>Desa</label>
               <div class="">
<select name="namadesa" id="namadesa" class="form-control" data-plugin="select2" style="width:100%" disabled data-option="{}" data-placeholder="Pilih Desa..">
<option></option>
</select>
</div>
</div>
<div class="form-group">
<label>Kode Pos</label>
                    <label id="inputke-6" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" type="text" disabled name="kodepos" id="kodepos" value="" class="form-control">
</div>  
</div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Batal</button>
                <button type="button" class="btn deep-orange p-x-md" id="simpan-newalamat" data-dismiss="modal">Simpan</button>
              </div>
            </div><!-- /.modal-content -->
          </div>
        </div>
        </div>
        <!-- / .modal alamat -->

        <!-- .modal Bank -->
        <div id="bank-modal" class="modal black-overlay fade custom-middle" data-backdrop="false">
          <div class="modal-dialog modal-sm animate middle" style="width: 100%" id="animate">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Nomor Rekening Baru</h5>
              </div>
              <div class="modal-body">
            <div class="form-group">
                    <label>Nama Bank</label>
                      <select name="namabank" style="width: 100%" id="namabank" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Bank..">
              <option></option>
              <option <?=($nbank=='BCA')?'selected':''?> value="BCA">BCA</option>
                        <option <?=($nbank=='BRI')?'selected':''?> value="BRI">BRI</option>
                        <option <?=($nbank=='MANDIRI')?'selected':''?> value="MANDIRI">MANDIRI</option>
                        <option <?=($nbank=='BNI')?'selected':''?> value="BNI">BNI</option>
                        <option <?=($nbank=='DANAMON')?'selected':''?> value="DANAMON">DANAMON</option>
                    </select>
                    </div>
                    <div class="form-group">
                    <label>Nomor Rekening</label>
                    <label id="test2" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[0-9 ]+" id="norekening" hidden class="form-control" name="norekening" value="">
                    </div>
                    <div class="form-group">
                    <label>Atas Nama</label>
                    <label id="test3" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[A-Za-z ]+" id="atasnama" hidden class="form-control" name="atasnama" value="">
                    </div>
                    <div class="form-group">
                    <label>Cabang Bank</label>
                    <label id="test1" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[A-Za-z ]+" id="cabangbank" hidden name="cabangbank" class="form-control" value="">
                    </div>
              <div class="modal-footer">
                <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Batal</button>
                <button type="button" class="btn deep-orange p-x-md" id="simpan-newbank" data-dismiss="modal">Simpan</button>
              </div>
            </div><!-- /.modal-content -->
          </div>
        </div>
        </div>
        <!-- / .modal bank -->

        <!-- .modal Batal Produk -->
        <div id="hapus-modal" class="modal black-overlay fade custom-middle" data-backdrop="false">
          <div class="modal-dialog modal-sm animate middle" style="width: 100%" id="animate">
            <div class="modal-content">
              <div class="modal-body">
            <div class="form-group text-center _500">
                    <input type="hidden" name="batalberapa" min="0" onkeypress="return false" id="batalberapa" class="form-control">
                    <label><i class="fa fa-exclamation-circle text-warning" style="font-size: 75px;" aria-hidden="true"></i></label>
                  <label style="font-size: 15px;">Yakin ingin membatalkan produk ini?</label>
                  </div>
              <div class="modal-footer" style="margin-bottom: -8%">
                <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Tidak Jadi</button>
                <?=str_repeat('&nbsp;',6)?>
                <button type="button" class="btn deep-orange p-x-md bataldong" data-dismiss="modal">Batalkan Pesanan</button>
              </div>
            </div><!-- /.modal-content -->
          </div>
        </div>
        </div>
        <!-- / .modal Batal Produk -->

      </form>
    </div>
  </div>
  </div>
<input type="hidden" id="idprovinsi-hidden" value="">
<input type="hidden" id="idkota-hidden" value="<?=$getdatapem->kota;?>">
<input type="hidden" id="idkecamatan-hidden" value="<?=$getdatapem->kecamatan;?>">
<input type="hidden" id="iddesa-hidden" value="<?=$getdatapem->desa;?>">
<input type="hidden" id="idkopos-hidden" value="<?=$getdatapem->kodepos;?>">
<input type="hidden" id="idjenisalamat-hidden" value="<?=getwhere('idanggota',$_SESSION['user_id'],'manggotaalamattujuan')->row()->nourut;?>">
<input type="hidden" id="alamatanggota-hidden" value="">
<?php
$awal  = new DateTime($getdata->row()->tanggaltransaksi);
$akhir = new DateTime(); // Waktu sekarang
$diff  = $awal->diff($akhir);
?>
<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript" src="{custom_path}bayar_produk.js"></script>
  <script type="text/javascript">
    $(function () {
      getDatePicker('#datetimepicker4','-<?=$diff->d?>d')
    
    function al2($nmprov,$nmkota,$nmdesa,$kopos){
        $('#detailalamat-show2').html($nmprov+', '+$nmkota+', '+$nmdesa+', '+$kopos)
      }
      
    $(document).ready(function(){

    $nmprov='<?=getrow("id",$getdatapem->provinsi,"mprovinsi")->nama?>';
    $nmkota='<?=getrow("id",$getdatapem->kota,"mkota")->nama?>';
    $nmdesa='<?=getrow("id",$getdatapem->desa,"mdesa")->nama?>';
    $kopos ='<?=$getdatapem->kodepos?>';
    al2($nmprov,$nmkota,$nmdesa,$kopos)
$("#image_preview").hover(function(){
    $("#btnfe").css("background-color", "#eee");
    $(this).css("cursor", "pointer");
    }, function(){
    $("#btnfe").css("background-color", "");
});
      previewIMG('#filefoto','#previewing')
      $('#image_preview').click(function(){
        $("#filefoto").click()
      })
      $('#ganti-ekspedisi').click(function(){
        $me=$(this);
 $me.toggleClass('off');
    if($me.is(".off")){ 
$me.html('Batal')
        $("#ekspedisi-show").css('display','none')
        $("#selectEkspedisi").css('display','block')
    }else {
        $("#ekspedisi-show").css('display','block')
        $("#selectEkspedisi").css('display','none')
$me.html('Ganti Ekspedisi') 
    }
      })
      $('#simpan-newalamat').click(function(){
        $('#detailalamat-show').html($('#alamatanggota-hidden').val())
    $nmprov=$('#namaprovinsi option:selected').text();
    $nmkota=$('#namakota option:selected').text();
    $nmdesa=$('#namadesa option:selected').text();
    $kopos =$('#kodepos').val();
        al2($nmprov,$nmkota,$nmdesa,$kopos)
      })
      $('.hapus-produk').click(function(){
        var tPesanan=$(this).attr('data-pesanan');
        var id=$(this).attr('data-id');
        $('#batalberapa').val(tPesanan)
        // $('#batalberapa').attr('max',tPesanan)
        $('#batalberapa').attr('data-id',id)
      })
      $('.bataldong').click(function(){
        var A=$('#batalberapa').val();
        var B=$('#batalberapa').attr('data-id');
        $.ajax({
             url:"../../v/verifikasi-Bayar/BHpesanan",
                method:"POST",
                data:{
                  jumlahpesan:A,
                  idproduk   :B,
                  idtransaksi:$("#idtransaksi").val()
                },
                beforeSend:function() {

                  },
             success: function(data){
              if(data==0){
                window.location.href ='{base_url}r/verifikasi-Bayar';
              }else{
               ToastrSukses("Produk berhasil dihapus","Info")             
                  setTimeout(function(){ 
                    location.reload(true)
                   }, 2500);
                }
                }
              })
      })
      $("#selectEkspedisi").change(function(){
        $.ajax({
             url:"../../v/verifikasi-Bayar/gantiEkspedisi",
                method:"POST",
                data:{
                  idekspedisi:$("#ekspedisi option:selected").val(),
                  idtransaksi:$("#idtransaksi").val()
                },
                beforeSend:function() {

                  },
             success: function(){
        $('#ganti-ekspedisi').html('Ganti Ekspedisi')
        $('#ganti-ekspedisi').toggleClass('off');
        $("#ekspedisi-show").css('display','block')
        $("#selectEkspedisi").css('display','none')
        $("#ekspedisi-show").html($("#ekspedisi option:selected").text())                  
                }
              })
        // $("#ekspedisi-hide").val($("#ekspedisi option:selected").val())
      })
            });
    });
  </script>