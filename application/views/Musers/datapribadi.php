<style type="text/css" media="screen">
  .map_canvass {
    width: 100%;
    height: 250px;
  }
  .map_canvas {
    width: 100%;
    height: 250px;
  }
    .img-container img {
      max-width: 100%;
    } 

</style>
<div class="padding">
    <?php echo ErrorSuccess($this->session)?>
  <?php if($error != '') echo ErrorMessage($error)?>
  <script type="text/javascript">
    $(document).ready(function(){
<?php
if(!empty($_SESSION['status'])){
    echo $_SESSION['status'];
$_SESSION['status']='';
}else{
$_SESSION['status']='';
    }

$getAlTu=getwhere('idanggota',$_SESSION['user_id'],'manggotaalamattujuan');
    ?>
})
</script>
<div class="row box">
  <div class="col-sm-12">
    <div class="box-header">
      <h5 class="mb-3">Pengaturan Data Pribadi</h5>
      <hr>
    </div>
    <?=form_open_multipart('s/users/upDATA','role="form" method="post"')?>
    <div class="col-sm-12 row">
    <div class="col-sm-6">
<!-- input biodata Start -->
<input type="hidden" id="fpmemberold-hidden" name="fpmemberold-hidden" value="<?=($fotomember!=='')?'{fotomember}':'0' ?>">
            <input type="hidden" name="datauptipe" value="0"/>
            <input type="hidden" name="upfotoga" id="upfotoga" value="0"/>
            <input type="hidden" id="resheight" name="resheight" />
            <input type="hidden" id="reswidth" name="reswidth" />
            <input type="hidden" id="x_axis" name="x_axis" />
            <input type="hidden" id="y_axis" name="y_axis" />
              <!-- <div id="dataprofil"> -->
                <span><b>Biodata:</b></span>
                <center>
                  <div class="form-group">
<div class=" b-t b-t-warning b-t-3x"></div>
                    <br>
                  <label><u>Foto Profil</u></label>
                  <div style="cursor:pointer;height: 360px;max-width:360px;width:360px; max-height:360px;border: 1px solid #ddd;display: flex;" id="image_preview" data-toggle="modal" data-target="#m-md">
  <img style="margin: auto;" id="imgke-1" src="{custom_path}gif/ajax-loader.gif">
  <img onerror="imgError(this);" hidden="hidden" style="cursor:pointer;margin: auto;max-width:360px; max-height:360px;" id="previewing" src="{custom_path}gif/ajax-loader.gif" class="lazyIMG img-preview" data-src="{fpmember_path}<?=(!empty($fotomember))?'{fotomember}':'{defaultimg}'?>" />
                  </div>
                  <?=br(1);?>
                  <button type="button" class="btn white" data-toggle="modal" data-target="#m-md">Unggah Foto</button>
              </div>
            </center>
<div class="b-t b-t-warning b-t-2x"></div>
<br>
<div class="form-group">
<label>ID Pembeli</label>
                    <label id="inputke-10" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<label hidden="hidden" style="background: #ededed" class="form-control" id="idpembeli"></label>
<input type="hidden" name="idpembeli" id="idpembeli-hide">
</div>
<div class="form-group">
<label>Tanggal Daftar</label>
                    <label id="inputke-11" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<label hidden="hidden" style="background: #ededed" class="form-control" id="tgldaftar"></label>
<input type="hidden" name="tgldaftar" id="tgldaftar-hide">
</div>
<div class="form-group">
<label>Jenis Pembeli</label>
                    <label id="inputke-12" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<label hidden="hidden" class="form-control" style="background: #ededed" id="jenispembeli"></label>
<input type="hidden" name="jenispembeli" id="jenispembeli-hide">
</div>
<div class="form-group">
<label>No. KTP</label>
                    <label id="inputke-1" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<label hidden="hidden" style="background: #ededed" class="form-control" id="noktp"></label>
<input type="hidden" name="noktp" id="noktp-hide">
</div>
<div class="form-group">
<label>Nama Lengkap</label>
                    <label id="inputke-2" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" pattern="[A-Za-z ]+" type="text" name="namaanggota" id="namaanggota" value="" class="form-control">
</div>
<div class="form-group">
<label>Tempat Lahir</label>
                    <label id="inputke-3" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" pattern="[A-Za-z ]+" type="text" name="tempatlahir" id="tempatlahir" value="" class="form-control">
</div>
<div class="form-group">
<label>Tanggal Lahir</label>
                    <label id="inputke-4" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" type="text" name="tanggallahir" id="tanggallahir" value="" class="form-control">
</div>
<div class="form-group">
<label>Agama</label>
<select name="agamaanggota" id="agamaanggota" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Agama..">
<option></option>
<option <?=($nagama=='Islam')?'selected':''?> value="Islam">Islam</option>
<option <?=($nagama=='Kristen')?'selected':''?> value="Kristen">Kristen</option>
<option <?=($nagama=='Katolik')?'selected':''?> value="Katolik">Katolik</option>
<option <?=($nagama=='Budha')?'selected':''?> value="Budha">Budha</option>
<option <?=($nagama=='Hindu')?'selected':''?> value="Hindu">Hindu</option>
</select>
</div>
<div class="form-group">
<label>Jenis Kelamin</label><br>
<label class="radio-inline ui-check">
<input type="radio" id="jklakilaki" name="jeniskelamin" value="1">
<i class="dark-white"></i>
Laki-laki
</label>
<label class="radio-inline ui-check">
<input type="radio" id="jkperempuan" name="jeniskelamin" value="2">
<i class="dark-white"></i>
Perempuan
</label>
</div>
<div class="form-group">
<label>Status Pernikahan</label>
<select name="stkawin" id="stkawin" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Status..">

</select>
</div>
<div class="form-group">
<label>Nama Ahli Waris</label>
                    <label id="inputke-13" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" pattern="[A-Za-z ]+" type="text" name="nmahliwaris" id="nmahliwaris" value="" class="form-control">
</div>
<div class="form-group">
<label>Hubungan Ahli Waris</label>
<select name="hubahliwaris" id="hubahliwaris" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Hubungan Ahli Waris..">
<option></option>
<option <?=($hubahliwaris=='1')?'selected':''?> value="1">Ayah</option>
<option <?=($hubahliwaris=='2')?'selected':''?> value="2">Ibu</option>
<option <?=($hubahliwaris=='3')?'selected':''?> value="3">Suami</option>
<option <?=($hubahliwaris=='4')?'selected':''?> value="4">Istri</option> 
<option <?=($hubahliwaris=='5')?'selected':''?> value="5">Anak</option>
</select>
</div>
<div class="form-group">
<label>Email</label>
                    <label id="inputke-14" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" pattern="[^@\s]+@[^@\s]+" type="email" name="dataemail" id="dataemail" value="" class="form-control">
</div>
<div class="form-group">
<label>Sponsor</label>
                    <label id="inputke-15" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<label hidden="hidden" style="background: #ededed" class="form-control" id="idsponsor"></label>
<input type="hidden" name="idsponsor" id="idsponsor-hide">
<!-- <input type="text" name="val-blob" id="val-blob"> -->
</div>
<!-- <div class="form-group">
<label>Status Pernikahan</label>
<input type="text" name="namaanggota" id="namaanggota" value="" class="form-control">
</div> -->

<!-- End input biodata -->

        <!-- large modal -->
        <div id="m-md" class="modal fade black-overlay" data-backdrop="false">
          <div class="modal-dialog animate" id="animate" style="width: 100%" data-class="fade-down">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Upload Foto Profil</h5>
              </div>
              <div class="modal-body text-center">
                <div class="row">
      <div class="col-md-12">
        <!-- <h3>Demo:</h3> -->
        <div class="img-container">
          <img id="image" class="cropper-hidden" alt="Picture">
          <div id="nofoto">
           <?=br(4)?>
          <font class="text-mute" style="font-weight: bold;font-size: 140px;">1</font>
          <font class="text-mute" style="font-weight: bold;font-size: 112px;">:</font>
          <font class="text-mute" style="font-weight: bold;font-size: 140px;">1</font>
        </div>
        </div>
      </div>
      
    </div>
   <!--  <div class="row">
    </div> -->
              </div>
              <div class="modal-footer">
       
      <div class="col-md-12 docs-buttons">
        <!-- <h3>Toolbar:</h3> -->

        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="Zoom In">
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
              <span class="fa fa-search-plus"></span> Perbesar
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="Zoom Out">
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
              <span class="fa fa-search-minus"></span> Perkecil
            </span>
          </button>
        </div>

        <div class="btn-group"> 
          <!-- <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
            <input type="file" class="sr-only" id="inputImage" name="user_file" accept="image/*">
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="">
              <span class="fa fa-upload"></span> Pilih file
            </span>
          </label> -->
          <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
            <input type="file" class="sr-only" id="inputImage" name="user_file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
            <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="Pilih foto anda">
              <span class="fa fa-upload"></span> Pilih file
            </span>
          </label>
        </div>
   
 <!-- <div class="text-right"> -->
                <div class="btn-group btn-group-crop">
        <!-- <button type="button" class="btn dark-white p-x-md" data-dismiss="modal">Tidak jadi</button> -->

          <button type="button" class="btn dark-white p-x-md" data-method="reset" data-dismiss="modal">
            <span class="docs-tooltip" data-toggle="tooltip" title="Tidak Jadi">
              <!-- <span class="fa fa-refresh"></span> -->Tidak jadi
            </span>
          </button> 
          <button type="button" data-dismiss="modal" class="btn btn-success" id="btn-modalFoto" disabled data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false">
              Selesai
            </span>
          </button>
        </div>
 <!-- </div> -->
      </div><!-- /.docs-toggles -->         
              </div>
            </div><!-- /.modal-content -->
          </div>
        </div>
              </div>
              <div class="col-sm-6">
<!-- input alamat Start -->
<span><b>Alamat:</b></span>
<div class="b-t b-t-warning b-t-3x"></div>
<br>
        <div class="clone-alamat row">
<!-- <div class="row col-sm-6"> -->
<div class="col-sm-6">
<div class="form-group">
<label>Provinsi</label>
<select name="namaprovinsi" id="namaprovinsi" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Provinsi..">
<option></option>
</select>
</div>
<div class="form-group">
<label>Kecamatan</label>
<select name="namakecamatan" id="namakecamatan" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Kecamatan..">
<option></option>
</select>
</div>
<div class="form-group">
<label>Kode Pos</label>
                    <label id="inputke-6" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" pattern="[0-9]+" type="text" readonly name="kodepos" id="kodepos" value="" class="form-control">
</div>
</div>
<div class="col-sm-6">
  
<div class="form-group">
<label>Kota</label>
<select name="namakota" id="namakota" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Kota..">
<option></option>
</select>
</div>

<div class="form-group">
<label>Desa</label>
<select name="namadesa" id="namadesa" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Desa..">
<option></option>
</select>
</div>
<!-- </div> -->
</div>
              </div> 
                  <div class="map_canvas"></div>
                  Google Maps 
              <br>
               <div class="row">
              <div class="col-sm-12" style="margin-right: -25px;">
                <?  ($lat && $lon) ? $valueMaps = '{lat},{lon}' : $valueMaps = 'Bandung, Bandung City, West Java, Indonesia'; ?> 
        <div class="input-group"> 
            <?= form_input('geocomplete', $valueMaps, 'class="form-control mb-1 col-sm-12" id="geocomplete"') ?>
          <span class="input-group-btn">
            <button id="find" class="btn warning" type="button">Cari</button>
          </span>
        </div> 
    </div>
      <div class="col-sm-6" style="margin-right: 0px;">
              <div class="form-group">
<input type="text" readonly name="lat" id="lat" value="{lat}" class="form-control">
              </div>
              </div>
      <div class="col-sm-6">
              <div class="form-group"> 
<input type="text" readonly name="lng" id="lng" value="{lon}" class="form-control">
              </div>
              </div> 
      <!-- </div> -->
      </div>
<div class="form-group">
<label>Jalan</label>
                    <label id="inputke-5" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<textarea rows="6" hidden="hidden" name="alamatanggota" class="form-control" id="alamatanggota" row="6" data-minwords="6"></textarea>
</div>
<br>
<span><b>Contact Person:</b></span>
<div class="b-t b-t-warning b-t-3x"></div>
<br>
<div class="form-group">
<label><i class="fa fa-phone"></i> No. Telepon</label>
                    <label id="inputke-7" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" pattern="[0-9]+" onkeypress="return hanyaAngka(this)" maxlength="12" type="text" name="notelp" id="notelp" value="" class="form-control">
</div>
<div class="form-group">
<label><i class="fas fa-mobile-alt"></i> No. HP</label>
                    <label id="inputke-8" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" pattern="[0-9]+" onkeypress="return hanyaAngka(this)" maxlength="12" type="text" name="nohp" id="nohp" value="" class="form-control">
</div>
<div class="form-group">
<label><i class="fas fa-mobile-alt"></i> No. HP (Whatsapp)</label>
                    <label id="inputke-9" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" pattern="[0-9]+" onkeypress="return hanyaAngka(this)" maxlength="12" type="text" name="nohpwa" id="nohpwa" value="" class="form-control">
</div>
<div class="form-group">
<label><i class="fab fa-facebook"></i> Facebook</label>
                    <label id="inputke-16" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" type="text" name="idfacebook" id="idfacebook" value="" class="form-control">
</div>
<div class="form-group">
<label><i class="fab fa-twitter-square"></i> Twitter</label>
                    <label id="inputke-17" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
<input hidden="hidden" type="text" name="idtwitter" id="idtwitter" value="" class="form-control">
</div>
<!-- input alamat End -->
          </div>
    </div>
<div class="b-b b-b-dark b-b-3x"></div>
<br>
<div class="col-lg-12">
<button type="button" style="width: 15%" class="btn info animated-hover faa-parent m-t tambah-alamat">Tambah Alamat Lain&nbsp;<i class="fa fa-plus faa-tada"></i></button>
<div class="block block-bordered more-alamat" style="display: none;">
<div class="block-header light b-t b-t-info b-l b-l-info b-b b-b-info b-r b-r-info">
<h3 class="block-title">Tambah Alamat</h3>
</div>
<div class="block-content b-t b-t-info b-l b-l-info b-b b-b-info b-r b-r-info ">
<span><b>Alamat:</b></span>
<div class="b-t b-t-info b-t-3x"></div>
<br>
<div class="block block-bordered">
<div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
<div class="form-group">
<label>Pilih Alamat Anda:</label>
               <div class="input-group mr-2">
<select style="width: 100%;" name="jenisalamat" id="jenisalamat" class="form-control" data-plugin="select2" data-option="{}">
</select>
</div>
</div>
<div class="block-footer">
&nbsp;
</div>
</div>
</div>
<div class="row">
<div class="col-sm-12 row">
<div class="form-group col-sm-2">
<label>Provinsi</label>
               <div class="input-group mr-2">
<select style="width: 100%;" name="namaprovinsi2" id="namaprovinsi2" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Provinsi..">
<option></option>
</select>
</div>
</div>
<div class="form-group col-sm-2">
<label>Kota</label>
               <div class="input-group mr-2">
<select style="width: 100%;" name="namakota2" id="namakota2" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Kota..">
<option></option>
</select>
</div>
</div>
<!-- </div> -->
<!-- <div class="col-sm-6"> -->
<div class="form-group col-sm-2">
<label>Kecamatan</label>
               <div class="input-group mr-2">
<select style="width: 100%;" name="namakecamatan2" id="namakecamatan2" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Kecamatan..">
<option></option>
</select>
</div>
</div>
<div class="form-group col-sm-2">
<label>Desa</label>
               <div class="input-group mr-2">
<select style="width: 100%;" name="namadesa2" id="namadesa2" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Desa..">
<option></option>
</select>
</div>
</div>
<div class="form-group col-sm-2">
<label>Kode Pos</label>
                    <!-- <label id="inputke-6" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label> -->
<input type="text" readonly name="kodepos2" id="kodepos2" value="" class="form-control">
</div>
</div>
      <div class="col-sm-12">
          <div class="map_canvass"></div>
          Google Maps 
      <br> 
      <!-- <div class="col-sm-12" style="margin-right: -25px;"> -->
        <? $valueMaps = 'Cimahi, Cimahi City, West Java, Indonesia'; ?> 
      <div class="row">
      <div class=" col-sm-6" style="margin-right:0px;">
        <div class="input-group"> 
            <?= form_input('geocomplete2', $valueMaps, 'class="form-control mb-1 col-sm-12" id="geocomplete2"') ?>
          <span class="input-group-btn">
            <button id="finds" class="btn warning" type="button">Cari</button>
          </span>
        </div> 
        </div> 
      <div class="col-sm-3" style="margin-right:0px;">
        <div class="form-group">
          <input type="text" readonly name="lat2" id="lat2" value="-6.884113554464287" class="form-control">
        </div>
       </div>
      <div class="col-sm-3">
        <div class="form-group"> 
          <input type="text" readonly name="lng2" id="lng2" value="107.54129317116394" class="form-control">
        </div>
      </div>
              <!-- </div>  -->
      <!-- </div> -->
      </div>
<div class="form-group">
<label>Jalan</label>
        <!-- <input name="formatted_address" type="text" value=""> -->
                    <!-- <label id="inputke-5" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label> -->
<!-- <input type="text" name="alamatanggota2" id="alamatanggota2" class="form-control"> -->
<textarea rows="5" name="alamatanggota2" class="form-control" id="alamatanggota2" row="6" data-minwords="6"></textarea>
</div>
    <div class="text-left">
          <label class="md-check" id="c_box" hidden>
              <input type="checkbox" name="jadikandefault" id="jadikandefault" value="1">
                <i class="blue"></i>Jadikan alamat utama
              </label>
              <br>
              <br>
<button hidden style="width: 12%" type="button" id="simpanalamat" class="btn warning m-t">Simpan Alamat</button>
<button hidden style="width: 12%" type="button" id="editalamat" class="btn warning m-t">Edit Alamat</button>
              </div>
              <br>
<div class="b-b b-b-dark b-b-1x"></div>
              <br>
      </div>
<div class="col-sm-12">
  <div id="status-hide" style="display: none;"></div>
  <table id="datatable" class="table table-striped v-middle p-0 m-0 box" data-plugin="dataTable">
    <thead>
      <tr class="deep-orange ">
        <th width="3%">NO</th>
        <th width="20%">ALAMAT</th>
        <th width="9%">PROVINSI</th>
        <th width="8%">KOTA</th>
        <th width="8%">KECAMATAN</th>
        <th width="8%">DESA</th>
        <th width="9%">KODE POS</th>
        <th width="9%">STATUS</th>
        <!-- <th width="6%">AKSI</th> -->
      </tr>
    </thead>
    <tbody id="alamatdetail">
      <?php
      $getdata=$this->u->showTBLalju()->result();
$no=1;
foreach ($getdata as $show) {
      ?>
      <tr data-id="<?=$show->nourut?>">
        <td><?=$no++?></td>
        <td><?=$show->alamat?></td>
        <td><?=getrow('id',$show->provinsi,'mprovinsi')->nama?></td>
        <td><?=getrow('id',$show->kota,'mkota')->nama?></td>
        <td><?=getrow('id',$show->kecamatan,'mkecamatan')->nama?></td>
        <td><?=getrow('id',$show->desa,'mdesa')->nama?></td>
        <td><?=$show->kodepos?></td>
        <td><a href="javascript:void(0)" data-id="<?=$show->nourut?>" class="statusAlamat"><?=statusAlamat($show->staktif)?></a></td> 
      </tr>
<?php }?>
    </tbody>
  </table>
</div>
</div><br>
                          </div>
                        </div>
                      </div>
                      <hr>
    <div class="text-right">
<button type="submit" class="btn deep-orange faa-parent animated-hover m-t">&nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-check faa-wrench"></i></button>
              </div>
              
  <?=form_close()?>
              </div>
    <div class="box-footer">
    </div>
    </div>
  </div>
        <!-- / .modal -->
<!-- </div> -->
<input type="hidden" id="idprovinsi-hidden" value="<?=$this->u->getDATA($_SESSION['user_id'],'manggota')->row()->provinsi;?>">
<input type="hidden" id="idkota-hidden" value="<?=$this->u->getDATA($_SESSION['user_id'],'manggota')->row()->kota;?>">
<input type="hidden" id="idkecamatan-hidden" value="<?=$this->u->getDATA($_SESSION['user_id'],'manggota')->row()->kecamatan;?>">
<input type="hidden" id="iddesa-hidden" value="<?=$this->u->getDATA($_SESSION['user_id'],'manggota')->row()->desa;?>">
<input type="hidden" id="idkopos-hidden" value="<?=$this->u->getDATA($_SESSION['user_id'],'manggota')->row()->kodepos;?>">
<input type="hidden" id="idjenisalamat-hidden" value="<?=$getAlTu->row()->nourut;?>">
<input type="hidden" id="alamatanggota-hidden" value="">
<input type="hidden" id="rowindex" value="">
<input type="hidden" id="colindex" value="">

<?php
        // $anggota = $this->u->getDATA($_SESSION['user_id'],'manggota')->row();
        // $bb = $this->u->getDesKecKot('id',$anggota->provinsi,'mprovinsi')->result();
?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5OAXrBY29rOI84KCADn_IAQXUgaqNo4Q&libraries=places"></script>

<script type="text/javascript" src="{custom_path}geomaps/jquery.geocomplete.min.js"></script>
<script type="text/javascript" src="{custom_path}user_setting.js"></script>
<!-- <script type="text/javascript" src="https://dl.dropbox.com/s/1cf4l0j8gridrvw/areamember.js?dl=0"></script> -->
<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
function imgError(image) {
        image.onerror = "";
        image.src = "https://member.kulcimart.com/assets/fotomember/<?=(!empty($fotomember))?'{fotomember}':'{defaultimg}'?>";
        // image.src = "http://cdn.member.kulcimart.com/assets/fotomember/<?=(!empty($fotomember))?'{fotomember}':'{defaultimg}'?>";
        return true;
      }
  $(function(){
   // $.when( $.ready ).then(function() {
  $('img').lazyload({
    effect:'fadeIn' 
  })
 $(document).ready(function() {
  $('.fotomem').change(function(){
    // alert($(this).val())
$('#upfotoga').val(1);
  })
  // $('#image_preview').click(function(){
  //       $("#filefoto").click()
  //     })
  $("#filefoto").hover(function(){
    $("#btnfe").css("background-color", "#eee");
    $(this).css("cursor", "pointer");
    }, function(){
    $("#btnfe").css("background-color", "");
});
               // getDataTable("#rekapbelanja","r/get-rekap-Belanja"); 

            });
            });
  </script> 
<script>
  $(function () {
    $("#geocomplete").geocomplete({
      map: ".map_canvas",
      details: "form ",
          geocodeAfterResult: true,
          blur: true,
      markerOptions: {
        draggable: true
      },
      mapOptions: {
        zoom: 9999,
      },
      maxZoom: 9999,
    });

    $("#geocomplete").bind("geocode:dragged", function (event, latLng) {
      $("input[name=lat]").val(latLng.lat());
      $("input[name=lng]").val(latLng.lng());
      $lalo=latLng.lat()+','+latLng.lng();
      // $("input[name=formatted_address]").val(latLng.formatted_address());
        var geocoder = new google.maps.Geocoder;
      $("#reset").show();
      // $tipe='';
geocodeLatLng(geocoder,$lalo)
    });


    $("#reset").click(function () {
      $("#geocomplete").geocomplete("resetMarker");
      $("#reset").hide();
      return false;
    });

    $("#find").click(function () {
      $("#geocomplete").trigger("geocode");
    }).click();


  });
// $('#geocomplete').change(function(){
//     $('#alamatanggota').empty()
//     $('#alamatanggota').val($(this).val())
// })
// $('#geocomplete2').change(function(){
//     $('#alamatanggota2').empty()
//     $('#alamatanggota2').val($(this).val())
// })
  function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat: '{lat}',
        lng: '{lng}'
      },
      zoom: 15
    });

    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    service.getDetails({
      placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
    }, function (place, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) {
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });
        google.maps.event.addListener(marker, 'click', function () {
          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
            'Place ID: ' + place.place_id + '<br>' +
            place.formatted_address + '</div>');
          infowindow.open(map, this);
        });
      }
    });
  }

  $(function () {
    $("#geocomplete2").geocomplete({
      map: ".map_canvass",
      details: "form ",
          geocodeAfterResult: true,
          // blur: true,
      markerOptions: {
        draggable: true
      },
      mapOptions: {
        zoom: 9999,
      },
      maxZoom: 9999,
    });

    $("#geocomplete2").bind("geocode:dragged", function (event, latLng) {
      $("input[name=lat2]").val(latLng.lat());
      $("input[name=lng2]").val(latLng.lng());
      $lalo=latLng.lat()+','+latLng.lng();
      // $("input[name=formatted_address]").val(latLng.formatted_address());
        var geocoder = new google.maps.Geocoder;
      $("#reset").show();
      $tipe=2;
geocodeLatLng(geocoder,$lalo,$tipe)
    });


    $("#reset").click(function () {
      $("#geocomplete2").geocomplete("resetMarker");
      $("#reset").hide();
      return false;
    });

    $("#finds").click(function () {
      $("#geocomplete2").trigger("geocode");
    }).click();


  });

  function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
      center: {
        lat2: '{lat}',
        lng2: '{lng}'
      },
      zoom: 15
    });

    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);
    service.getDetails({
      placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
    }, function (place, status) {
      if (status === google.maps.places.PlacesServiceStatus.OK) { 
        var marker = new google.maps.Marker({
          map: map,
          position: place.geometry.location
        });
        google.maps.event.addListener(marker, 'click', function () {
          infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
            'Place ID: ' + place.place_id + '<br>' +
            place.formatted_address + '</div>');
          infowindow.open(map, this);
        });
      }
    });
  } 
  function geocodeLatLng(geocoder,latlong,tipe='') {
        var input = latlong;
        var latlngStr = input.split(',', 2);
        var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
        geocoder.geocode({'location': latlng}, function(results, status) {
          if (status === 'OK') {
            if (results[0]) { 
              $('#geocomplete'+tipe).empty()
              $('#alamatanggota'+tipe).empty()
              $('#geocomplete'+tipe).val(results[0].formatted_address);
              $('#alamatanggota'+tipe).val(results[0].formatted_address);
            }
          }
        });
      }

</script>
