<div class="padding">
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<script type="text/javascript">
    $(document).ready(function(){
<?php
if(!empty($_SESSION['status'])){
    echo $_SESSION['status'];
$_SESSION['status']='';
}else{
$_SESSION['status']='';
    }
    ?>
})
</script>
<div class="row box">
  <div class="col-sm-12">
    <div class="box-header">
      <h5 class="mb-3">Pengaturan Kata Sandi</h5>
      <hr>
    </div>
    <div class="col-sm-6">
      <div class="clearfix">
              <form method="post" role="form" action="{site_url}u/users/changepwd">
                <div class="form-group">
                  <label>Kata Sandi Lama</label>
                  <input type="password" name="old_password" class="form-control">
                </div>
                <div class="form-group">
                  <label>Kata Sandi Baru</label>
                  <input type="password" name="newpassword" class="form-control">
                </div>
                <div class="form-group">
                  <label>Ulangi Kata Sandi Baru</label>
                  <input type="password" name="re_password" class="form-control">
                </div>
<button type="submit" class="btn deep-orange faa-parent animated-hover m-t">&nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-check faa-wrench"></i></button>
              </form>
            </div>
          </div>
    <div class="box-footer">
    </div>
        </div>
      </div>
    </div>
<script type="text/javascript" src="{custom_path}areamember.js"></script> 