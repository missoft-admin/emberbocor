<div class="padding">
<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?>
<div class="row box">
  <div class="col-sm-12">
    <div class="box-header">
      <h5 class="mb-3">Pengaturan Data Rekening</h5>
      <hr>
    </div>
    <div class="col-sm-6">
      <div class="clearfix">
	    	<form role="form" method="post" action="{site_url}s/users/upDATA">
	            <input type="hidden" name="datauptipe" value="1">
	             <div class="form-group">
                    <label>Nama Bank</label>
                    	<select name="namabank" id="namabank" class="form-control" data-plugin="select2" data-option="{}" data-placeholder="Pilih Bank..">
							<option></option>
							<option <?=($nbank=='BCA')?'selected':''?> value="BCA">BCA</option>
		                    <option <?=($nbank=='BRI')?'selected':''?> value="BRI">BRI</option>
		                    <option <?=($nbank=='MANDIRI')?'selected':''?> value="MANDIRI">MANDIRI</option>
		                    <option <?=($nbank=='BNI')?'selected':''?> value="BNI">BNI</option>
		                    <option <?=($nbank=='DANAMON')?'selected':''?> value="DANAMON">DANAMON</option>
                		</select>
                    </div>
                    <div class="form-group">
                    <label>Cabang Bank</label>
                    <label id="test1" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[A-Za-z ]+" id="cabangbank" hidden name="cabangbank" class="form-control" value="">
                    </div>
                    <div class="form-group">
                    <label>Nomor Rekening</label>
                    <label id="test2" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[0-9 ]+" id="norekening" hidden class="form-control" name="norekening" value="">
                    </div>
                    <div class="form-group">
                    <label>Atas Nama</label>
                    <label id="test3" class="form-control"><img src="{custom_path}gif/ajax-loader.gif"></label>
                    <input type="text" pattern="[A-Za-z ]+" id="atasnama" hidden class="form-control" name="atasnama" value="">
                    </div>
<button type="submit" class="btn deep-orange faa-parent animated-hover m-t">&nbsp;&nbsp;&nbsp;&nbsp;Simpan&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-check faa-wrench"></i></button>
	          </form>
	      </div>
	      </div>
    <div class="box-footer">
    </div>
	  </div>
	</div>
</div>
<script type="text/javascript" src="{custom_path}user_setting.js"></script>
<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
	
</script>