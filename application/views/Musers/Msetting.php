<div>
  <div class="d-sm-flex">
    <div class="w w-auto-xs light bg bg-auto-sm b-r">
      <div class="py-3">
        <div class="nav-active-border left b-primary">
          <ul class="nav flex-column nav-sm">
            <li class="nav-item">
              <a class="nav-link active" id="editprofil" href="#" data-toggle="tab" data-target="#tab-1">Profile</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " id="editrekening" href="#" data-toggle="tab" data-target="#tab-2">Edit Rekening</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " href="#" data-toggle="tab" data-target="#tab-3">Lihat Peta Jaringan</a>
            </li>
<!--             <li class="nav-item">
              <a class="nav-link " href="#" data-toggle="tab" data-target="#tab-4">Notifications</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link " href="#" data-toggle="tab" data-target="#tab-5">Security</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col p-0">
      <div class="tab-content pos-rlt">
        <?php
              if (isset($_SESSION['msg']) && $_SESSION['msg'] <> '') {
echo '<div class="psn"><strong>'.$_SESSION['hd'].'</strong>&nbsp;'
.$_SESSION['msg'].
'</div>';
}  
$_SESSION['hd']='';
$_SESSION['msg'] = '';
                ?>
        <div class="tab-pane active" id="tab-1">
          <div class="p-4 b-b _600">My Profile</div>
          <form role="form" action="{site_url}s/users/upDATA" class="p-4 col-md-6">
            <input type="hidden" name="datauptipe" value="0">
              <div id="dataprofil">

              </div>
            
          </form>
        </div>
        <div class="tab-pane" id="tab-2">
          <div class="p-4 b-b _600">Edit Rekening</div>
          <form role="form" action="{site_url}s/users/upDATA" class="p-4 col-md-6">
            <input type="hidden" name="datauptipe" value="1">
              <div id="datarekening">
                
              </div>
            
          </form>
        </div>
        <div class="tab-pane" id="tab-3">
          <div class="p-4 b-b _600">Peta Jaringan</div>
          <form role="form" class="p-4 col-md-6">
            <p>E-mail me whenever</p>
            <div class="checkbox">
              <label class="ui-check">
                <input type="checkbox"><i class="dark-white"></i> Anyone posts a comment
              </label>
            </div>
            <div class="checkbox">
              <label class="ui-check">
                <input type="checkbox"><i class="dark-white"></i> Anyone follow me
              </label>
            </div>
            <div class="checkbox">
              <label class="ui-check">
                <input type="checkbox"><i class="dark-white"></i> Anyone send me a message
              </label>
            </div>
            <div class="checkbox">
              <label class="ui-check">
                <input type="checkbox"><i class="dark-white"></i> Anyone invite me to group
              </label>
            </div>
            <button type="submit" class="btn primary mt-2">Update</button>
          </form>
        </div>
        <div class="tab-pane" id="tab-4">
          <div class="p-4 b-b _600">Notifications</div>
          <form role="form" class="p-4 col-md-6">
            <p>Notice me whenever</p>
            <div class="checkbox">
              <label class="ui-check">
                <input type="checkbox"><i class="dark-white"></i> Anyone seeing my profile page
              </label>
            </div>
            <div class="checkbox">
              <label class="ui-check">
                <input type="checkbox"><i class="dark-white"></i> Anyone follow me
              </label>
            </div>
            <div class="checkbox">
              <label class="ui-check">
                <input type="checkbox"><i class="dark-white"></i> Anyone send me a message
              </label>
            </div>
            <div class="checkbox">
              <label class="ui-check">
                <input type="checkbox"><i class="dark-white"></i> Anyone invite me to group
              </label>
            </div>
            <button type="submit" class="btn primary mt-2">Update</button>
          </form>
        </div>
        <div class="tab-pane" id="tab-5">
          <div class="p-4 b-b _600">Security</div>
          <div class="p-4">
            <div class="clearfix">
              <form method="post" role="form" action="{site_url}u/users/changepwd" class="col-md-6 p-0">
                <div class="form-group">
                  <label>Old Password</label>
                  <input type="password" name="old_password" class="form-control">
                </div>
                <div class="form-group">
                  <label>New Password</label>
                  <input type="password" name="newpassword" class="form-control">
                </div>
                <div class="form-group">
                  <label>New Password Again</label>
                  <input type="password" name="re_password" class="form-control">
                </div>
                <button type="submit" class="btn primary mt-2">Update</button>
              </form>
            </div>

            <p class="mt-4"><strong>Delete account?</strong></p>
            <button type="submit" class="btn danger m-t" data-toggle="modal" data-target="#modal">Delete Account</button>

          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- .modal -->
  <div id="modal" class="modal fade animate black-overlay" data-backdrop="false">
    <div class="modal-dialog modal-sm">
      <div class="modal-content flip-y">
        <div class="modal-body text-center">          
          <p class="py-3 mt-3"><i class="fa fa-remove text-warning fa-3x"></i></p>
          <p>Are you sure to delete your account?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn white" data-dismiss="modal">No</button>
          <button type="button" class="btn danger" data-dismiss="modal">Yes</button>
        </div>
      </div><!-- /.modal-content -->
    </div>
  </div>
  <!-- / .modal -->
</div>

<script type="text/javascript">

</script>