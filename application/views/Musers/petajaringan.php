<div class="padding">
<div class="row box">
  <div class="col-sm-12">
      <div class="box-header">
            <h5 class="mb-3">Data Peta Anggota Rekomendasi</h5>
<hr>
          </div>
        <div class="block block-bordered light">
          <div class="block-header">
              <h3 class="block-title">Filtering</h3>
          </div>
          <div class="block-content col-sm-12">
      <div class="box">
        <div class="box-body">
      <div class="clearfix">
        <form class="form-inline" action="{site_url}u/users/settings/anggotarekomendasi" method="post">
    <div class="col-sm-12">
        <div class="form-group">
                    <label><b><u>CARI BERDASARKAN :</u></b></label>
                      <select name="tipetampildata" id="tipetampildata" class="form-control col-sm-12" data-plugin="select2" data-option="{}" data-placeholder="Silahkan Pilih..">
              <option></option>
              <option <?=($tipetampildata==1)?'selected':''?> value="1">PENJUAL</option>
              <option <?=($tipetampildata==2)?'selected':''?> value="2">PEMBELI</option>
              <option <?=($tipetampildata==3)?'selected':''?> value="3">PENJUAL & PEMBELI</option>
                    </select>
                    </div>
          <div class="form-group col-sm-12">&nbsp;</div>
          <div class="form-group">
        <button type="submit" class="btn primary col-sm-12" id="filter">Filter</button>
          </div>
      </div>
        </form>
        <?=br(1)?>

        </div>
      </div>
      </div>
      </div>
    </div>
<div class=" b-t b-t-primary b-t-3x"></div>
        <?=br(1)?>
        <div class="row">
        <div class="col-sm-12 tabelshow1">
<div class="block block-bordered">
      <div class="block-header indigo">
          <h3 class="block-title">PENJUAL</h3>
      </div>
      <div class="block-content">

        <table id="table-penjual-kulcimart" class="table table-stripeds v-middle p-0 m-0 box"> 
          <thead style="text-transform: uppercase;"> 
            <tr class="indigo"> 
              <th width="3%">No</th> 
              <th width="15%">Tanggal daftar</th>
              <th width="11%">Id PENJUAL</th>
              <th width="15%">Nama PENJUAL</th>
              <th width="16%">Alamat</th>
              <th width="16%">TOTAL TRANSAKSI</th>
              <th width="16%">Nominal Transaksi</th>
            </tr> 
          </thead>
  <tfoot>
      <tr>
              <th colspan="6" style="text-align: right;">TOTAL</th>
              <th style="text-align: right;"></th>
      </tr>
  </tfoot>
        </table>
      </div>
    </div>
        </div>
        <div class="col-sm-12 tabelshow2">
<div class="block block-bordered">
      <div class="block-header deep-orange">
          <h3 class="block-title">PEMBELI</h3>
      </div>
      <div class="block-content ">

        <table id="table-pembeli-kulcimart" class="table table-striped v-middle p-0 m-0 box"> 
          <thead style="text-transform: uppercase;"> 
            <tr class="deep-orange"> 
              <th width="3%">No</th> 
              <th width="15%">Tanggal daftar</th>
              <th width="11%">Id PEMBELI</th>
              <th width="15%">Nama PEMBELI</th>
              <th width="16%">Alamat</th>
              <th width="16%">TOTAL TRANSAKSI</th>
              <th width="16%">Nominal Transaksi</th>
            </tr> 
          </thead>
  <tfoot>
      <tr>
              <th colspan="6" style="text-align: right;">TOTAL</th>
              <th style="text-align: right;"></th>
      </tr>
  </tfoot>
        </table>
      </div>
      </div>
    </div>

        </div>
      </div>
    </div>
</div>
<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
  $(function () {
   // $.when( $.ready ).then(function() {
$('.tabelshow1').slideUp('fast')
$('.tabelshow2').slideUp('fast')
    if($('#tipetampildata').val()==1){
$('.tabelshow1').slideDown(1000)
$('.tabelshow2').slideUp('fast');
    }else
    if($('#tipetampildata').val()==2){
$('.tabelshow1').slideUp("fast");
$('.tabelshow2').slideDown(1000);
    }else{
$('.tabelshow1').slideDown(1000)
$('.tabelshow2').slideDown(1000)      
    }
    // })
 $(document).ready(function() {
      getDataSSP('#table-penjual-kulcimart','../GETpenjual',6)
      getDataSSP('#table-pembeli-kulcimart','../GETpembeli',6)
      // getFilter('#bulan_fil','MM','months','Bulan')
      // getFilter('#tahun_fil','yyyy','years','Tahun')
    })
    })
  </script>