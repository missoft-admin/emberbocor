<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>Kulcimart | Page not found 404</title>
  <meta name="description" content="Responsive, Bootstrap, BS4" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- for ios 7 style, multi-resolution icon of 152x152 -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
  <link rel="apple-touch-icon" href="./../../../assets/images/logo.svg">
  <meta name="apple-mobile-web-app-title" content="Flatkit">
  <!-- for Chrome on Android, multi-resolution icon of 196x196 -->
  <meta name="mobile-web-app-capable" content="yes">
  <link rel="shortcut icon" sizes="196x196" href="./../../../assets/images/logo.svg">
  
  <!-- style -->

  <link rel="stylesheet" href="./../../../assets/libs/font-awesome/css/font-awesome.min.css" type="text/css" />

  <!-- build:css ../assets/css/app.min.css -->
  <link rel="stylesheet" href="./../../../assets/libs/bootstrap/dist/css/bootstrap.min.css" type="text/css" />
  <link rel="stylesheet" href="./../../../assets/css/app.css" type="text/css" />
  <link rel="stylesheet" href="./../../../assets/css/style.css" type="text/css" />
  <!-- endbuild -->

	<link href='https://fonts.googleapis.com/css?family=Anton|Passion+One|PT+Sans+Caption' rel='stylesheet' type='text/css'>
<link href='./../../../assets/custom/css/error404.css' rel='stylesheet' type='text/css'>
</head>
<body>

<body>
 <!-- Error Page -->
            <div class="error">
                <div class="container-floud">
                    <div class="shadowkiru col-xs-12 ground-color text-center">
                        <div class="container-error-404">
                            <div class="clip"><div class="shadow"><span class="digit thirdDigit"></span></div></div>
                            <div class="clip"><div class="shadow"><span class="digit secondDigit"></span></div></div>
                            <div class="clip"><div class="shadow"><span class="digit firstDigit"></span></div></div>
                            <div class="msg">OH!<span class="triangle"></span></div>
                        </div>
                        <h2 class="h1">Sorry! Page not found</h2>
                        <h6 class="h1"><a href="../../../">Go home!</a></h6>
                    </div>
                </div>
            </div>
        <!-- Error Page -->

<!-- ############ SWITHCHER END-->

<!-- build:js scripts/app.min.js -->
<!-- jQuery -->
  <script src="./../../../assets/libs/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap -->
  <script src="./../../../assets/libs/popper.js/dist/umd/popper.min.js"></script>
  <script src="./../../../assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- core -->
  <script src="./../../../assets/libs/pace-progress/pace.min.js"></script>
  <script src="./../../../assets/libs/pjax/pjax.js"></script>

  <script src="./../../../assets/scripts/lazyload.config.js"></script>
  <script src="./../../../assets/scripts/lazyload.js"></script>
  <script src="./../../../assets/scripts/plugin.js"></script>
  <script src="./../../../assets/scripts/nav.js"></script>
  <script src="./../../../assets/scripts/scrollto.js"></script>
  <script src="./../../../assets/scripts/toggleclass.js"></script>
  <script src="./../../../assets/scripts/theme.js"></script>
  <script src="./../../../assets/scripts/ajax.js"></script>
  <script src="./../../../assets/scripts/app.js"></script>
<!-- endbuild -->
</body>
</html>

<script type="text/javascript" src="./../../../assets/custom/js/error404.js"></script>