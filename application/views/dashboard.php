<?php echo ErrorSuccess($this->session)?>
<?php if($error != '') echo ErrorMessage($error)?><?php
$hari   = date('l');
    ?>
      <div class="clearfix">
<div class="p-3 light lt box-shadow-0 d-flex"> 
	<div class="flex">
		<h1 class="text-md mb-1 _400">Selamat datang kembali, <?=$_SESSION['namalengkap'];
			if($cekidanggota<>0){?>
			<span data-toggle="tooltip" data-placement="top" title="Terverifikasi"><i class="fa fa-check-circle"></i></span>
			<?php
		}?>
		</h1>
		<small class="text-muted">Terakhir Login Pada: <?=hari_indonesia($hari).', '.date('d-m-Y')?></small>
	</div>
</div>

<div class="row no-gutters box">
	<div class="col-sm-12">
			<div class="box-header">
      <h5 class="mb-3"><b>REKAPITULASI BELANJA</b></h5>
      <hr>
  </div>
  <div class="col-sm-12">
        <div class="block block-bordered light">
          <div class="block-header light">
              <h3 class="block-title">Filtering Periode</h3>
          </div>
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_fil">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan}" id="bulan_fil" name="bulan_fil">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_fil">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun}" id="tahun_fil" name="tahun_fil">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Filter</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
		</div>
		<div class="box-body">
      <h6 class="mb-3">TRANSAKSI YANG BELUM SELESAI DITERIMA</h6>
<table width="100%" id="example" class="table table-striped v-middle p-0 m-0 box">
	<thead class="deep-orange">
          <tr>
              <th>NO</th>
              <th>ID TRANSAKSI</th>
              <th>TANGGAL TRANSAKSI</th>
              <th>NAMA PENJUAL</th>
              <th>TOTAL BAYAR</th>
              <th>STATUS</th>
          </tr>
      </thead>
  </table>
	</div>
</div>
</div>

<div class="white bg">
	<div class="row no-gutters">
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Produk Yang Sering dibeli<hr></h6>
		</div>
			<div class="list inset">
				<?php foreach ($produkfav1 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->foto!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_fotoproduk}kecil/<?=$row->foto?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaproduk);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaproduk;echo ($index==0)?' &#10084;':''?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalbeli);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<? } ?>
				</div>
			</div>
		</div> 

	<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Produk Yang Jarang dibeli<hr></h6>
		</div>
			<div class="list inset">
				<?php foreach ($produkfav2 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->foto!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_fotoproduk}kecil/<?=$row->foto?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaproduk);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaproduk;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalbeli);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<? } ?>
				</div>
			</div>
		</div> 
</div>
</div>
<div class=" b-t b-t-warning b-t-3x"></div>

<div class="light">
	<div class="row no-gutters">
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
					<h6 class="mb-3">10 Penjual Paling Sering Ditransaksi <hr></h6>
				</div>
				<div class="list inset">
					<?php foreach ($partnerfav1 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_foto}kecil/<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;echo ($index==0)?' &#10084;':''?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalditransaksi);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
					</div>
					<? } ?>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
					<h3>&nbsp;</h3>
				</div>
				<div class="list inset">
					<?php foreach ($partnerfav2 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_foto}kecil/<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalditransaksi);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class=" b-t b-t-warning b-t-3x"></div>


<div class="white bg">
	<div class="row no-gutters">
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Pembeli Yang Disponsori Pembelian Terbanyak<hr></h6>
		</div>
			<div class="list inset">
				<?php if($memspon1){
				 foreach ($memspon1 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil==0){ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{fpmember_path}<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalmemspon);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<?php }
				}else{ ?><center class="text-mute">
					<div class="col-sm-12 form-group">
						<label><i>Tidak ada data</i></label>
					</div></center>
				<? }?>
				</div>
			</div>
		</div> 

	<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Pembeli Yang Disponsori Pembelian Paling Sedikit<hr></h6>
		</div>
			<div class="list inset">
				<?php if($memspon2){
				 foreach ($memspon2 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil==0){ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{fpmember_path}<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalmemspon);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<?php }
				}else{ ?><center class="text-mute">
					<div class="col-sm-12 form-group">
						<label><i>Tidak ada data</i></label>
					</div></center>
				<? }?>
				</div>
			</div>
		</div> 
</div>
</div>
<div class=" b-t b-t-warning b-t-3x"></div>

<div class="light bg">
	<div class="row no-gutters">
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Penjual Yang Disponsori Penjualan Terbanyak<hr></h6>
		</div>
			<div class="list inset">
				<?php if($partspon1){
				 foreach ($partspon1 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil==0){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_foto}kecil/<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalpartner1);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div> 
					<?php }
				}else{ ?><center class="text-mute">
					<div class="col-sm-12 form-group">
						<label><i>Tidak ada data</i></label>
					</div></center>
				<? }?>
				</div>
			</div>
		</div> 

	<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Penjual Yang Disponsori Penjualan Paling Sedikit<hr></h6>
		</div>
			<div class="list inset">
				<?php if($partspon2){
					foreach ($partspon2 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil==0){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_foto}kecil/<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalpartner1);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<?php }
				}else{ ?><center class="text-mute">
					<div class="col-sm-12 form-group">
						<label><i>Tidak ada data</i></label>
					</div></center>
				<? }?>
				</div>
			</div>
		</div> 
</div>
</div>
</div>


<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
  $(function () {
      getFilter('#bulan_fil','MM','months','Bulan')
      getFilter('#tahun_fil','yyyy','years','Tahun')
 $(document).ready(function() {
     getDataSSP("#example","riwayat-Order?tahun_fil="+$('#tahun_fil').val()+"&&bulan_fil="+$('#bulan_fil').val());
            });
})
  </script>  