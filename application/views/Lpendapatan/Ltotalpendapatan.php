<?php
$gtotal='';
?>
<div class="padding">
<div class="row box">
  <div class="col-sm-12">
		<div class="box-header">
			<h5 class="mb-3">Data Total Pendapatan</h5>
      <hr>
    </div>
        <div class="block block-bordered light">
          <div class="block-header light">
              <h3 class="block-title">Filtering</h3>
          </div>
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}p/totalcashback" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_fil">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan}" id="bulan_fil" name="bulan_fil">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_fil">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun}" id="tahun_fil" name="tahun_fil">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Filter</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
<div class=" b-t b-t-warning b-t-3x"></div>
    <!-- <div class="row"> -->
          <div class="box-body col-sm-12">
<table width="100%" id="totalpesndapatan" class="table table-striped v-middle p-0 m-0 box">
  <thead class="deep-orange ">
          <tr>
              <th width="3%">#</th>
              <th colspan="2">Total Pendapatan</th>
              <th width="15%">Jumlah</th>
          </tr>
      </thead>
        <?php
if($status==1){
        ?>
      <tbody>
        <tr>
          <th>1</th>
          <th width="5%">&nbsp;</th>
          <th>Bonus Sponsor</th>
          <td><?php
          foreach ($totalsponsor as $row) {
          echo number_format($row->Bonus);
          $gtotal+=$row->Bonus;
          }
          ?></td>
        </tr>
        <tr>
          <th>2</th>
          <th width="5%">&nbsp;</th>
          <th>Bonus Partnership1</th>
          <td><?php
          foreach ($totalpartnership1 as $row) {
          echo number_format($row->Bonus);
          $gtotal+=$row->Bonus;
          }
          ?></td>
        </tr>
        <tr>
          <th>3</th>
          <th width="5%">&nbsp;</th>
          <th>Bonus Partnership2</th>
          <td><?php
          foreach ($totalpartnership2 as $row) {
          echo number_format($row->Bonus);
          $gtotal+=$row->Bonus;
          }
          ?></td>
        </tr>
        <tr>
          <th>4</th>
          <th width="5%">&nbsp;</th>
          <th>Bonus Loyalti</th>
          <td><?php
          // foreach ($totalloyalti as $row) {
          echo number_format($totalloyalti);
          $gtotal+=$totalloyalti;
          // }
          ?></td>
        </tr>
      </tbody>
      <tfoot>
          <tr>
              <th colspan="3">TOTAL</th>
              <th id="gtotal">Rp.<?=number_format($gtotal)?></th>
          </tr>
      </tfoot>
        <?php
}else{
        ?>
      <tbody>
        <tr>
          <td colspan="4" class="text-center">Tidak ada pendapatan pada periode ini.</td>
        </tr>
      </tbody>
<?php }?>
  </table>
    </div>
    <div class="box-footer">
    </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
  $(function () {
      getFilter('#bulan_fil','MM','months','Bulan')
      getFilter('#tahun_fil','yyyy','years','Tahun')
    })
  </script>