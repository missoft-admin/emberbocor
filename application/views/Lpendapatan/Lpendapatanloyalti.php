<?php
// $bulan   = date('l');
?>
<div class="padding">
<div class="row box">
	<div class="col-sm-12">
			<div class="box-header">
            <h5 class="mb-3">Pendapatan Loyalti</h5>
<hr>
          </div>
        <div class="block block-bordered light">
          <div class="block-header light">
              <h3 class="block-title">Filtering</h3>
          </div>
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}p/loyalti" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_fil">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan}" id="bulan_fil" name="bulan_fil">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_fil">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun}" id="tahun_fil" name="tahun_fil">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Filter</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
<div class=" b-t b-t-warning b-t-3x"></div>
            <div class="row">
          <div class="box-body col-sm-12">
  <div class="block block-bordered">
      <div class="block-header light b-t b-t-warning b-l b-l-warning b-b b-b-warning b-r b-r-warning">
          <h3 class="block-title">Rumus pendapatan loyalti</h3>
      </div>
      <div class="block-content b-t b-t-warning b-l b-l-warning b-b b-b-warning b-r b-r-warning ">
<b>
  <div class="form-group">
  <label class="col-sm-2">PENDAPATAN LOYALTI</label><label class="col-sm-8">= (KP/&Sigma;KPL)*NLN</label>
<br>
  <label class="col-sm-2">&Sigma;KPL</label><label class="col-sm-8">= TOTAL KOEFISIEN PERAIH LOYALTI</label>
<br>
  <label class="col-sm-2">KP</label><label class="col-sm-8">= KOEFISIEN PRIBADI
    <sub class="text-info"><small><b> (PENJUMLAHAN KOEFISIEN NOMINAL DENGAN KOEFISIEN JUMLAH TRANSAKSI)</b></small></sub></label>
<br>
  <label class="col-sm-2">NLN</label><label class="col-sm-8">= NOMINAL LOYALTI NASIONAL
   <sub class="text-info"><small><b> (TOTAL SELURUH BONUS LOYALTI DARI TRANSAKSI YANG TERKUMPUL DALAM 1 BULAN)</b></small></sub></label>
 </div>
</b>
<hr>
<?php
if($status==0){
?>

      <div class="block block-bordered col-sm-12">
          <div class="block-content">
            <center>
            <h3>Maaf!, Anda Tidak Mendapatkan Bonus Loyalti</h3>
            <img src="{img_path}nodata.png" width="360px">
            <h4>Untuk Pendapatan Loyalti</h4>
            <h6>Pada periode: {bulan} {tahun}</h6>
          </center>
</div>
</div>
<?php
}else{
$hasil=($KP/$tKPL)*$Bloyalti;
?>
<!-- ############################################################################################ -->
<div class="form-group">
  <label class="col-sm-2">&Sigma;KPL</label><label class="col-sm-8">= {tKPL}</label>
<br>
  <label class="col-sm-2">KP</label><label class="col-sm-8">= {KP}</label>
<br>
  <label class="col-sm-2">NLN</label><label class="col-sm-8">= Rp <?=number_format($Bloyalti)?></label>
 </div>
  <hr>
<b>
<div class="form-group">
  <label class="col-sm-12 text-md">PENDAPATAN LOYALTI = Rp <?=number_format($hasil)?></label>
</div>
</b>
<!-- <table id="tabel__xy-pL" class="table v-middle p-0 m-0 box">
  <thead>
    <tr>
      <th>#</th>
      <th>ID ANGGOTA</th>
      <th>NAMA ANGGOTA</th>
      <th>JUMLAH TRANSAKSI</th>
      <th>KOEFISIEN JUMLAH TRANSAKSI</th>
      <th>TOTAL HARGA</th>
      <th>KOEFISIEN TOTAL HARGA</th>
      <th>TOTAL KOEFISIEN</th>
    </tr>
  </thead>
</table> -->
<!-- ############################################################################################ -->
<?php }?>
      </div>
    </div>

</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
  $(function () {
      getFilter('#bulan_fil','MM','months','Bulan')
      getFilter('#tahun_fil','yyyy','years','Tahun')
// getDataSSP("#tabel__xy-pL","pendapatan-Loyalti?tahun_fil="+$('#tahun_fil').val()+"&&bulan_fil="+$('#bulan_fil').val())
    })
  </script>