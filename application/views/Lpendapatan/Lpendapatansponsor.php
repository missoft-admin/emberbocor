<div class="padding">
<div class="row box">
  <div class="col-sm-12">
    <div class="box-header">
      <h5 class="mb-3">Pendapatan Sponsor</h5>
      <hr>
    </div>
        <div class="block block-bordered light">
          <div class="block-header light">
              <h3 class="block-title">Filtering</h3>
          </div>
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}p/sponsor" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_fil">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan}" id="bulan_fil" name="bulan_fil">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_fil">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun}" id="tahun_fil" name="tahun_fil">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Filter</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
<div class=" b-t b-t-warning b-t-3x"></div>
    <div class="box-body">
<table width="100%" id="pendapatansponsor" class="table table-striped v-middle p-0 m-0 box">
  <thead class="deep-orange ">
      <tr>
          <th width="2%">#</th>
          <th>ID Anggota</th>
          <th>ID Transaksi</th>
          <th>Tipe Bonus</th>
          <th>Periode Transaksi</th>
          <th>Tanggal Transaksi</th>
          <th width="18%">Nominal Bonus</th>
          <th width="1px" style="display: none">Nominal Bonus</th>
      </tr>
  </thead>
  <tfoot>
      <tr>
          <th colspan="5">TOTAL</th>
              <th colspan="2" style="text-align: right;"></th>
              <!-- <th colspan=""></th> -->
      </tr>
  </tfoot>
  </table>
    </div>
    <div class="box-footer">
    </div>
		</div>
	</div>
</div>
<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
  $(function () {
      getFilter('#bulan_fil','MM','months','Bulan')
      getFilter('#tahun_fil','yyyy','years','Tahun')
 $(document).ready(function() {
     getDataSSP("#pendapatansponsor","../p/pendapatan-Sponsor?tahun_fil="+$('#tahun_fil').val()+"&&bulan_fil="+$('#bulan_fil').val(),6);
            });
})
  </script> 