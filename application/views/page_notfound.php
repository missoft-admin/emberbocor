<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title>Kulcimart | 404</title>
	<meta name="description" content="Responsive, Bootstrap, BS4" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- for ios 7 style, multi-resolution icon of 152x152 -->
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-barstyle" content="black-translucent">
	<link rel="apple-touch-icon" href="<?=$img_path?>logo.svg">
	<meta name="apple-mobile-web-app-title" content="Flatkit">
	<!-- for Chrome on Android, multi-resolution icon of 196x196 -->
	<meta name="mobile-web-app-capable" content="yes">
	<link rel="shortcut icon" sizes="196x196" href="<?=$img_path?>logo.svg">

	<!-- style -->

	<link rel="stylesheet" href="<?=$libs_path?>font-awesome/css/font-awesome.min.css" type="text/css" />

	<link rel="stylesheet" href="<?=$libs_path?>bootstrap/dist/css/bootstrap.min.css" type="text/css" />
	<link rel="stylesheet" href="<?=$css_path?>app.css" type="text/css" />
	<link rel="stylesheet" href="<?=$css_path?>style.css" type="text/css" />
	<link rel="stylesheet" href="<?=$css_path?>theme/warning.css" type="text/css" />
	<!-- endbuild -->
</head>

<body>
	<div class="d-flex flex align-items-center h-v info theme">
		<div class="text-center p-5 w-100">
			<h1 class="display-5 my-5">Sorry! the page you are looking for doesn't exist.</h1>
			<p>Go back to <a href="<?=$base_url?>dashboard" class="b-b b-white">dashboard</a></p>
			<p class="my-5 text-muted h4">
				-- 404 --
			</p>
		</div>
	</div>

	<!-- jQuery -->
	<script src="<?=$libs_path?>jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="<?=$libs_path?>popper.js/dist/umd/popper.min.js"></script>
	<script src="<?=$libs_path?>bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- core -->
	<script src="<?=$libs_path?>pace-progress/pace.min.js"></script>
	<script src="<?=$libs_path?>pjax/pjax.js"></script>

	<script src="<?=$scripts_path?>lazyload.config.js"></script>
	<script src="<?=$scripts_path?>lazyload.js"></script>
	<script src="<?=$scripts_path?>plugin.js"></script>
	<script src="<?=$scripts_path?>nav.js"></script>
	<script src="<?=$scripts_path?>scrollto.js"></script>
	<script src="<?=$scripts_path?>toggleclass.js"></script>
	<script src="<?=$scripts_path?>theme.js"></script>
	<script src="<?=$scripts_path?>ajax.js"></script>
	<script src="<?=$scripts_path?>app.js"></script>
	<!-- endbuild -->
</body>

</html>
