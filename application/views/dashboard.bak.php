<div class="p-3 light lt box-shadow-0 d-flex">
	<div class="flex">
		<h1 class="text-md mb-1 _400">Welcome back, <?=$_SESSION['namalengkap'];
			if($cekidanggota<>0){?>
			<span data-toggle="tooltip" data-placement="top" title="Terverifikasi"><i class="fa fa-check-circle"></i></span>
			<?php
		}?>
		</h1>
		<small class="text-muted">Last logged in: <?=date('l, d-m-Y');?></small>
	</div>
</div>

<div class="row no-gutters box">
	<div class="col-sm-12">
		<div class="padding">
      <h5 class="mb-3">Rekapitulasi Belanja</h5>
      <hr>
        <div class="block block-bordered light">
          <div class="block-header light">
              <h3 class="block-title">Periode</h3>
          </div>
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_fil">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan}" id="bulan_fil" name="bulan_fil">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_fil">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun}" id="tahun_fil" name="tahun_fil">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Filter</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
<table width="100%" id="example" class="table v-middle p-0 m-0 box">
	<thead>
          <tr>
              <th>#</th>
              <th>ID Transaksi</th>
              <th>Tanggal Transaksi</th>
              <th>Nama Partner</th>
              <th>Total Harga</th>
              <th>Status</th>
          </tr>
      </thead>
  </table>
		</div>
	</div>
</div>

<div class="light bg">
	<div class="row no-gutters">
        <div class="block block-bordered light col-sm-12">
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_prodfav">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan_prodfav}" id="Bfil_prodfav" name="bulan_prodfav">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_prodfav">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun_prodfav}" id="Tfil_prodfav" name="tahun_prodfav">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Cari Periode</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Produk Yang Sering dibeli</h6>
		</div> 
			<div class="list inset">
				<?php foreach ($produkfav1 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->foto!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_fotoproduk}<?=$row->foto?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaproduk);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaproduk;echo ($index==0)?' &#10084;':''?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalbeli);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<? } ?>
				</div>
			</div>
		</div> 

	<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Produk Yang Jarang dibeli</h6>
		</div> 
			<div class="list inset">
				<?php foreach ($produkfav2 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->foto!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_fotoproduk}<?=$row->foto?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaproduk);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaproduk;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalbeli);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<? } ?>
				</div>
			</div>
		</div> 
</div>
</div>

<div class="white">
	<div class="row no-gutters">
        <div class="block block-bordered light col-sm-12">
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_partfav">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan_partfav}" id="Bfil_partfav" name="bulan_partfav">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_partfav">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun_partfav}" id="Tfil_partfav" name="tahun_partfav">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Cari Periode</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
					<h3>10 Partner Paling Sering Ditransaksi</h3>
				</div>
				<div class="list inset">
					<?php foreach ($partnerfav1 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_foto}<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;echo ($index==0)?' &#10084;':''?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalditransaksi);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
					</div>
					<? } ?>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
					<h3>&nbsp;</h3>
				</div>
				<div class="list inset">
					<?php foreach ($partnerfav2 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_foto}<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalditransaksi);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="light bg">
	<div class="row no-gutters">
        <div class="block block-bordered light col-sm-12">
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_memfav">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan_memfav}" id="Bfil_memfav" name="bulan_memfav">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_memfav">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun_memfav}" id="Tfil_memfav" name="tahun_memfav">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Cari Periode</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Member Yang Disponsori Pembelian Terbanyak</h6>
		</div>
			<div class="list inset">
				<?php foreach ($memspon1 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{fpmember_path}<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalmemspon);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<? } ?>
				</div>
			</div>
		</div> 

	<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Member Yang Disponsori Pembelian Paling Sedikit</h6>
		</div>
			<div class="list inset">
				<?php foreach ($memspon2 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{fpmember_path}<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalmemspon);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<? } ?>
				</div>
			</div>
		</div> 
</div>
</div>

<div class="white bg">
	<div class="row no-gutters">
        <div class="block block-bordered light col-sm-12">
          <div class="block-content b-t b-t-light b-l b-l-light b-b b-b-light b-r b-r-light ">
        <form class="form-inline" action="{site_url}" method="post">
          <div class="form-group col-sm-6">
            <label for="bulan_partspon">Bulan</label>
        <input type="text" class="form-control col-sm-12" value="{bulan_partspon}" id="Bfil_partspon" name="bulan_partspon">
          </div>
          <div class="form-group col-sm-6">
            <label for="tahun_partspon">Tahun</label>
        <input type="text" class="form-control col-sm-12" value="{tahun_partspon}" id="Tfil_partspon" name="tahun_partspon">
          </div>
          <div class="form-group col-sm-12"><br>
          </div>
          <div class="form-group col-sm-12">
        <button type="submit" class="btn primary col-sm-12" id="filter">Cari Periode</button>
          </div>
        </form>
        <?=br(1)?>
      </div>
    </div>
		<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Partner Yang Disponsori Penjualan Terbanyak</h6>
		</div>
			<div class="list inset">
				<?php foreach ($partspon1 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_foto}<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalpartner1);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<? } ?>
				</div>
			</div>
		</div> 

	<div class="col-sm-6">
			<div class="p-lg-3">
				<div class="box-header">
			<h6 class="mb-3">Partner Yang Disponsori Penjualan Paling Sedikit</h6>
		</div>
			<div class="list inset">
				<?php foreach ($partspon2 as $index => $row) { ?>
					<div class="list-item " data-id="item-<?=$index;?>">
						<?php if($row->fotoprofil!==''){ ?>
						<span class="w-40 h-40 avatar circle green">
			          <img src="{partner_foto}<?=$row->fotoprofil?>" alt=".">
			      </span>
						<?php }else{ ?>
						<span class="w-40 h-40 avatar circle <?=GetColorRandom();?>">
							<?=GetAvatarName($row->namaanggota);?>
						</span>
						<?php } ?>
						<div class="list-body">
							<a href="" class="item-title _500"><?=$row->namaanggota;?></a>

							<div class="item-except text-sm text-muted h-1x">
								<b><?=number_format($row->totalpartner1);?></b> Transaksi
							</div>

							<div class="item-tag tag hide">
							</div>
						</div>
						</div>
					<? } ?>
				</div>
			</div>
		</div> 
</div>
</div>


<script type="text/javascript" src="{custom_path}areamember.js"></script>
<script type="text/javascript">
  $(function () {
      getFilter('#bulan_fil','mm','months','Bulan')
      getFilter('#tahun_fil','yyyy','years','Tahun')
      getFilter('#Bfil_prodfav','mm','months','Bulan')
      getFilter('#Tfil_prodfav','yyyy','years','Tahun')
      getFilter('#Bfil_partfav','mm','months','Bulan')
      getFilter('#Tfil_partfav','yyyy','years','Tahun')
      getFilter('#Bfil_memfav','mm','months','Bulan')
      getFilter('#Tfil_memfav','yyyy','years','Tahun')
      getFilter('#Bfil_partspon','mm','months','Bulan')
      getFilter('#Tfil_partspon','yyyy','years','Tahun')
 $(document).ready(function() {
     getDataSSP("#example","riwayat-Order?tahun_fil="+$('#tahun_fil').val()+"&&bulan_fil="+$('#bulan_fil').val());
            });
})
  </script>  