<?php
defined('BASEPATH') OR exit('Hacking Attempt : Keluar dari sistem..!!');

class Auth extends CI_Controller {

	public function __construct() {
            parent::__construct();
            $this->load->model('Auth_model','auth');
         
         }
	
  public function index()
  {
exit('Hacking Attempt : Keluar dari sistem..!!');
  }

  public function signup()
  {
        $data = array();
        $data['toptitle']   = 'Sign Up';
        $data['title']      = 'Sign Up';
        $data['content']    = 'auth/signup';

        $data = array_merge($data, path_variable());
        $this->parser->parse('auth/auth_template', $data);
  }

  public function signin()
  {
if(isset($_SESSION['logged'])==false){
  // redirect('SignIn','refresh');

        $data = array();
        $data['toptitle']   = 'Sign In';
        $data['title']      = 'Sign In';
        $data['content']    = 'auth/signin';

        $data = array_merge($data, path_variable());
        $this->parser->parse('auth/auth_template', $data);
				// $this->do_login();
      }else{
  redirect('dashboard','refresh');
}
  }

  public function forgot_password()
  {
        $data = array();
        $data['toptitle']   = 'Forgot Password';
        $data['title']      = 'Forgot Password';
        $data['content']    = 'auth/forgot_password';

        $data = array_merge($data, path_variable());
        $this->parser->parse('auth/auth_template', $data);
  }

	public function proses_signup()
	{
  // create the data object
    $data = new stdClass();
    
    // set validation rules
    $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|is_unique[manggota.username]', array('is_unique' => 'This username already exists. Please choose another one.'));
    $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[manggota.email]');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]');
    $this->form_validation->set_rules('password_confirm', 'Confirm Password', 'trim|required|min_length[6]|matches[password]');
    
    if ($this->form_validation->run() === false) {
      redirect('SignUp','refresh');
    } else {
      
      // set variables from the form
      $username = $this->input->post('username');
      $email    = $this->input->post('email');
      $password = $this->input->post('password');
      
      if ($this->auth->create_user($username, $email, $password)) {
        redirect('SignIn','refresh');
        
      } else {
        
        // user creation failed, this should never happen
        $data->error = 'There was a problem creating your new account. Please try again.';
        
        // send error to the view
        $this->load->view('header');
        $this->load->view('user/register/register', $data);
        $this->load->view('footer');
        
      }
      
    }
  }

public function do_login(){
  // create the data object
    $data = new stdClass();
    
//     // set validation rules
//     $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
//     $this->form_validation->set_rules('password', 'Password', 'required');
    
//     if ($this->form_validation->run() == false) {
      
//       // validation not ok, send validation errors to the view
//         $_SESSION['status']='Toastr("Username atau kata sandi anda salah","Info")';
// redirect('SignIn','refresh');
//     } else {
      
      // set variables from the form
      $username = $this->input->post('username');
      $password = $this->input->post('password');
      $where = array(
                    'username' => $username,
                    'tipeanggota' => 1,
                    'passkey' => hash('sha256',md5($password))
                );
      $cek=$this->auth->resolve_user_login($where);
      if ($cek->num_rows()>0) {
        $user_id = $this->auth->get_user_id_from_username($username);
        $user    = $this->auth->get_user($user_id);
        
        // set session user datas
        $_SESSION['user_id']      = (string)$user->idanggota;
        $_SESSION['username']     = (string)$user->username;
        $_SESSION['namalengkap']     = (string)$user->namaanggota;
        $_SESSION['logged']    = (bool)true;

    redirect('dashboard','refresh');
      } else {
        // login failed
        $_SESSION['status']='Toastr("Username atau kata sandi anda salah","Info")';
        redirect('SignIn','refresh');
        
      }
      
    // }
}

  /**
   * logout function.
   * 
   * @access public
   * @return void
   */
public function do_logout(){
    
    // create the data object
    $data = new stdClass();
    
    if (isset($_SESSION['logged']) && $_SESSION['logged'] === true) {
       $this->session->sess_destroy();
      // remove session datas
      foreach ($_SESSION as $key => $value) {
        unset($_SESSION[$key]);
      }

      // user logout ok
redirect('SignIn','refresh');
    } else {
      
      // there user was not logged in, we cannot logged him out,
      // redirect him to site root
      redirect('/');
      
    }
    
  }


  }