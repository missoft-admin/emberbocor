<?php
defined('BASEPATH') OR exit('Hacking Attempt : Keluar dari sistem..!!');

class Tverifikasi extends CI_Controller {

  public function __construct() {
            parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model('Tverifikasi_model','tv');
            $this->form_validation->set_error_delimiters('<label>', '</label>');
         }
  
  public function index()
  {
// if($_SESSION['logged_in']==false){
//   redirect('SignUp','refresh');
// }else{
//   redirect('dashboard','refresh');
// }   
  
  }
  
  public function verifikasiBayarPendaftaran(){
    $data = array();
    $data['error']        ='';
    $data['toptitle']     ='Verifikasi Bayar Pendaftaran';
    $data['title']        ='Verifikasi Bayar Pendaftaran';
    $data['content']      ='Tverifikasi/bayarpendaftaran';
    $data['cekdata'] =getwhere('idanggotamember',$_SESSION['user_id'],'tbayardaftarmember')->num_rows();
            $data['nbank']      =getwhere('idanggota',$_SESSION['user_id'],'manggota')->row()->namabank;
    $data = array_merge($data, path_variable());
    $this->parser->parse('page_template', $data);  
  }

public function prosesInputBayarPendaftaran()
  {
    $form=$this->form_validation;
    $data = new stdClass();
    // $form->set_rules('nominaltransaksi', 'Nominal Transaksi', 'trim|required|');
    // $form->set_rules('fotobuktitransfer', 'Foto Bukti Transfer', 'trim|required');
    $form->set_rules('namabank', 'Foto Bukti Transfer', 'trim|required');
    $form->set_rules('norekening', 'Nomor Rekening', 'trim|required|min_length[4]');
    $form->set_rules('atasnama', 'Atas Nama', 'trim|required|min_length[4]');
    
    if ($form->run() == true) {
      $data=array();
      $data['idbayardaftarmember']  =getkode('tbayardaftarmember','idbayardaftarmember','BDM');
      $data['tanggalbayar']         =date('Y-m-d', strtotime($this->input->post('tanggalbayar')));
      $data['idanggotamember']      =$_SESSION['user_id'];
      $data['nominaltransaksi']     =RemoveComma($this->input->post('nominaltransaksi'));
      // $data['fotobuktitransfer']=$this->input->post('fotobuktitransfer');
      $data['namabank']             =$this->input->post('namabank');
      $data['norekening']           =$this->input->post('norekening');
      $data['atasnama']             =$this->input->post('atasnama');

            $config['upload_path']          = './assets/buktiverifikasi/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png|*';
            $config['max_size']             = 5000;
            $this->load->library('upload',$config);
            $this->upload->initialize($config);

        if (!is_dir('assets/buktiverifikasi')) {
            mkdir('./assets/buktiverifikasi', 0777, true);
        }
        $dir_exist = true; // mengecek direktori jika sudah ada atau tidak ada
        if (!is_dir('assets/buktiverifikasi/')) {
            mkdir('./assets/buktiverifikasi/', 0777, true);
            $dir_exist = false; // direktori sudah ada
        }

            if ( ! $this->upload->do_upload('filefotonya')){
                $error = array('error' => $this->upload->display_errors());
        $_SESSION['status']='Toastr("Format foto bukti transfer tidak sesuai","Info")';
        redirect('v/verifikasi-Pendaftaran','refresh');
            }else{ 
                $upload_data = $this->upload->data(); 
                $data['fotobuktitransfer'] = $upload_data['file_name'];
            }


      if(saveData('tbayardaftarmember',$data)){
        $_SESSION['status']='ToastrSukses("Selamat, anda sudah membayar pendaftaran","Info")';
        redirect('v/verifikasi-Pendaftaran','refresh');
        }

     }else{
      $this->failed_save();
    }
  }

    function failed_save(){
    $data = $this->input->post();
    $data['error']      = validation_errors();
    $data['toptitle']   = 'Verifikasi Bayar Pendaftaran';
    $data['title']      = 'Verifikasi Bayar Pendaftaran';
    $data['content']    = 'Tverifikasi/bayarpendaftaran';
    $data['cekdata']    =getwhere('idanggotamember',$_SESSION['user_id'],'tbayardaftarmember')->num_rows();
    // $data['idpembayaran'] =$this->tv->getidbayardaftarmember();

    $data = array_merge($data, path_variable());
    $this->parser->parse('page_template', $data);
  }

  public function prosesInputBayarProduk(){
        $form=$this->form_validation;
        $data = new stdClass();
        // $form->set_rules('nominaltransaksi', 'Nominal Transaksi', 'trim|required|numeric|min_length[4]');
        // $form->set_rules('fotobuktitransfer', 'Foto Bukti Transfer', 'trim|required');
        $form->set_rules('namabank', 'Nama Bank', 'trim|required');
        $form->set_rules('norekening', 'Nomor Rekening', 'trim|required|min_length[4]');
        $form->set_rules('atasnama', 'Atas Nama', 'trim|required|min_length[4]');
        
        if ($form->run() == true) {
          $data=array();
          $data['idtransaksibayar']     =getkode('tjualbayar','idtransaksibayar','BT');
          $data['tanggalbayar']         =date('Y-m-d', strtotime($this->input->post('tanggalbayar')));
          // $data2['catatanpermintaan']   =$_POST['catatanpermintaan'];
          $data['idtransaksi']          =$this->input->post('idtransaksi');
          // $data['idanggotamember']      =$_SESSION['user_id'];
          $data['nominaltransaksi']     =RemoveComma($this->input->post('nominaltransaksi'));
          // $data['fotobuktitransfer']=$this->input->post('fotobuktitransfer');
          $data['namabank']             =$this->input->post('namabank');
          $data['norekening']           =$this->input->post('norekening');
          $data['atasnama']             =$this->input->post('atasnama');
                $config['upload_path']          = './assets/buktiverifbayar/';
                $config['allowed_types']        = 'gif|jpg|png|jpeg';
                $config['max_size']             = 5000;
                $this->load->library('upload',$config);
                $this->upload->initialize($config);

            if (!is_dir('assets/buktiverifbayar')) {
                mkdir('./assets/buktiverifbayar', 0777, true);
            }
            $dir_exist = true; // mengecek direktori jika sudah ada atau tidak ada
            if (!is_dir('assets/buktiverifbayar/')) {
                mkdir('./assets/buktiverifbayar/', 0777, true);
                $dir_exist = false; // direktori sudah ada
            }

                if ( ! $this->upload->do_upload('filefotonya')){
                    $error = array('error' => $this->upload->display_errors());
        $_SESSION['status']='Toastr("Format foto bukti transfer tidak sesuai","Info")';
          redirect('r/verifikasi-Bayar/'.encryptURL($_POST['__url__']),'refresh');
                }else{ 
                    $upload_data = $this->upload->data(); 
                    $data['fotobuktitransfer'] = $upload_data['file_name'];
                }

          if(saveData('tjualbayar',$data)){
            $uData=array('statusproses'=>2,
                         'stkonfirmbayarmember'=>1,
                         'nourutalamat'=>($_POST['jenisalamat']==0)?1:$_POST['jenisalamat']
                       );
for($i=0;$i<$_POST['rowTjualDetail'];$i++){
            $idprod=$_POST['idproduk'][$i];
            $tJualdata=array('catatanupenjual'=>$_POST['catatanpermintaan'][$i]);
            TjualDetail($_POST['idtransaksi'],$_POST['idproduk'][$i],'update',$tJualdata);
}
             upTblJual('idtransaksi', $this->input->post('idtransaksi'),'tjual',$uData);
        $_SESSION['status']='ToastrSukses("Produk yang anda beli berhasil dibayar","Info")';
          redirect('r/verifikasi-Bayar','refresh');
            }

         }else{
          $this->failed_save2();
        }

    }

function gantiEkspedisi(){
  $data['idekspedisi']=$_POST['idekspedisi'];
  $where=array('idanggota'=>$_SESSION['user_id'],'idtransaksi'=>$_POST['idtransaksi']);
  updateData($where,$data,'tjual');
  echo json_encode($data);
}

function BHpesanan(){
  // $jmljual=$_POST['jumlahpesan'];
  // if($jmljual==0){
$tipe='delete';
    $data='';
    $idtrans=$_POST['idtransaksi'];
  // }else{
    // $tipe='update';
    // $data['jumlahjual']=$jmljual;
  // }
    $where=array('idtransaksi'=>$idtrans);
      TjualDetail($idtrans,$_POST['idproduk'],$tipe,$data);
      $row=getwhere('idtransaksi',$idtrans,'tjualdetail')->num_rows();
if($row==0){
  deleteData($where,'tjual');
}
    echo json_encode($row);
}

    function failed_save2(){
        $data = $this->input->post();
        $data['error']      =validation_errors();
        $data['toptitle']   ='Verifikasi Pembayaran Produk';
        $data['title']      ='Verifikasi Pembayaran Produk';
        $data['content']    ='Tverifikasi/bayarproduk';
        // $data['idpembayaran'] =$this->tv->getidbayardaftarmember();

        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }

public function prosesInputTerimaProduk(){
        // $form=$this->form_validation;
        $data = new stdClass();
        $getPenjual=getwhere('idtransaksi',$this->input->post('idtransaksi'),'tjual')->row();
        $dataPenjual=getwhere('idanggota',$getPenjual->idanggotapartner,'manggota')->row();
        $getPembeli=getwhere('idanggota',$_SESSION['user_id'],'manggota')->row();
        
        $where = array(
                    'idtransaksi' => $this->input->post('idtransaksi')
                  );
        $data = array('statusproses'=>5,'tanggalterima'=>date('Y-m-d', strtotime($_POST['tanggalterima'])),'catatanterima'=>$_POST['catatantransaksi'],'ratingtransaksi'=>$_POST['ratingtransaksi']);
      $link='https://partner.kulcimart.com/tjual_rekapitulasi/index/5';
      // if(
      // ){
      updateData($where,$data,'tjual');
        kirimEmail($dataPenjual, $getPembeli);
            $_SESSION['status']='ToastrSukses("Produk yang anda beli berhasil diterima","Info")';
            redirect('r/verifikasi-Terima','refresh');
          // }else{
          //   // show_error($this->email->print_debugger());
          // $_SESSION['status']='Toastr("Produk yang anda beli gagal diterima","Info")';
          //   redirect('r/verifikasi-Terima','refresh');  
          // }
    }

    function failed_save3(){
        // $data = $this->input->post();
        $data['error']      =validation_errors();
        $data['toptitle']   ='Verifikasi Pembayaran Produk';
        $data['title']      ='Verifikasi Pembayaran Produk';
        $data['content']    ='Tverifikasi/bayarproduk';
        // $data['idpembayaran'] =$this->tv->getidbayardaftarmember();

        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }


public function getJENAL()
    { 
        if($_POST['nourut']==0){
            $postID='stutama';
            $idnya=1;
        }else{
            $postID='nourut';
            $idnya=$_POST['nourut'];
        }
        // print_r($postID);exit();
        $bb = $this->tv->showAlju($_POST['nourut'],$idnya)->result();
        $bb = getDoubleWhere('idanggota',$_SESSION['user_id'],$postID,$idnya,'manggotaalamattujuan');
   
        $dataalamattujuan_json = '{"jenal":[';
        foreach ($bb->result() as $aa) {
                    $dataalamattujuan_json .= '{"idanggota":"'.$aa->idanggota.'","alamat":"'.$aa->alamat.'","desa":"'.$aa->desa.'","kecamatan":"'.$aa->kecamatan.'","kota":"'.$aa->kota.'","provinsi":"'.$aa->provinsi.'","kodepos":"'.$aa->kodepos.'","lat":"'.$aa->lat.'","long":"'.$aa->long.'"},';
        }
        $dataalamattujuan_json = substr($dataalamattujuan_json, 0, strlen($dataalamattujuan_json) - 1);
        $dataalamattujuan_json .= ']}';
    echo $dataalamattujuan_json;
}

  }
?>