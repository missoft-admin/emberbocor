<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    /**
     * Dashboard controller.  
     * Developer denipurnama371@gmail.com
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
    		$this->load->model('Dashboard_model','dash');
    }

    public function index()
    {
$Bfil_partfav   =($this->input->post('Bfil_partfav')==null)?date('m'):$this->input->post('Bfil_partfav');
$Tfil_partfav   =($this->input->post('Tfil_partfav')==null)?date('Y'):$this->input->post('Tfil_partfav');
    $per_partfav    =implode('', array($Bfil_partfav, $Tfil_partfav));
$Bfil_memfav    =($this->input->post('Bfil_memfav')==null)?date('m'):$this->input->post('Bfil_memfav');
$Tfil_memfav    =($this->input->post('Tfil_memfav')==null)?date('Y'):$this->input->post('Tfil_memfav');
    $per_memfav    =implode('', array($Bfil_memfav, $Tfil_memfav));
$Bfil_prodfav   =($this->input->post('Bfil_prodfav')==null)?'02':$this->input->post('Bfil_prodfav');
$Tfil_prodfav   =($this->input->post('Tfil_prodfav')==null)?date('Y'):$this->input->post('Tfil_prodfav');
    $per_prodfav    =implode('', array($Bfil_prodfav, $Tfil_prodfav));
$Bfil_partspon    =($this->input->post('Bfil_partspon')==null)?date('m'):$this->input->post('Bfil_partspon');
$Tfil_partspon    =($this->input->post('Tfil_partspon')==null)?date('Y'):$this->input->post('Tfil_partspon');
    $per_partspon    =implode('', array($Bfil_partspon, $Tfil_partspon));
        $data = array();
        $data['toptitle']   = 'Dashboard';
        $data['title']      = 'Dashboard';
        $data['content']    = 'dashboard';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?date('m'):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('bulan_fil')==null)?date('Y'):$this->input->post('tahun_fil');
            $data['bulan_prodfav']      =$Bfil_prodfav;
            $data['tahun_prodfav']      =$Tfil_prodfav;
            $data['bulan_partfav']      =$Bfil_partfav;
            $data['tahun_partfav']      =$Tfil_partfav;
            $data['bulan_memfav']       =$Bfil_memfav;
            $data['tahun_memfav']       =$Tfil_memfav;
            $data['bulan_partspon']     =$Bfil_partspon;
            $data['tahun_partspon']     =$Tfil_partspon;

            $data['cekidanggota'] =getwhere('idanggotamember',$_SESSION['user_id'],'tbayardaftarmember')->num_rows();
            $data['partnerfav1']  =$this->dash->getTpartnerFav(0,$per_partfav);
            $data['partnerfav2']  =$this->dash->getTpartnerFav(5,$per_partfav);
            $data['produkfav1']   =$this->dash->getTprodFav(0,'desc',$per_prodfav);
            $data['produkfav2']   =$this->dash->getTprodFav(0,'asc',$per_prodfav);
            $data['memspon1']     =$this->dash->getTmemspon(0,'desc',$per_memfav);
            $data['memspon2']     =$this->dash->getTmemspon(0,'asc',$per_memfav);
            $data['partspon1']    =$this->dash->getTpartspon(0,'desc',$per_partspon);
            $data['partspon2']    =$this->dash->getTpartspon(0,'asc',$per_partspon);
            // $data['terbanyaktransaksi1']  = $this->dash->getTransaksiTerbanyak(0);
            // $data['terbanyaktransaksi2']  = $this->dash->getTransaksiTerbanyak(5);
        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }

    public function filterhistoribelanja()
    {
        $data = array();
        $data['toptitle']   = 'Dashboard';
        $data['title']      = 'Dashboard';
        $data['content']    = 'dashboard';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?date('m'):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('bulan_fil')==null)?date('Y'):$this->input->post('tahun_fil');

            $data['cekidanggota'] =$this->dash->cekidanggota('idanggotamember',$_SESSION['user_id'],'tbayardaftarmember')->num_rows();
            $data['partnerfav1']  =$this->dash->getTpartnerFav(0);
            $data['partnerfav2']  =$this->dash->getTpartnerFav(5);
            $data['produkfav1']   =$this->dash->getTprodFav(0,'desc');
            $data['produkfav2']   =$this->dash->getTprodFav(0,'asc');
            $data['memspon1']     =$this->dash->getTmemspon(0,'desc');
            $data['memspon2']     =$this->dash->getTmemspon(0,'asc');
            $data['partspon1']    =$this->dash->getTpartspon(0,'desc');
            $data['partspon2']    =$this->dash->getTpartspon(0,'asc');
            // $data['terbanyaktransaksi1']  = $this->dash->getTransaksiTerbanyak(0);
            // $data['terbanyaktransaksi2']  = $this->dash->getTransaksiTerbanyak(5);
        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }

        public function getRiwayat(){
$tahun=$this->input->get('tahun_fil');
$bulan=$this->input->get('bulan_fil');
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter=" and `t2`.`periodetransaksi`='".$periode."'";
}else{
$filter="";
}
                $table = 'manggota'; 
                $primaryKey = 'idanggota';

                $columns = array( 
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 0, 'field' => 'idtransaksi' ),
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 1, 'field' => 'idtransaksi' ),

                    array( 'db' => '`t2`.`tanggaltransaksi`', 'dt' => 2, 'field' => 'tanggaltransaksi', 'formatter' => function( $d, $row ) {
                           return date('d-m-Y',strtotime($d));
                       }), 
                    array('db'  => '`t2`.`idanggotapartner`',     'dt' => 3, 'field' => 'idanggotapartner', 'formatter' => function( $d, $row ) {
                           return getwhere('t1.idanggota',$d,'manggota')->row()->namaanggota;
                       }),
                    array('db'  => 'sum(`t2`.`totalharga`) as totalharga', 'dt' => 4, 'field' => 'totalharga', 'formatter' => function( $d, $row ) {
                           return 'Rp.'.number_format($d);
                       }),
                    array('db'  => '`t2`.`statusproses`', 'dt' => 5, 'field' => 'statusproses', 'formatter' => function( $d, $row ) {
                           return statusJualBeli($d);
                       })
                
                );
                    $sql_details = array(
                        'user' => 'kulcimart',
                        'pass' => 'kurvasoft@2018',
                        'db'   => 'kulcimar_development',
                        'host' => '103.29.212.140');

                    // $this->load->library('Datatables_ssp');
                    $joinQuery = "FROM `manggota` as `t1` LEFT JOIN `tjual` AS `t2` ON (`t1`.`idanggota` = `t2`.`idanggota`)";
                    $extraWhere = "`t1`.`idanggota` ='".$_SESSION['user_id']."' $filter";
                    $groupBy = "`t2`.`idtransaksi`";
                    $having = "";
                    $ordercus = "order by `t2`.`statusproses` asc";
                    echo json_encode(
                        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
                    );
        }

}
