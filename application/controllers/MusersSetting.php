<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MusersSetting extends CI_Controller
{

    /**
     * Dashboard controller.
     * Developer denipurnama371@gmail.com
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model('Users_model','u');
            // $_SESSION['logged_in']=false;
            $this->form_validation->set_error_delimiters('<label>', '</label>');
    }

    public function index()
    {
        if(empty($_SESSION['logged_in'])){
            redirect('SignIn','refresh');
        }
    }

    public function tampildatapribadi(){
            $id=$_SESSION['user_id'];
        $data = array();
        $data = $this->u->getInformasi($id);
        $data['error']      = '';
        $data['toptitle']   = 'Data Pribadi';
        $data['title']      = 'Data Pribadi';
        $data['content']    = 'Musers/datapribadi';
        $data['nagama']     =getwhere('idanggota',$id,'manggota')->row()->agama;
        $data['hubahliwaris']     =getwhere('idanggota',$id,'manggota')->row()->hubunganwaris;
        $data['fotomember'] =getwhere('idanggota',$id,'manggota')->row()->fotoprofil;
        // $data['dataAlamat'] =getwhere('idanggota',$_SESSION['user_id'],'manggotaalamattujuan')->result();
        $data['dataAlamat'] =$this->u->getAlamatT7an()->result();
        $data['defaultimg'] ='no_avatar.png';

        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }
    
    public function tampildatarekening(){
        $data = array();
        $data['error']      = '';
        $data['toptitle']   = 'Data Rekening';
        $data['title']      = 'Data Rekening';
        $data['content']    = 'Musers/datarekening';
        $data['nbank']      =getwhere('idanggota',$_SESSION['user_id'],'manggota')->row()->namabank;

        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }

    public function editdatapassword(){
        $data = array();
        $data['error']      = '';
        $data['toptitle']   = 'Ubah Kata Sandi';
        $data['title']      = 'Ubah Kata Sandi';
        $data['content']    = 'Musers/datapassword';

        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }

    public function tampilpetajaringan(){
        $data = array();
        $data['error']      = '';
        $data['toptitle']   = 'Peta Anggota Rekomendasi';
        $data['title']      = 'Peta Anggota Rekomendasi';
        $data['content']    = 'Musers/petajaringan';
    $tipetampil=$this->input->post('tipetampildata');
$data['tipetampildata']  =($tipetampil==null)?'':$tipetampil;
        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }

    public function changePWD(){
    $this->form_validation->set_rules('old_password', 'Password', 'trim|required');
    $this->form_validation->set_rules('newpassword', 'New Password', 'required|matches[re_password]');
    $this->form_validation->set_rules('re_password', 'Retype Password', 'required');
    if($this->form_validation->run() == FALSE){
        $_SESSION['status']='Toastr("Silahkan isi kata sandi anda dengan benar","Info")';
        redirect('u/users/settings/datapassword','refresh');
    }else{
      $query = $this->u->checkOldPass(hash('sha256', md5($this->input->post('old_password'))));
      if($query){
        $query2 = $this->u->saveNewPass(hash('sha256', md5($this->input->post('newpassword'))));
        if($query2){
            // setFlashMsg()
        $_SESSION['status']='ToastrSukses("Kata sandi anda berhasil diubah","Info")';
          redirect('u/users/settings/datapassword');
        }else{
        $_SESSION['status']='Toastr("Kata sandi anda gagal diubah","Info")';
          redirect('u/users/settings/datapassword');
        }
      }else{
        $_SESSION['status']='Toastr("Kata sandi lama anda salah","Info")';
          redirect('u/users/settings/datapassword');
      }
      }
      }

    function upDATA(){
date_default_timezone_set("Asia/Jakarta");
$newNAMEimg='Foto Pembeli '.$_SESSION['user_id'].' pada '.date("Y-m-d H-i-s");
$where['idanggota'] =$_SESSION['user_id']; 
//        ############################### Start Update Profil ###############################
        if($this->input->post('datauptipe')==0){
            $tgllahir=$this->input->post('tanggallahir');
                $data['noktp'] =$this->input->post('noktp');
                $data['tanggaldaftar']      =$this->input->post('tgldaftar');
                $data['jenisanggota']       =$this->input->post('jenispembeli');
                $data['namaanggota']        =$this->input->post('namaanggota');
                $data['tempatlahir']        =$this->input->post('tempatlahir');
                $data['tanggallahir']       =date('Y-m-d', strtotime($tgllahir));
                $data['agama']              =$this->input->post('agamaanggota');
                $data['jeniskelamin']       =$this->input->post('jeniskelamin');
                $data['alamat']             =$this->input->post('alamatanggota');
                $data['desa']               =$this->input->post('namadesa');
                $data['kecamatan']          =$this->input->post('namakecamatan');
                $data['kota']               =$this->input->post('namakota');
                $data['provinsi']           =$this->input->post('namaprovinsi');
                $data['kodepos']            =$this->input->post('kodepos');
                $data['telepon']            =$this->input->post('notelp');
                $data['statuspernikahan']   =$this->input->post('stkawin');
                $data['ahliwaris']          =$this->input->post('nmahliwaris');
                $data['hubunganwaris']      =$this->input->post('hubahliwaris');
                $data['email']              =$this->input->post('dataemail');
                $data['idsponsor']          =$this->input->post('idsponsor');
                $data['hp']                 =$this->input->post('nohp');
                $data['hpwa']               =$this->input->post('nohpwa'); 
                $data['facebook']           =$this->input->post('idfacebook'); 
                $data['twitter']            =$this->input->post('idtwitter'); 
                $data['lat']                =$this->input->post('lat'); 
                $data['lon']                =$this->input->post('lng'); 
                $data['updateterakhiroleh'] =$_SESSION['namalengkap'];

if($this->input->post('upfotoga')==1){
            $config['upload_path']          = './assets/fotomember/';
            $config['allowed_types']        = '*';
            $config['max_size']             = 10000;
$config['file_name'] = $newNAMEimg;
            $this->load->library('upload',$config);
            $this->upload->initialize($config);

        if (!is_dir('assets/fotomember')) {
            mkdir('./assets/fotomember', 0777, true);
        }
        $dir_exist = true; // mengecek direktori jika sudah ada atau tidak ada
        if (!is_dir('assets/fotomember/')) {
            mkdir('./assets/fotomember/', 0777, true);
            $dir_exist = false; // direktori sudah ada
        }
$img=$this->input->post('fakefoto');
            if ( ! $this->upload->do_upload('user_file')){
        $_SESSION['status']='Toastr("Format foto tidak mendukung","Info")';
                redirect('u/users/settings/datapribadi');
            }else{ 
                // $upload_data = $this->upload->data();
                $widthex=explode(".",$this->input->post('reswidth'));
                $heightex=explode(".",$this->input->post('resheight'));
                $x_axis=explode(".",$this->input->post('x_axis'));
                $y_axis=explode(".",$this->input->post('y_axis'));
                $upload_data = $this->upload->data();
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $upload_data['full_path']; //get original image
                    $config['maintain_ratio'] = false;
                    $config['quality'] = '100%';
                    $config['width']    = $widthex[0];
                    $config['height']   = $widthex[0];
                    $config['x_axis']   = $x_axis[0];
                    $config['y_axis']   = $y_axis[0];
                    $config['overwrite'] = TRUE;
                    $this->load->library('image_lib', $config);
                    $this->image_lib->initialize($config);
                    if ($this->image_lib->crop()) {
                    $resize['width'] = 360;
                    $resize['height'] = 360;
                    $this->image_lib->initialize($resize);
                    $this->image_lib->resize();
                        // $this->handle_error($this->image_lib->display_errors());
                $data['fotoprofil'] = $upload_data['file_name'];
                    }
            }
            $path_to_file = 'assets/fotomember/' . $this->input->post('fpmemberold-hidden');
            if($this->input->post('fpmemberold-hidden')!=='0'){
                        unlink($path_to_file); // HAPUS FILE GAMBAR LAMA
                    }
}else{
    $data['fotoprofil'] =$this->input->post('fpmemberold-hidden');
}
            $redir='u/users/settings/datapribadi';
//        ################################# End Update Profil #################################
        }else{
//        ############################### Start Update Rekening ###############################
                $data['namabank']   =$this->input->post('namabank');
                $data['cabangbank'] =$this->input->post('cabangbank');
                $data['norekening'] =$this->input->post('norekening');
                $data['atasnama'] =$this->input->post('atasnama');
                $data['updateterakhiroleh'] =$_SESSION['namalengkap'];
                $redir='u/users/settings/datarekening';
            }
                // $data['updateterakhiroleh'] =$_SESSION['namalengkap'];
            if($this->u->upDATA($where,$data,'manggota')){
        $_SESSION['status']='ToastrSukses("Data berhasil disimpan","Info")';
                redirect($redir);
            }else{
        $_SESSION['status']='Toastr("Data gagal disimpan","Info")';
                redirect('u/users/settings/datapribadi');
            }
//        ############################### End Update Rekening ###############################
    }

public function getPROFIL()
    {   
        $bb = getwhere('idanggota',$_SESSION['user_id'],'manggota')->row();
        $dataprofil_json = '{"users":[';
                    $dataprofil_json .= '{"namaanggota":"'.$bb->namaanggota.'",
                    "noktp":"'.$bb->noktp.'",
                    "alamat":"'.$bb->alamat.'",
                    "notelp":"'.$bb->telepon.'",
                    "nohp":"'.$bb->hp.'",
                    "nohpwa":"'.$bb->hpwa.'",
                    "tempatlahir":"'.$bb->tempatlahir.'",
                    "tanggallahir":"'.date('d-m-Y', strtotime($bb->tanggallahir)).'",
                    "agamaanggota":"'.$bb->agama.'",
                    "jeniskelamin":"'.$bb->jeniskelamin.'",
                    "provinsi":"'.$bb->provinsi.'",
                    "idanggota":"'.$bb->idanggota.'",
                    "tanggaldaftar":"'.HumanDateShort($bb->tanggaldaftar).'",
                    "jenisanggota":"'.$bb->jenisanggota.'",
                    "statuspernikahan":"'.$bb->statuspernikahan.'",
                    "ahliwaris":"'.$bb->ahliwaris.'",
                    "hubunganwaris":"'.$bb->hubunganwaris.'",
                    "email":"'.$bb->email.'",
                    "idsponsor":"'.$bb->idsponsor.'",
                    "facebook":"'.$bb->facebook.'",
                    "nmsponsor":"'.getwhere('idanggota',$bb->idsponsor,'manggota')->row()->namaanggota.'",
                    "twitter":"'.$bb->twitter.'"},';
        $dataprofil_json = substr($dataprofil_json, 0, strlen($dataprofil_json) - 1);
        $dataprofil_json .= ']}';
    echo $dataprofil_json;
}

public function getREK()
    {   
        $bb = getwhere('idanggota',$_SESSION['user_id'],'manggota')->row();
        $datarekening_json = '{"rekening":[';
                    $datarekening_json .= '{"namabank":"'.$bb->namabank.'","cabangbank":"'.$bb->cabangbank.'","norekening":"'.$bb->norekening.'","atasnama":"'.$bb->atasnama.'"},';
        $datarekening_json = substr($datarekening_json, 0, strlen($datarekening_json) - 1);
        $datarekening_json .= ']}';
    echo $datarekening_json;
}

public function selectJenal()
    {
        $bb = $this->u->getAlamatT7an()->result();
        $dataseljenal_json = '{"countROW":'.getwhere('idanggota',$_SESSION['user_id'],'manggotaalamattujuan')->num_rows().',';
        $dataseljenal_json .= '"jenisala":[';
        if(count($bb)==0){
        $dataseljenal_json .= '{"tidakada":"ya"},';
            
        }else{
        foreach ($bb as $aa) {
                    $dataseljenal_json .= '{"nourut":"'.$aa->nourut.'","idprov":"'.$aa->provinsi.'","idkota":"'.$aa->kota.'","idkec":"'.$aa->kecamatan.'","iddesa":"'.$aa->desa.'","nmprov":"'.getrow('id',$aa->provinsi,'mprovinsi')->nama.'","nmkota":"'.getrow('id',$aa->kota,'mkota')->nama.'","nmkec":"'.getrow('id',$aa->kecamatan,'mkecamatan')->nama.'","nmdesa":"'.getrow('id',$aa->desa,'mdesa')->nama.'","alamatjln":"'.$aa->alamat.'","kodepos":"'.$aa->kodepos.'","lat":"'.$aa->lat.'","lng":"'.$aa->long.'" },';
        }
        }
        $dataseljenal_json = substr($dataseljenal_json, 0, strlen($dataseljenal_json) - 1);
        $dataseljenal_json .= ']}';
    echo $dataseljenal_json;
    // echo $datarow='{"countROW":['.getwhere('idanggota',$_SESSION['user_id'],'manggotaalamattujuan')->num_rows().']}';
}

public function getPROV()
    {
        $bb = $this->u->getProv('mprovinsi')->result();
        $dataprovinsi_json = '{"namaprov":[';
        foreach ($bb as $aa) {
                    $dataprovinsi_json .= '{"idprov":"'.$aa->id.'","namaprovinsi":"'.$aa->nama.'"},';
        }
        $dataprovinsi_json = substr($dataprovinsi_json, 0, strlen($dataprovinsi_json) - 1);
        $dataprovinsi_json .= ']}';
    echo $dataprovinsi_json;
}

public function getKOTA()
    {
        $idprov=$this->input->post('idprov');
        $bb = $this->u->getDesKecKot('provinsiid',$idprov,'mkota')->result();
        $datakota_json = '{"namakot":[';
        foreach ($bb as $aa) {
                    $datakota_json .= '{"idkota":"'.$aa->id.'","namakota":"'.$aa->nama.'"},';
        }
        $datakota_json = substr($datakota_json, 0, strlen($datakota_json) - 1);
        $datakota_json .= ']}';
    echo $datakota_json;
}

public function getKEC()
    {
        $idkota=$this->input->post('idkota');
        $bb = $this->u->getDesKecKot('kotaid',$idkota,'mkecamatan')->result();
        $datakec_json = '{"namakec":[';
        foreach ($bb as $aa) {
                    $datakec_json .= '{"idkec":"'.$aa->id.'","namakecamatan":"'.$aa->nama.'"},';
        }
        $datakec_json = substr($datakec_json, 0, strlen($datakec_json) - 1);
        $datakec_json .= ']}';
    echo $datakec_json;
}

public function getDESA()
    {
        $idkec=$this->input->post('idkec');
        $bb = $this->u->getDesKecKot('kecamatanid',$idkec,'mdesa')->result();
        $datadesa_json = '{"namades":[';
        foreach ($bb as $aa) {
                    $datadesa_json .= '{"iddesa":"'.$aa->id.'","namadesa":"'.$aa->nama.'"},';
        }
        $datadesa_json = substr($datadesa_json, 0, strlen($datadesa_json) - 1);
        $datadesa_json .= ']}';
    echo $datadesa_json;
}

public function getKOPOS()
    {
        $iddesa=$this->input->post('iddesa');
        $aa = $this->u->getDesKecKot('desaid',$iddesa,'mkodepos')->row();
        $datakopos_json = '{"kopos":[';
        // foreach ($bb as $aa) {
                    $datakopos_json .= '{"idkopos":"'.$aa->id.'","kodepos":"'.$aa->kodepos.'"},';
        // }
        $datakopos_json = substr($datakopos_json, 0, strlen($datakopos_json) - 1);
        $datakopos_json .= ']}';
    echo $datakopos_json;
}

public function saveEditAlamatBaru()
    {
        // print_r($_POST['stutama']);exit();
$countSTU=getDoubleWhere('idanggota',$_SESSION['user_id'],'stutama',1,'manggotaalamattujuan');
        $data['idanggota']  =$_SESSION['user_id'];
        $data['nourut']     =$_POST['nourut'];
        $data['provinsi']   =$_POST['idprov'];
        $data['kota']       =$_POST['idkota'];
        $data['kecamatan']  =$_POST['idkeca'];
        $data['desa']       =$_POST['iddesa'];
        $data['kodepos']    =$_POST['kopos'];
        $data['alamat']     =$_POST['alamat'];
        $data['lat']     =$_POST['lat'];
        $data['long']     =$_POST['long'];
$where=array('nourut'=>$_POST['nourut'],'idanggota'=>$_SESSION['user_id']);
$where2=array('stutama'=>1,'idanggota'=>$_SESSION['user_id']);
$where3=array('idanggota'=>$_SESSION['user_id']);
// $data2['alamat']     =$_POST['alamat'];
        if($_POST['stutama']==1){
            $data['stutama']    =1;
        updateData($where,$data,'manggotaalamattujuan');
        if($countSTU->num_rows()>0){
            $data2['stutama']    =0;
        updateData($where2,$data2,'manggotaalamattujuan');
        }
        }else{
            $data['stutama']    =0;
        }
        if($_POST['tipe']=='baru'){
        $data['staktif']    =1;
            saveData('manggotaalamattujuan',$data);
        }else
        if($_POST['tipe']=='edit'){
            // update manggotaalamattujuan
            updateData($where,$data,'manggotaalamattujuan');
            // update manggotaalamattujuan

        $data3['idanggota']  =$_SESSION['user_id'];
        $data3['provinsi']   =$_POST['idprov'];
        $data3['kota']       =$_POST['idkota'];
        $data3['kecamatan']  =$_POST['idkeca'];
        $data3['desa']       =$_POST['iddesa'];
        $data3['kodepos']    =$_POST['kopos'];
        $data3['alamat']     =$_POST['alamat'];
        $data3['lat']        =$_POST['lat'];
        $data3['lon']        =$_POST['long'];
            updateData($where3,$data3,'manggota');
        }
        echo json_encode($data);
            // }
        // }
}

public function getJENAL()
    { 
        if($_POST['nourut']==0){
            $postID='stutama';
            $idnya=1;
        }else{
            $postID='nourut';
            $idnya=$_POST['nourut'];
        }
        // print_r($postID);exit();
        $bb = $this->u->showAlju($_POST['nourut'],$idnya)->result();
        $bb = getDoubleWhere('idanggota',$_SESSION['user_id'],$postID,$idnya,'manggotaalamattujuan');
   
        $dataalamattujuan_json = '{"jenal":[';
        foreach ($bb->result() as $aa) {
                    $dataalamattujuan_json .= '{"idanggota":"'.$aa->idanggota.'","alamat":"'.$aa->alamat.'","desa":"'.$aa->desa.'","kecamatan":"'.$aa->kecamatan.'","kota":"'.$aa->kota.'","provinsi":"'.$aa->provinsi.'","kodepos":"'.$aa->kodepos.'","lat":"'.$aa->lat.'","long":"'.$aa->long.'"},';
        }
        $dataalamattujuan_json = substr($dataalamattujuan_json, 0, strlen($dataalamattujuan_json) - 1);
        $dataalamattujuan_json .= ']}';
    echo $dataalamattujuan_json;
}

function getPetaJaringan(){
// $tahun=$this->input->get('tahun_fil');
// $bulan=getABulan($this->input->get('bulan_fil'));
// $periode=implode('', array($tahun,$bulan));
// if($periode){
// $filter=" and `t2`.`periodetransaksi`='".$periode."'";
// }else{
// $filter="";
// }
    $this->penjual();
    $this->pembeli();
}

function getPetaJaringanPenjual(){
// $tahun=$this->input->get('tahun_fil');
// $bulan=getABulan($this->input->get('bulan_fil'));
// $periode=implode('', array($tahun,$bulan));
// if($periode){
// $filter=" and `t2`.`periodetransaksi`='".$periode."'";
// }else{
// $filter="";
// }
        $table = 'manggota'; 
        $primaryKey = 'idanggota';

        $columns = array( 
        // array( 'db' => '`t1`.`idtransaksi`', 'dt' => 0, 'field' => 'idtransaksi' ),
        array( 'db' => '`t1`.`idtransaksi`', 'dt' => 0, 'field' => 'idtransaksi' ),
        array( 'db' => '`t2`.`tanggaldaftar`', 'dt' => 1, 'field' => 'tanggaldaftar', 'formatter' => function( $d, $row ) {
               return HumanDateShort($d);
           }),
        array( 'db' => '`t2`.`idanggota`', 'dt' => 2, 'field' => 'idanggota', 'formatter' => function( $d, $row ) {
               return $d.' <b>(PENJUAL)</b>';
           }),
        array('db'  => '`t2`.`namaanggota`', 'dt' => 3, 'field' => 'namaanggota' ),
        array('db'  => '`t2`.`alamat`', 'dt' => 4, 'field' => 'alamat' ),
        array('db'  => 'COUNT(`t1`.`idtransaksi`) As totaltransaksi', 'dt' => 5, 'field' => 'totaltransaksi', 'formatter' => function( $d, $row ) {
               return $d.' Transaksi';
           }),
        array('db'  => 'SUM(`t1`.`totalharga`) As nominaltransaksi', 'dt' => 6, 'field' => 'nominaltransaksi', 'formatter' => function( $d, $row ) {
            $total=($d!='')?$d:'0';
               return number_format($total);
           })      
        );
    $sql_details = sql_connect();

    $joinQuery = "FROM `$table` as `t2` LEFT JOIN `tjual` as `t1` ON ( `t2`.`idanggota` = `t1`.`idanggota` )";
    $joinQuery.= "LEFT JOIN `tjualdetail` AS `t3` ON (`t3`.`idtransaksi` = `t1`.`idtransaksi`)";
    $extraWhere = "`t2`.`idsponsor` ='".$_SESSION['user_id']."' AND
(LEFT(`t2`.`idanggota`,1)='A' ) ";
    $groupBy = "`t2`.`idanggota`";
    $having = "";
    $ordercus = "order by namaanggota asc";
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );
}
function getPetaJaringanPembeli(){
// $tahun=$this->input->get('tahun_fil');
// $bulan=getABulan($this->input->get('bulan_fil'));
// $periode=implode('', array($tahun,$bulan));
// if($periode){
// $filter=" and `t2`.`periodetransaksi`='".$periode."'";
// }else{
// $filter="";
// }
        $table = 'manggota'; 
        $primaryKey = 'idanggota';

        $columns = array( 
        // array( 'db' => '`t1`.`idtransaksi`', 'dt' => 0, 'field' => 'idtransaksi' ),
        array( 'db' => '`t1`.`idtransaksi`', 'dt' => 0, 'field' => 'idtransaksi' ),

        array( 'db' => '`t2`.`tanggaldaftar`', 'dt' => 1, 'field' => 'tanggaldaftar', 'formatter' => function( $d, $row ) {
               return HumanDateShort($d);
           }),
        array( 'db' => '`t2`.`idanggota`', 'dt' => 2, 'field' => 'idanggota', 'formatter' => function( $d, $row ) {
               return $d.' <b>(PEMBELI)</b>';
           }),
        array( 'db' => '`t2`.`namaanggota`', 'dt' => 3, 'field' => 'namaanggota' ),
        array('db'  => '`t2`.`alamat`', 'dt' => 4, 'field' => 'alamat'),
        array('db'  => 'IF((`t1`.`statusproses`) >= 5, COUNT(`t3`.`idtransaksi`),0) As totaltransaksi', 'dt' => 5, 'field' => 'totaltransaksi', 'formatter' => function( $d, $row ) {
               return $d.' Transaksi';
           }),
        array('db'  => 'IF((`t1`.`statusproses`) >= 5, SUM(`t3`.`hargajual` * `t3`.`jumlahjual`),0) As nominaltransaksi', 'dt' => 6, 'field' => 'nominaltransaksi', 'formatter' => function( $d, $row ) {
            $total=($d!='')?$d:'0';
               return number_format($total);
           })      
        );
    $sql_details = sql_connect();

    $joinQuery = "FROM `$table` as `t2` LEFT JOIN `tjual` AS `t1` ON (`t1`.`idanggota` = `t2`.`idanggota`)";
    $joinQuery.= "LEFT JOIN `tjualdetail` AS `t3` ON (`t3`.`idtransaksi` = `t1`.`idtransaksi`)";
    $extraWhere = "`t2`.`idsponsor` ='".$_SESSION['user_id']."' AND
LEFT(`t2`.`idanggota`,1)='B'";
    $groupBy = "`t2`.`idanggota`";
    $having = "";
    $ordercus = "order by namaanggota asc";
    echo json_encode(
        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
    );
}

function gantiStatAl() {
        $id= $_POST["id"]; 
    $aa=$this->u->gantiStatus($id);
// if($aa){
        $bb = $this->u->dataStatus($id);
        $dataAlamat_json = '['.$bb.']';
        // $dataAlamat_json .= '}';
    echo $dataAlamat_json;
// }
    // echo "{}";
    }


}