<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    /**
     * Dashboard controller.  
     * Developer denipurnama371@gmail.com
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
    		$this->load->model('Dashboard_model','dash');
    }

    public function index()
    {
        $data = array();
        $data['error']      = '';
        $data['toptitle']   = 'Dashboard';
        $data['title']      = 'Dashboard';
        $data['content']    = 'dashboard';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('bulan_fil')==null)?date('Y'):$this->input->post('tahun_fil');

            $data['cekidanggota'] =getwhere('idanggotamember',$_SESSION['user_id'],'tbayardaftarmember')->num_rows();
            $data['partnerfav1']  =$this->dash->getTpartnerFav(0);
            $data['partnerfav2']  =$this->dash->getTpartnerFav(5);
            $data['produkfav1']   =$this->dash->getTprodFav(0,'desc');
            $data['produkfav2']   =$this->dash->getTprodFav(0,'asc');
            $data['memspon1']     =$this->dash->getTmemspon(0,'desc');
            $data['memspon2']     =$this->dash->getTmemspon(0,'asc');
            $data['partspon1']    =$this->dash->getTpartspon(0,'desc');
            $data['partspon2']    =$this->dash->getTpartspon(0,'asc');
            // $data['terbanyaktransaksi1']  = $this->dash->getTransaksiTerbanyak(0);
            // $data['terbanyaktransaksi2']  = $this->dash->getTransaksiTerbanyak(5);
        $data = array_merge($data, path_variable());
        $this->parser->parse('page_template', $data);
    }

        public function getRiwayat(){
$tahun=$this->input->get('tahun_fil');
$bulan=getABulan($this->input->get('bulan_fil'));
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter=" and `t2`.`periodetransaksi`='".$periode."'";
}else{
$filter="";
}
                $table = 'tjual'; 
                $primaryKey = 'idanggota';

                $columns = array( 
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 0, 'field' => 'idtransaksi' ),
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 1, 'field' => 'idtransaksi' ),

                    array( 'db' => '`t2`.`tanggaltransaksi`', 'dt' => 2, 'field' => 'tanggaltransaksi', 'formatter' => function( $d, $row ) {
                           return date('d-m-Y',strtotime($d));
                       }),
                    // array('db'  => '(select `namaanggota` from `manggota` where `idanggota`=`t2`.`idanggotapartner`) as namaanggota', 'dt' => 3, 'field' => 'namaanggota'),
                    // array('db'  => '`t2`.`idanggotapartner`', 'dt' => 3, 'field' => 'idanggotapartner', 'formatter' => function( $d, $row ) {
                    //        return getwhere('t1.idanggota',$d,'manggota')->row()->namaanggota;
                    //    }),
                    array('db'  => '`t1`.`namaanggota`', 'dt' => 3, 'field' => 'namaanggota' ),
                    array('db'  => '`t2`.`totalbayar`', 'dt' => 4, 'field' => 'totalbayar', 'formatter' => function( $d, $row ) {
                           return 'Rp.'.number_format($d);
                       }),
                    array('db'  => '`t2`.`statusproses`', 'dt' => 5, 'field' => 'statusproses', 'formatter' => function( $d, $row ) {
                           return statusJualBeli($d);
                       })                
                );
            $sql_details = sql_connect();

            $joinQuery = "FROM $table as `t2` LEFT JOIN `manggota` AS `t1` ON (`t1`.`idanggota` = `t2`.`idanggotapartner`) LEFT JOIN `tjualdetail` AS `t3` ON (`t3`.`idtransaksi` = `t2`.`idtransaksi`)";
            $extraWhere = "`t2`.`idanggota` ='".$_SESSION['user_id']."' and (`t2`.`statusproses` <5 $filter )";
            $groupBy = "`t2`.`idtransaksi`";
            // `t1`.`idanggota` ='".$_SESSION['user_id']."'
            $having = "";
            $ordercus = "order by `t2`.`tanggaltransaksi` asc, `t2`.`statusproses` asc";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }

}
