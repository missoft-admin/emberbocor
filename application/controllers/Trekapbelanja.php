<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Trekapbelanja extends CI_Controller
{

    /**
     * Dashboard controller.  
     * Developer denipurnama371@gmail.com
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
            $this->load->model('Trekapbelanja_model','rebel');
    }

    public function historibelanja(){
                $data = array();
                $data['toptitle']   = 'TRANSAKSI DATA';
                $data['title']      = 'Rekapitulasi Belanja';
                $data['content']    = 'Trekapbelanja/Triwayatbelanja';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('tahun_fil')==null)?date('Y'):$this->input->post('tahun_fil');
$bulan=($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
$tahun=($this->input->post('tahun_fil')==null)?date('Y'):$this->input->post('tahun_fil');
$periode=implode('', array($tahun,getABulan($bulan)));
// if($periode){
// $filter=$periode;
// }else{
// $filter="";
// }
$where=array('idanggota'=>$_SESSION['user_id'],'periodetransaksi'=>$periode);
// echo periodeSUM($where,'tjual','totalbayar')->sumHarga;
$data['totalbiaya'] =periodeSUM($where,'tjual','totalbayar')->sumHarga;
                $data = array_merge($data, path_variable());
                $this->parser->parse('page_template', $data);
            }

    public function verifikasipembayaran(){
                $data = array();
                $data['error']      = '';
                $data['toptitle']   = 'TRANSAKSI DATA';
                $data['title']      = 'Upload Bukti Transaksi';
                $data['content']    = 'Trekapbelanja/Tvbayarbelanja';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('tahun_fil')==null)?date('Y'):$this->input->post('tahun_fil');
            
                $data = array_merge($data, path_variable());
                $this->parser->parse('page_template', $data);
            }

    public function verifikasipenerimaan(){
                $data = array();
                $data['error']      = '';
                $data['toptitle']   = 'TRANSAKSI DATA';
                $data['title']      = 'Verifikasi Penerimaan Produk';
                $data['content']    = 'Trekapbelanja/Tvterimabelanja';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('tahun_fil')==null)?date('Y'):$this->input->post('tahun_fil');

                $data = array_merge($data, path_variable());
                $this->parser->parse('page_template', $data);
            }

public function formverifikasibayar($id){
            $data = array();
            $data['error']      = '';
            $data['toptitle']   = 'Verifikasi Pembayaran Produk';
            $data['title']      = 'Verifikasi Pembayaran Produk';
            $data['content']    = 'Tverifikasi/bayarproduk';
            $data['nbank']      =getwhere('idanggota',$_SESSION['user_id'],'manggota')->row()->namabank;

            $data = array_merge($data, path_variable());
            $this->parser->parse('page_template', $data);
        }

    public function formverifikasiterima(){
                $data = array();
                $data['error']      = '';
                $data['toptitle']   = 'Verifikasi Penerimaan Produk';
                $data['title']      = 'Verifikasi Penerimaan Produk';
                $data['content']    = 'Tverifikasi/terimaproduk';

                $data = array_merge($data, path_variable());
                $this->parser->parse('page_template', $data);
            }

    public function detailtrans($id = null)
    {
        $data = array();
        if ($id !== null) {
            $cek = getrow('idtransaksi', $id, 'tjual');
            if ($cek) {
                $this->parser->parse('Trekapbelanja/detailtrans', array('idtrans' => $id));
            } else {
                echo "";
            }
        } else {
            echo "";
        }
    }

        public function getRiwayatOrder(){
$tahun=$this->input->get('tahun_fil');
$bulan=getABulan($this->input->get('bulan_fil'));
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter=" and `t2`.`periodetransaksi`='".$periode."'";
}else{
$filter="";
}
                $table = 'tjual'; 
                $primaryKey = 'idanggota';

                $columns = array( 
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 0, 'field' => 'idtransaksi' ),
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 1, 'field' => 'idtransaksi', 'formatter' => function( $d, $row ) {
                           return '<a href="javasript:void(0)" class="bolder _500" data-toggle="modal" data-target="#histori-modal">'.$d.'</a>';}),
                    array( 'db' => '`t2`.`tanggaltransaksi`', 'dt' => 2, 'field' => 'tanggaltransaksi', 'formatter' => function( $d, $row ) {
                           return date('d-m-Y',strtotime($d));
                       }),
                    // array('db'  => '(select `namaanggota` from `manggota` where `idanggota`=`t2`.`idanggotapartner`) as namaanggota', 'dt' => 3, 'field' => 'namaanggota'),
                    // array('db'  => '`t2`.`idanggotapartner`', 'dt' => 3, 'field' => 'idanggotapartner', 'formatter' => function( $d, $row ) {
                    //        return getwhere('t1.idanggota',$d,'manggota')->row()->namaanggota;
                    //    }),
                    array('db'  => '`t1`.`namaanggota`', 'dt' => 3, 'field' => 'namaanggota' ),
                    array('db'  => '`t2`.`totalbayar`', 'dt' => 4, 'field' => 'totalbayar', 'formatter' => function( $d, $row ) {
                           return 'Rp.'.number_format($d);
                       }),
                    array('db'  => '`t2`.`statusproses`', 'dt' => 5, 'field' => 'statusproses', 'formatter' => function( $d, $row ) {
                           return statusJualBeli($d);
                       })                
                );
            $sql_details = sql_connect();

            $joinQuery = "FROM $table as `t2` LEFT JOIN `manggota` AS `t1` ON (`t1`.`idanggota` = `t2`.`idanggotapartner`) LEFT JOIN `tjualdetail` AS `t3` ON (`t3`.`idtransaksi` = `t2`.`idtransaksi`)";
            $extraWhere = "`t2`.`idanggota` ='".$_SESSION['user_id']."' $filter  ";
            $groupBy = "`t2`.`idtransaksi`";
            // `t1`.`idanggota` ='".$_SESSION['user_id']."'
            $having = "";
            $ordercus = "order by `t2`.`tanggaltransaksi` asc, `t2`.`statusproses` asc";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }

        public function getROvbayar(){
$tahun=$this->input->get('tahun_fil');
$bulan=getABulan($this->input->get('bulan_fil'));
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter=" and `t2`.`periodetransaksi`='".$periode."'";
}else{
$filter="";
}
                $table = 'tjual'; 
                $primaryKey = 'idanggota';

                $columns = array( 
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 0, 'field' => 'idtransaksi' ),
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 1, 'field' => 'idtransaksi' ),

                    array( 'db' => '`t2`.`tanggaltransaksi`', 'dt' => 2, 'field' => 'tanggaltransaksi', 'formatter' => function( $d, $row ) {
                           return date('d-m-Y',strtotime($d));
                       }),
                    array('db'  => '`t1`.`namaanggota`', 'dt' => 3, 'field' => 'namaanggota' ),
                    array('db'  => '`t2`.`totalbayar`', 'dt' => 4, 'field' => 'totalbayar', 'formatter' => function( $d, $row ) {
                           return 'Rp.'.number_format($d);
                       }),
                    array('db'  => '`t2`.`statusproses`', 'dt' => 5, 'field' => 'statusproses', 'formatter' => function( $d, $row ) {
                           return statusJualBeli($d);
                       }),
                    array('db'  => '`t2`.`idtransaksi`', 'dt' => 6, 'field' => 'idtransaksi', 'formatter' => function( $d, $row ) {

    return '<a href="verifikasi-Bayar/'.encryptURL($d).'" class="btn btn-icon btn-social white" title="Verifikasi Bayar">
    <i class="far fa-edit"></i>
    <i class="far fa-edit indigo"></i>
    </a>';
                       })
                
                );
            $sql_details = sql_connect();

            $joinQuery = "FROM `tjual` as `t2` LEFT JOIN `manggota` AS `t1` ON (`t1`.`idanggota` = `t2`.`idanggotapartner`) LEFT JOIN `tjualdetail` AS `t3` ON (`t3`.`idtransaksi` = `t2`.`idtransaksi`)";
            $extraWhere = "`t2`.`idanggota` ='".$_SESSION['user_id']."'";
            $extraWhere .= "and (`t2`.`statusproses` = '1' $filter)";
            $groupBy = "`t2`.`idtransaksi`";
            $having = "";
            $ordercus = "order by `t2`.`tanggaltransaksi` asc";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }

        public function getROvterima(){
$tahun=$this->input->get('tahun_fil');
$bulan=getABulan($this->input->get('bulan_fil'));
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter=" and `t2`.`periodetransaksi`='".$periode."'";
}else{
$filter="";
}
                $table = 'tjual'; 
                $primaryKey = 'idanggota';

                $columns = array( 
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 0, 'field' => 'idtransaksi' ),
                    array( 'db' => '`t2`.`idtransaksi`', 'dt' => 1, 'field' => 'idtransaksi' ),

                    array( 'db' => '`t2`.`tanggaltransaksi`', 'dt' => 2, 'field' => 'tanggaltransaksi', 'formatter' => function( $d, $row ) {
                           return date('d-m-Y',strtotime($d));
                       }),
                    array('db'  => '`t1`.`namaanggota`', 'dt' => 3, 'field' => 'namaanggota' ),
                    array('db'  => '`t2`.`totalbayar`', 'dt' => 4, 'field' => 'totalbayar', 'formatter' => function( $d, $row ) {
                           return 'Rp.'.number_format($d);
                       }),
                    array('db'  => '`t2`.`statusproses`', 'dt' => 5, 'field' => 'statusproses', 'formatter' => function( $d, $row ) {
                           return statusJualBeli($d);
                       }),
                    array('db'  => '`t2`.`idtransaksi`', 'dt' => 6, 'field' => 'idtransaksi', 'formatter' => function( $d, $row ) {

    return '<a href="verifikasi-Terima/'.encryptURL($d).'" class="btn btn-icon btn-social white" title="Verifikasi Terima">
    <i class="far fa-edit"></i>
    <i class="far fa-edit indigo"></i>
    </a>';
                       })
                
                );
            $sql_details = sql_connect();

            $joinQuery = "FROM `tjual` as `t2` LEFT JOIN `manggota` AS `t1` ON (`t1`.`idanggota` = `t2`.`idanggotapartner`) LEFT JOIN `tjualdetail` AS `t3` ON (`t3`.`idtransaksi` = `t2`.`idtransaksi`)";
            $extraWhere = "`t2`.`idanggota` ='".$_SESSION['user_id']."'";
            $extraWhere .= "and (`t2`.`statusproses` = '4' $filter)";
            $groupBy = "`t2`.`idtransaksi`";
            $having = "";
            $ordercus = "order by `t2`.`tanggaltransaksi` asc";
            echo json_encode(
                SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
            );
        }

public function getSUMperiod($tahun,$bulan){
// $tahun=$this->input->get('tahun_fil');
// $bulan=getABulan($this->input->get('bulan_fil'));
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter=$periode;
}else{
$filter="";
}
$where=array('idanggota'=>$_SESSION['user_id'],'periodetransaksi'=>$filter);
echo periodeSUM($where,'tjual','totalbayar')->sumHarga;
}




}