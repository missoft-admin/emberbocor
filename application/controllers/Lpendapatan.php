<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lpendapatan extends CI_Controller
{

    /**
     * Pendapatan controller.
     * Developer denipurnama371@gmail.com
     */

    public function __construct()
    {
        parent::__construct();
        PermissionUserLoggedIn($this->session);
    		$this->load->model('Pendapatan_Model','pen');
            // $_SESSION['logged_in']=false;
    }

    public function index()
    {
        
    }

    function sponsor(){
                $data = array();
                $data['toptitle']   = 'Pendapatan';
                $data['title']      = 'Pendapatan Sponsor';
                $data['content']    = 'Lpendapatan/Lpendapatansponsor';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('tahun_fil')==null)?date('Y'):$this->input->post('tahun_fil');

                $data = array_merge($data, path_variable());
                $this->parser->parse('page_template', $data);
            }

    function partnership1(){
                $data = array();
                $data['toptitle']   = 'Pendapatan';
                $data['title']      = 'Pendapatan Royalti 1';
                $data['content']    = 'Lpendapatan/Lpendapatanpartnership1';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('tahun_fil')==null)?date('Y'):$this->input->post('tahun_fil');

                $data = array_merge($data, path_variable());
                $this->parser->parse('page_template', $data);
        }

     function partnership2(){
                $data = array();
                $data['toptitle']   = 'Pendapatan';
                $data['title']      = 'Pendapatan Royalti 2';
                $data['content']    = 'Lpendapatan/Lpendapatanpartnership2';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('tahun_fil')==null)?date('Y'):$this->input->post('tahun_fil');

                $data = array_merge($data, path_variable());
                $this->parser->parse('page_template', $data);
     }

     function loyalti(){
                $data = array();
                $data['toptitle']   = 'Pendapatan';
                $data['title']      = 'Pendapatan Loyalti';
                $data['content']    = 'Lpendapatan/Lpendapatanloyalti';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('tahun_fil')==null)?date('Y'):$this->input->post('tahun_fil');

$periode        =implode('', array($data['tahun'],getABulan($data['bulan'])));
$data['periode']=$periode;
$limit=getTPendapat($periode);

    if(count($limit)>0){
$bonusGET=getBonLoyalti($limit->Tpendapat,$periode);
$kPL=getKPL($limit->Tpendapat,$periode);
if(count($kPL)>0 && count($bonusGET)>0){
$data['KP']=$bonusGET->TotalKoefisien;
$data['tKPL']=$kPL->tKPL;
$data['Bloyalti']=$limit->BonusLoyalti;
$data['status']=1;
}else{
$data['status']=0;
    }
}
// $data['status']=$status;
                $data = array_merge($data, path_variable());
                $this->parser->parse('page_template', $data);
     }

     function totalpendapatan(){
                $data = array();
                $data['toptitle']   = 'Total Pendapatan';
                $data['title']      = 'Total Pendapatan';
                $data['content']    = 'Lpendapatan/Ltotalpendapatan';
            $data['bulan']  =($this->input->post('bulan_fil')==null)?getBulanIndo(date('m')):$this->input->post('bulan_fil');
            $data['tahun']  =($this->input->post('tahun_fil')==null)?date('Y'):$this->input->post('tahun_fil');

$data['periode']=implode('', array($data['tahun'],getABulan($data['bulan'])));
$periode=implode('', array($data['tahun'],getABulan($data['bulan'])));
$totalsponsor       =$this->pen->getBonusAnggota($_SESSION['user_id'],1,$periode)->result();
$totalpartnership1  =$this->pen->getBonusAnggota($_SESSION['user_id'],2,$periode)->result();
$totalpartnership2  =$this->pen->getBonusAnggota($_SESSION['user_id'],3,$periode)->result();
// $totalloyalti       =$this->pen->getBonusLoyalti($_SESSION['user_id'],$periode)->result();
if(count($totalsponsor)>0||count($totalpartnership1)>0||count($totalpartnership2)>0){
                $data['totalsponsor']         = $totalsponsor;
                $data['totalpartnership1']    = $totalpartnership1;
                $data['totalpartnership2']    = $totalpartnership2;
$limit=getTPendapat($periode);

    if(count($limit)>0){
$bonusGET=getBonLoyalti($limit->Tpendapat,$periode);

$KP=$bonusGET->TotalKoefisien;
$tKPL=getKPL($limit->Tpendapat,$periode)->tKPL;
$Bloyalti=$limit->BonusLoyalti;
}
$hasil=($KP/$tKPL)*$Bloyalti;
                $data['totalloyalti']         = $hasil;
    $status=1;
    }else{
    $status=0;
    }
$data['status']=$status;
                $data = array_merge($data, path_variable());
                $this->parser->parse('page_template', $data);
}

    public function getPendapatanLoyalti_json()
    {
$tahun=$this->input->get('tahun_fil');
$bulan=getABulan($this->input->get('bulan_fil'));
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter="periodetransaksi='".$periode."'";
}else{
$filter="";
}
                $table = 'koefisienloyalti';
                $primaryKey = 'idanggota';

                $columns = array( 
                    array('db'=>'idanggota','dt'=>0,'field'=>'idanggota'),
                    array('db'=>'idanggota','dt'=>1,'field'=>'idanggota'),
                    array('db'=>'namaanggota','dt'=>2,'field'=>'namaanggota'),

                    array('db'=>'jumlahtransaksi','dt'=>3,'field'=>'jumlahtransaksi'),
                    array('db'=>'koef_jumlahtransaksi','dt'=>4,'field'=>'koef_jumlahtransaksi'),
                    array('db'=>'Tot_harga','dt'=>5,'field'=>'Tot_harga','formatter'=>function( $d, $row ) {
                           return'Rp'.number_format($d);
                       }), 
                    array('db'=>'koef_totaltransaksi','dt'=>6,'field'=>'koef_totaltransaksi'),
                    array('db'=>'TotalKoefisien','dt'=>7,'field'=>'TotalKoefisien')
                
                );
                    $sql_details = sql_connect();

                    $joinQuery = "from (select * from koefisienloyalti limit ".getTPendapat()->Tpendapat.") as koefisienloyalti";
                    $extraWhere = "$filter";
                    $groupBy = "";
                    $having = "";
                    $ordercus = "";
                    echo json_encode(
                        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
                    );
    }

    public function getPendapatanSponsor_json()
    {
$tahun=$this->input->get('tahun_fil');
$bulan=getABulan($this->input->get('bulan_fil'));
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter=" and periodetransaksi='".$periode."'";
}else{
$filter="";
}
                $table = 'manggotabonus';
                $primaryKey = 'idanggota';

                $columns = array( 
                    array( 'db' => 'idanggota', 'dt' => 0, 'field' => 'idanggota' ),
                    array( 'db' => 'idanggota', 'dt' => 7, 'field' => 'idanggota', 'formatter' => function( $d, $row ) {
                           return '';
                       }),
                    array( 'db' => 'idanggota', 'dt' => 1, 'field' => 'idanggota' ),
                    array( 'db' => 'idtransaksi', 'dt' => 2, 'field' => 'idtransaksi' ),

                    array('db'  => 'tipebonus',     'dt' => 3, 'field' => 'tipebonus', 'formatter' => function( $d, $row ) {
                           return tipeBonus($d);
                       }),
                    array('db'  => 'periodetransaksi', 'dt' => 4, 'field' => 'periodetransaksi'),
                    array( 'db' => 'tanggaltransaksi', 'dt' => 5, 'field' => 'tanggaltransaksi', 'formatter' => function( $d, $row ) {
                           return date('d-m-Y',strtotime($d));
                       }), 
                    array('db'  => 'nominalbonus', 'dt' => 6, 'field' => 'nominalbonus', 'formatter' => function( $d, $row ) {
                           return number_format($d);
                       })
                
                );
                    $sql_details = sql_connect();

                    // $this->load->library('Datatables_ssp');
                    $joinQuery = "";
                    $extraWhere = "idanggota ='".$_SESSION['user_id']."' and(tipebonus='1' $filter)";
                    $groupBy = "";
                    $having = "";
                    $ordercus = "order by tanggaltransaksi asc";
                    echo json_encode(
                        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
                    );
    }

    public function getPendapatanPartnership1_json()
    {
$tahun=$this->input->get('tahun_fil');
$bulan=getABulan($this->input->get('bulan_fil'));
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter=" and periodetransaksi='".$periode."'";
}else{
$filter="";
}
                $table = 'manggotabonus';
                $primaryKey = 'idanggota';

                $columns = array( 
                    array( 'db' => 'idanggota', 'dt' => 0, 'field' => 'idanggota' ),
                    array( 'db' => 'idanggota', 'dt' => 7, 'field' => 'idanggota', 'formatter' => function( $d, $row ) {
                           return '';
                       }),
                    array( 'db' => 'idanggota', 'dt' => 1, 'field' => 'idanggota' ),
                    array( 'db' => 'idtransaksi', 'dt' => 2, 'field' => 'idtransaksi' ),

                    array('db'  => 'tipebonus',     'dt' => 3, 'field' => 'tipebonus', 'formatter' => function( $d, $row ) {
                           return tipeBonus($d);
                       }),
                    array('db'  => 'periodetransaksi', 'dt' => 4, 'field' => 'periodetransaksi'),
                    array( 'db' => 'tanggaltransaksi', 'dt' => 5, 'field' => 'tanggaltransaksi', 'formatter' => function( $d, $row ) {
                           return date('d-m-Y',strtotime($d));
                       }), 
                    array('db'  => 'nominalbonus', 'dt' => 6, 'field' => 'nominalbonus', 'formatter' => function( $d, $row ) {
                           return number_format($d);
                       })
                
                );
                    $sql_details = sql_connect();

                    // $this->load->library('Datatables_ssp');
                    $joinQuery = "";
                    $extraWhere = "idanggota ='".$_SESSION['user_id']."' and(tipebonus='2' $filter)";
                    $groupBy = "";
                    $having = "";
                    $ordercus = "order by tanggaltransaksi asc";
                    echo json_encode(
                        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
                    );
    }

    public function getPendapatanPartnership2_json()
    {
$tahun=$this->input->get('tahun_fil');
$bulan=getABulan($this->input->get('bulan_fil'));
$periode=implode('', array($tahun,$bulan));
if($periode){
$filter=" and periodetransaksi='".$periode."'";
}else{
$filter="";
}
                $table = 'manggotabonus';
                $primaryKey = 'idanggota';

                $columns = array( 
                    array( 'db' => 'idanggota', 'dt' => 0, 'field' => 'idanggota' ),
                    array( 'db' => 'idanggota', 'dt' => 7, 'field' => 'idanggota', 'formatter' => function( $d, $row ) {
                           return '';
                       }),
                    array( 'db' => 'idanggota', 'dt' => 1, 'field' => 'idanggota' ),
                    array( 'db' => 'idtransaksi', 'dt' => 2, 'field' => 'idtransaksi' ),

                    array('db'  => 'tipebonus',     'dt' => 3, 'field' => 'tipebonus', 'formatter' => function( $d, $row ) {
                           return tipeBonus($d);
                       }),
                    array('db'  => 'periodetransaksi', 'dt' => 4, 'field' => 'periodetransaksi'),
                    array( 'db' => 'tanggaltransaksi', 'dt' => 5, 'field' => 'tanggaltransaksi', 'formatter' => function( $d, $row ) {
                           return date('d-m-Y',strtotime($d));
                       }), 
                    array('db'  => 'nominalbonus', 'dt' => 6, 'field' => 'nominalbonus', 'formatter' => function( $d, $row ) {
                           return number_format($d);
                       })
                
                );
                    $sql_details = sql_connect();

                    // $this->load->library('Datatables_ssp');
                    $joinQuery = "";
                    $extraWhere = "idanggota ='".$_SESSION['user_id']."' and(tipebonus='3' $filter)";
                    $groupBy = "";
                    $having = "";
                    $ordercus = "order by tanggaltransaksi asc";
                    echo json_encode(
                        SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy, $having, $ordercus )
                    );
    }

    public function getPendapatanLoyalti_json___()
    {
        $this->load->model('datatable_model','datatable');
        $this->tipesql=1;
        $this->id = $_SESSION['user_id'];
        $this->where = 'idanggota';
        $this->table2 = '';
        $this->where2  = '';
        $this->table = 'tbonusloyalti';
        $this->column_search = array('idtransaksi','tanggaltransaksi');
        $this->order = array('idtransaksi' => 'desc');

        $list = $this->datatable->DTwithJoinWhere();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $r->idbonusloyalti;
            $row[] = $r->idtransaksi;
            $row[] = $r->tanggaltransaksi;
            $row[] = $r->periodetransaksi;
            $row[] = $r->idanggotapartner;
            $row[] = $r->idanggota;
            $row[] = number_format($r->bloyalti);
            $row[] = '';
            $data[] = $row;
        } 
        $output = array("draw" => $_POST['draw'],"recordsTotal" => $this->datatable->count_all(),"recordsFiltered" => $this->datatable->count_filtered(),"data" => $data,);
        echo json_encode($output);
    }

    public function getTotalPendapatan_json()
    {
        $this->load->model('datatable_model','datatable');
        $this->tipesql=2;
        $this->select = "";
        $this->id1 = $_SESSION['user_id'];
        $this->id2 = '1';
        $this->id3 = '2';
        $this->id4 = '3';
        $this->where1 = 'manggotabonus.idanggota';
        $this->where2  = 'manggotabonus.tipebonus';
        $this->where3  = '';
        $this->where4  = '';
        // $this->where2  = '';
        $this->table = 'manggotabonus';
        $this->table2 = 'tbonusloyalti';
        $this->join2 = 'manggotabonus.idanggota = tbonusloyalti.idanggota';
        $this->column_search = array('idtransaksi','tanggaltransaksi');
        $this->order = array('idtransaksi' => 'desc');

        $list = $this->datatable->DTwithJoinWhere();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $r) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $r->idtransaksi;
            $row[] = $r->idanggotapartner;
            $row[] = $r->idanggota;
            $row[] = $r->tanggaltransaksi;
            $row[] = $r->periodetransaksi;
            // $row[] = $r->idbonusloyalti;
            $row[] = number_format($r->nominalbonus);
            $row[] = number_format($r->bloyalti);
            // $row[] = 'Rp. '.number_format($r->bloyalti);
            $data[] = $row;
        } 
        $output = array("draw" => $_POST['draw'],"recordsTotal" => $this->datatable->count_all(),"recordsFiltered" => $this->datatable->count_filtered(),"data" => $data,);
        echo json_encode($output);
    }

}