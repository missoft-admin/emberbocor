<?php

function BackendBreadcrum($data)
{
    $result = array();
    foreach ($data as $row) {
        if ($row[1] == '#') {
            $url = '<li><a class="text-muted" href="#">'.$row[0].'</a></li>';
        } elseif ($row[1] == '') {
            $url = '<li><a class="link-effect" href="#">'.$row[0].'</a></li>';
        } else {
            $url = '<li><a class="link-effect" href="'.site_url($row[1]).'">'.$row[0].'</a></li>';
        }
        array_push($result, $url);
    }

    return implode('', $result);
}

function HumanDate($date)
{
    $date = date('j F Y', strtotime($date));

    return $date;
}

function HumanDateMy($date)
{
    $date = date('F Y', strtotime($date));

    return $date;
}

function HumanDateShort($date)
{
    $date = date('d-m-Y', strtotime($date));

    return $date;
}

function HumanDateTime($date)
{
    $date = date('j F Y h:i:s', strtotime($date));

    return $date;
}

function LastLoginDate($date)
{
    if ($date != '' && $date != '0000-00-00 00:00:00') {
        $date = date('j F Y h:i:s', strtotime($date));
    } else {
        $date = 'First Login';
    }

    return $date;
}

function PermissionUserLoggedIn($session)
{
    if (!$session->userdata('logged')) {
        $session->set_flashdata('error', true);
        $session->set_flashdata('message_flash', 'Access Denied');
        redirect('SignIn');
    }
}

function PermissionUserLogin($session)
{
    if ($session->userdata('logged_in')) {
        $session->set_flashdata('error', true);
        $session->set_flashdata('message_flash', 'Access Denied');
        redirect('dashboard');
    }
}

function ErrorSuccess($session)
{
    if ($session->flashdata('error')) {
        return ErrorMessage($session->flashdata('message_flash'));
    } elseif ($session->flashdata('confirm')) {
        return SuccesMessage($session->flashdata('message_flash'));
    } else {
        return '';
    }
}

function ErrorMessage($message)
{
    return '<div class="alert alert-warning alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h3 class="font-w300 push-15">Perhatian!</h3>
        <p>'.$message.'</p>
      </div>';
}

function SuccesMessage($message)
{
    return '<div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h3 class="font-w300 push-15">Berhasil!</h3>
      <p>'.$message.'</p>
    </div>';
}

function TipeIklan($id)
{
    if ($id != '') {
        $data = array(
          '1' => 'Terbesar',
          '2' => 'Sedang',
          '3' => 'Kecil',
          '4' => 'Iklan Baris',
        );

        return $data[$id];
    } else {
        return '';
    }
}

function GetAvatarName($str)
{
    $ret = '';
    foreach (explode(' ', $str) as $word)
        $ret .= strtoupper($word[0]);
    return substr($ret, 0, 2);
}

function GetColorRandom()
{
    $number = rand(0, 500);
    if($number > 0 && $number < 50){
      return 'primary';
    }else if($number > 51 && $number < 100){
      return 'info';
    }else if($number > 100 && $number < 150){
      return 'success';
    }else if($number > 151 && $number < 200){
      return 'warning';
    }else if($number > 201 && $number < 250){
      return 'primary';
    }else if($number > 251 && $number < 300){
      return 'warn';
    }else if($number > 301 && $number < 350){
      return 'dark';
    }else if($number > 351 && $number < 400){
      return 'accent';
    }else{
      return 'danger';
    }
}

function RemoveTitik($number, $delimiter = '.')
{
    return str_replace($delimiter, '', $number);
}

function RemoveComma($number, $delimiter = ',')
{
    return str_replace($delimiter, '', $number);
}

function menuIsActive($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(1) == $menu) ? "class=\"active\"" : "";
}

function menuIsActived($menu)
{
    $CI =& get_instance();
    // explode('/', $m->__url);
    return ($CI->uri->segment(2) == $menu) ? "class=\"active\"" : "";
}

function menuActiveUsers($menu)
{
    $CI =& get_instance();
    return ($CI->uri->segment(4) == $menu) ? "class=\"active\"" : "";
}



function getListFields($table) {
    $ci =& get_instance();
    $fields = $ci->db->list_fields($table);
    foreach($fields as $r) {
        $data[$r] = '';
    }
    return $data;
}

function dropdownAgama($defaultValue='Islam', $attr='class="form-control"') {
    $options = array(
            'Islam'        => 'Islam',
            'Kristen'      => 'Kristen',
            'Katolik'      => 'Katolik',
            'Budha'        => 'Budha',
            'Hindu'        => 'Hindu',
    );

    return form_dropdown('agama', $options, $defaultValue, $attr);
}

function dropdownJenkel($defaultValue='1', $attr='class="form-control"') {
    $options = array(
            '1'     => 'Laki',
            '2'     => 'Perempuan',
    );

    return form_dropdown('jeniskelamin', $options, $defaultValue, $attr);
}

function dropdownStatusPernikahan($defaultValue='1', $attr='class="form-control"') {
    $options = array(
            '0'     => 'Belum Menikah',
            '1'     => 'Menikah',
    );

    return form_dropdown('statuspernikahan', $options, $defaultValue, $attr);
}

function dropdownBank($defaultValue='', $attr='class="form-control"') {
    $options = array(
            ''              => 'Pilih Opsi',
            'BCA'           => 'BCA',
            'Mandiri'       => 'Mandiri',
            'BNI'           => 'BNI',
            'BRI'           => 'BRI',
    );

    return form_dropdown('namabank', $options, $defaultValue, $attr);
}

function dropdownTipeProduk($defaultValue='', $attr='class="form-control"') {
    $options = array(
            ''              => 'Pilih Opsi',
            'Kering'        => 'Kering',
            'Basah'         => 'Basah',
            'Setengah Jadi' => 'Setengah Jadi',
            'Mentah'        => 'Mentah',
    );

    return form_dropdown('tipeproduk', $options, $defaultValue, $attr);
}

function statusMember($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge success text-u-c">Active</span>',
            '0' => '<span class="badge danger text-u-c">Blokir</span>',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function statusAlamat($id) {
    if ($id != '') {
        $data = array(
            '0' => '<span class="badge danger text-u-c text-sm btn">Tidak Aktif</span>',
            '1' => '<span class="badge info text-u-c text-sm btn">Aktif</span>',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function tipeBonus($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge success text-sm text-u-c">Bonus Sponsor</span>',
            '2' => '<span class="badge success text-sm text-u-c">Bonus Royalti 1</span>',
            '3' => '<span class="badge success text-sm text-u-c">Bonus Royalti 2</span>',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function statusJualBeli($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge badge-pill success pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Menunggu"><b class="arrow right b-success pull-in"></b><i class="fas fa-spinner fa-pulse"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Proses"><b class="arrow left b-light pull-in"></b><i class="fas fa-money-bill-alt"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Packing"><b class="arrow right b-light pull-in"></b><i class="fab fa-dropbox"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Pengiriman"><b class="arrow left b-light pull-in"></b><i class="fa fa-car"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai"><b class="arrow right b-light pull-in"></b><i class="fa fa-check"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai Diambil"><b class="arrow left b-light pull-in"></b><i class="fa fa-check"></i></span>',
            '2' => '<span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Menunggu"><b class="arrow right b-light pull-in"></b><i class="fa fa-spinner"></i></span>
            <span class="badge badge-pill success pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Proses"><b class="arrow left b-success pull-in"></b><i class="fa fa-money-bill-alt faa-wrench animated"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Packing"><b class="arrow right b-light pull-in"></b><i class="fab fa-dropbox"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Pengiriman"><b class="arrow left b-light pull-in"></b><i class="fa fa-car"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai"><b class="arrow right b-light pull-in"></b><i class="fa fa-check"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai Diambil"><b class="arrow left b-light pull-in"></b><i class="fa fa-check"></i></span>',
            '3' => '<span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Menunggu"><b class="arrow right b-light pull-in"></b><i class="fa fa-spinner"></i></span>  
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Proses"><b class="arrow left b-light pull-in"></b><i class="fa fa-money-bill-alt"></i></span>  
            <span class="badge badge-pill success pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Packing"><b class="arrow right b-success pull-in"></b><i class="fab fa-dropbox faa-float faa-fast animated"></i></span>  
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Pengiriman"><b class="arrow left b-light pull-in"></b><i class="fa fa-car"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai"><b class="arrow right b-light pull-in"></b><i class="fa fa-check"></i></span> 
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai Diambil"><b class="arrow left b-light pull-in"></b><i class="fa fa-check"></i></span>',
            '4' => '<span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Menunggu"><b class="arrow right b-light pull-in"></b><i class="fa fa-spinner"></i></span>  
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Proses"><b class="arrow left b-light pull-in"></b><i class="fa fa-money-bill-alt"></i></span>  
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Packing"><b class="arrow right b-light pull-in"></b><i class="fab fa-dropbox"></i></span>  
            <span class="badge badge-pill success pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Pengiriman"><b class="arrow left b-success pull-in"></b><i class="fa fa-car faa-burst faa-fast animated"></i></span>  
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai"><b class="arrow right b-light pull-in"></b><i class="fa fa-check"></i></span>  
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai Diambil"><b class="arrow left b-light pull-in"></b><i class="fa fa-check"></i></span>',
            '5' => '<span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Menunggu"><b class="arrow right b-light pull-in"></b><i class="fa fa-spinner"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Proses"><b class="arrow left b-light pull-in"></b><i class="fa fa-money-bill-alt"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Packing"><b class="arrow right b-light pull-in"></b><i class="fab fa-dropbox"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Pengiriman"><b class="arrow left b-light pull-in"></b><i class="fa fa-car"></i></span>
            <span class="badge badge-pill success pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai"><b class="arrow right b-success pull-in"></b><i class="fa fa-check faa-flash animated"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai Diambil"><b class="arrow left b-light pull-in"></b><i class="fa fa-check"></i></span>',
            '6' => '<span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Menunggu"><b class="arrow right b-light pull-in"></b><i class="fa fa-spinner"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Proses"><b class="arrow left b-light pull-in"></b><i class="fa fa-money-bill-alt"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Packing"><b class="arrow right b-light pull-in"></b><i class="fab fa-dropbox"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Pengiriman"><b class="arrow left b-light pull-in"></b><i class="fa fa-car"></i></span>
            <span class="badge badge-pill light pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai"><b class="arrow right b-light pull-in"></b><i class="fa fa-check"></i></span>
            <span class="badge badge-pill success pos-rlt text-sm mr-2" data-toggle="tooltip" data-placement="top" title="Selesai Diambil"><b class="arrow left b-success pull-in"></b><i class="fa fa-check faa-tada animated"></i></span>',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function jenisAnggotaMember($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge grey text-u-c">Guest Member</span>',
            '2' => '<span class="badge warning text-u-c">Premium Member</span>',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function jenisAnggotaPartner($id) {
    if ($id != '') {
        $data = array(
            '1' => '<span class="badge grey text-u-c">Guest Partner</span>',
            '2' => '<span class="badge warning text-u-c">Gold Partner</span>',
            '3' => '<span class="badge success text-u-c">Premium Partner</span>'
        );
        return $data[$id];
    } else {
        return '';
    }
}

function tipeProduk($id) {
    if ($id != '') {
        $data = array(
            '1' => 'Kering',
            '2' => 'Basah',
            '3' => 'Setengah jadi',
            '4' => 'Mentah',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function jenisKelamin($id) {
    if ($id != '') {
        $data = array(
            '1' => 'Laki-laki',
            '2' => 'Perempuan',
        );
        return $data[$id];
    } else {
        return '';
    }
}

function numberIndo($number) {
    if($number) {
        return number_format($number, 2, ',', '.');
    } else {
        return $number;
    }
}

function setFlashMsg($type, $msg, $redir) {
    $ci =& get_instance();
    $ci->session->set_flashdata($type,true);
    $ci->session->set_flashdata('message_flash',$msg);
    redirect($redir,'location');
}

function getHargaJual($hargaProduk) {
    return $hargaProduk+((10/100)*$hargaProduk);
}

function getKeuntungan($hargaProduk) {
    return (10/100)*$hargaProduk;
}

function getPendapatan($hargaProduk) {
    $keuntungan = getKeuntungan($hargaProduk);
    return (45/100)*$keuntungan;
}

function getBSponsor($hargaProduk) {
    $keuntungan = getKeuntungan($hargaProduk);
    return (20/100)*$keuntungan;
}

function getBPartner1($hargaProduk) {
    $keuntungan = getKeuntungan($hargaProduk);
    return (12.5/100)*$keuntungan;    
}

function getBPartner2($hargaProduk) {
    $keuntungan = getKeuntungan($hargaProduk);
    return (7.5/100)*$keuntungan;    
}

function getBLoyalti($hargaProduk) {
    $keuntungan = getKeuntungan($hargaProduk);
    return (15/100)*$keuntungan;    
}

// function Jenis($id){
//  $data = array(
//                 '1' => 'Januari',
//                 '2' => 'Februari'
//         );
//         return $data[$id];
// }

function rTransaksi($id){
 $data = array(
                '1' => 'Buruk Sekali',
                '2' => 'Buruk',
                '3' => 'Cukup',
                '4' => 'Bagus',
                '5' => 'Bagus Sekali'
        );
        return $data[$id];
}

function getBulanIndo($id){
 $data = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'Nopember',
                '12' => 'Desember',
        );
        return $data[$id];
}

function hari_indonesia($id){
$data = array('Monday'  => 'Senin',
    'Tuesday'  => 'Selasa',
    'Wednesday' => 'Rabu',
    'Thursday' => 'Kamis',
    'Friday' => 'Jumat',
    'Saturday' => 'Sabtu',
    'Sunday' => 'Minggu');
        return $data[$id];
}

function getABulan($id){
 $data = array(
                'Januari'   =>  '01',
                'Februari'  =>  '02',
                'Maret'     =>  '03',
                'April'     =>  '04',
                'Mei'       =>  '05',
                'Juni'      =>  '06',
                'Juli'      =>  '07',
                'Agustus'   =>  '08',
                'September' =>  '09',
                'Oktober'   =>  '10',
                'Nopember'  =>  '11',
                'Desember'  =>  '12',
        );
        return $data[$id];
}
function kirimEmail($penjual, $pembeli) {
    $CI=& get_instance();
    $CI->email->set_newline("\r\n");

    $htmlContent = '<h1>Kulcimart.com</h1>';
    $htmlContent .= '<h4>Hai '.$penjual->namaanggota.',</h4>';
    $htmlContent .= "Produk anda sudah diterima oleh ".$pembeli->namaanggota.".";
    $htmlContent .= "<br><img src='https://member.kulcimart.com/assets/images/ceklis.png' width='50px'>";
    $htmlContent .= "<br><a href='https://partner.kulcimart.com/tjual_rekapitulasi/index/5'>cek history</a>";

    $CI->email->to($penjual->email);
    $CI->email->from('demo@kulcimart.com', 'Kulcimart');
    $CI->email->subject('Penerimaan produk kepada pembeli.');
    // $CI->email->attach('https://member.kulcimart.com/assets/images/ceklis.png');
    $CI->email->message($htmlContent);
    $CI->email->send();
  }
function sql_connect(){
 $conn= array(
    'user' => 'kulcimart',
    'pass' => 'kurvasoft@2018',
    'db'   => 'kulcimar_development',
    'host' => '103.29.212.140');
  return $conn;
}

function encryptURL($url){
  $CI=& get_instance();
  $base64 =$CI->encrypt->encode($url);
  $urisafe = strtr($base64, '+/', '-_');
  return $urisafe;
}

function decryptURL($url){
  $CI=& get_instance();
  $urisafe = strtr($url, '-_', '+/');
  $base64 =$CI->encrypt->decode($urisafe);
  return $base64;
}