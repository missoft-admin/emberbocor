<?php
    function getSUM($kondisi,$where,$table,$column){      
        $CI =& get_instance();
        return $CI->db->select('sum(t1.'.$column.') as sumHarga')
                        ->where($kondisi,$where)
                        ->get($table.' t1')->row();
    }
	function periodeSUM($where,$table,$column){      
		$CI =& get_instance();
        return $CI->db->select('sum(t1.'.$column.') as sumHarga')
                        ->where($where)
    					// ->where('periodetransaksi',$datwhere)
                        ->get($table.' t1')->row();
    }

	function getMAX($kondisi,$where,$table,$column){      
		$CI =& get_instance();
        return $CI->db->select('max('.$column.') as maxHarga')
    					->where($kondisi,$where)
                        ->get($table);
    }
function getrow($fieldkey, $fieldval, $tbl){
        $CI =& get_instance();
        $CI->db->where($fieldkey,$fieldval);
        // $CI->db->where('staktif',1);
        $query = $CI->db->get($tbl);
        return $query->row();
    }
    function getwhere($kondisi,$where,$table){      
        $CI =& get_instance();
        return $CI->db->get_where($table.' t1', array($kondisi => $where));
    }

    function TjualDetail($idtrans,$idprod,$tipe,$data=null)
    {
        $CI =& get_instance();
       return $CI->db->where('idtransaksi', $idtrans)
                ->where('idproduk',$idprod)
                ->$tipe('tjualdetail',$data);
    }

    function ReplaceData($where,$data,$table){
      if($this->db->where($where)->replace($table, $data)){
        return true;
      }else{
        $this->error_message = "Penyimpanan Gagal";
        return false;
      }
    }

	function totalRow($where,$table){      
		$CI =& get_instance();
        $CI->db->where($where);
        $query = $CI->db->get($table)->num_rows();
        return $query;
    }

	function getDoubleWhere($kondisi1,$where1,$kondisi2,$where2,$table){      
		$CI =& get_instance();
        return $CI->db->where($kondisi1,$where1)
					  ->where($kondisi2,$where2)
                  	  ->get($table.' t1');
    }

	function getwherejoin($kondisi,$where,$table,$table2,$join){
		$CI =& get_instance();
        return $CI->db->select('*')
    					->where($kondisi,$where)
    					->join($table2,$join)
                        ->get($table.' t1');
    }

    function getData($table){      
		$CI =& get_instance();
        return $CI->db->select('*')
                        ->get($table.' t1');
    }

	function upTblJual($kondisi,$where,$table,$data){      
		$CI =& get_instance();
        return $CI->db->where($kondisi,$where)
			        ->update($table,$data);
    }

    function deleteData($where,$table) {
        $CI =& get_instance();
        return $CI->db->where($where)
                      ->delete($table);  
    }

	function saveData($table,$data) {
		$CI =& get_instance();
		return $CI->db->insert($table, $data);	
	}

    function updateData($where,$data,$table){
        $CI =& get_instance();
        return $CI->db->where($where)
                    ->update($table,$data);
    }

 	function getkode($table,$id,$tipekode){
		$CI =& get_instance();
		$CI->db->select('RIGHT(t1.'.$id.',6) as kode', FALSE)
		  		->order_by($id,'DESC')
		  		->limit(1);
		  $query = $CI->db->get($table.' t1');
		  if($query->num_rows() <> 0){
		   $data =$query->row();      
		   $kode =intval($data->kode) + 1;    
		  }
		  else {
		   $kode =1;
		  }
		  $kodemax = str_pad($kode, 6, "0", STR_PAD_LEFT);
		  $kodejadi = $tipekode.date("ymd").$kodemax;
		  return $kodejadi;  
	}

function getTPendapat($periode){
	$CI =& get_instance();
    return $CI->db->select('*,ceil(TotalPembeliTransaksi*0.005) as Tpendapat')
                    ->from('datapenjualan')
                    ->where('periodetransaksi',$periode)
                    ->get()->row();
}

function getBonLoyalti($limit,$periode){
	$CI =& get_instance();
    return $CI->db->select('*')
                    ->from('koefisienloyalti')
                    ->limit($limit)
                    ->where('idanggota',$_SESSION['user_id'])
                    ->where('periodetransaksi',$periode)
                    ->get()->row();
}

function getKPL($limit,$periode){
	$CI =& get_instance();
    return $CI->db->select('sum(subQ.TotalKoefisien) as tKPL')
                    ->from('(SELECT TotalKoefisien,periodetransaksi
                            FROM koefisienloyalti
                            where periodetransaksi='.$periode.'
                            limit '.$limit.') as subQ')
                    ->group_by('subQ.periodetransaksi')
                    ->get()->row();
}

?>