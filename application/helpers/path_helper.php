<?php

function path_variable()
{
    $CI =& get_instance();
    $data = array(
        'site_url'             => site_url(),
        'base_url'             => base_url(),
        'partner_url'          => 'http://www.partner.kulcimart.com/',
        'partner_assets'       => 'http://www.partner.kulcimart.com/assets/',
        'partner_fotoproduk'   => 'http://www.partner.kulcimart.com/assets/gambarProduk/',
        'partner_foto'   => 'http://www.partner.kulcimart.com/assets/files/anggota/',

        // 'assets_path'          => 'http://cdngue-bb3c.kxcdn.com/assets/',
        // 'css_path'             => 'http://cdngue-bb3c.kxcdn.com/assets/css/',
        // 'libs_path'            => 'http://cdngue-bb3c.kxcdn.com/assets/libs/',
        // 'scripts_path'         => 'http://cdngue-bb3c.kxcdn.com/assets/scripts/',
        // 'files_path'           => 'http://cdngue-bb3c.kxcdn.com/assets/files/',
        'bootstrap_path'            => base_url().'assets/libs/bootstrap/dist/',
        // 'custom_path'          => 'http://cdngue-bb3c.kxcdn.com/assets/custom/',
        // 'toastr_path'          => 'http://cdngue-bb3c.kxcdn.com/assets/custom/toastr/',
        // 'js_croper'            => 'http://cdngue-bb3c.kxcdn.com/assets/imgCroper/js/',
        // 'css_croper'           => 'http://cdngue-bb3c.kxcdn.com/assets/imgCroper/css/',

        'fpmember_path'        => base_url().'assets/fotomember/',
        // 'img_path'             => 'http://cdngue-bb3c.kxcdn.com/assets/images/',
        'img_path'             => base_url().'assets/images/',
        // 'produk_path'          => 'http://cdngue-bb3c.kxcdn.com/assets/files/produk/',
        // 'fpmember_path'        => 'http://cdngue-bb3c.kxcdn.com/assets/fotomember/',

        'assets_path'          => base_url().'assets/',
        'css_path'             => base_url().'assets/css/',
        'libs_path'            => base_url().'assets/libs/',
        'scripts_path'         => base_url().'assets/scripts/',
        'files_path'           => base_url().'assets/files/',
        'produk_path'          => base_url().'assets/files/produk/',
        'custom_path'          => base_url().'assets/custom/',
        'toastr_path'          => base_url().'assets/custom/toastr/',
        'css_croper'           => base_url().'assets/imgCroper/css/',
        'js_croper'            => base_url().'assets/imgCroper/js/',

        'user_id'              => $CI->session->userdata('idanggota'),
        'user_avatar'          => $CI->session->userdata('user_avatar'),
        'username'            => $CI->session->userdata('username'),
        'user_permission'      => $CI->session->userdata('user_permission'),
        'user_last_login'      => HumanDate($CI->session->userdata('user_last_login'))
    );
    return $data;
}
