<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Datatable_model extends CI_Model {

    private function _get_datatables_query()
    {
      if($this->where!==''){
            if($this->tipesql==1){
                     if($this->table2==''){
                            $this->db->where($this->where,$this->id);
                     if($this->where2!==''){
                        $this->db->where($this->where,$this->id)
                                 ->where($this->where2,$this->id2);
                     }
                     }else{
                            $this->db->where($this->where,$this->id)
                                      ->join($this->table2,$this->join);
                 }
               }else
               if($this->tipesql==2){
                $this->db->where($this->where1,$this->id1)
                           ->or_where($this->where2,$this->id2)
                           ->or_where($this->where2,$this->id3)
                           ->or_where($this->where2,$this->id4)
                           ->join($this->table2,$this->join2);
               }
          }
                      $this->db->from($this->table);            
         $i = 0;
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {                
                if($i===0) {
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
            }
            $i++;
        }
    }

    function getwhere($tablenya,$id){
    return  $this->db->get_where($tablenya, array('idanggota' => $id))->row();
    }

    function DTwithJoinWhere() {
        $this->_get_datatables_query();
        if($_POST['length'] != -1) {            
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        return $this->db->get()->result();
    }

    function count_filtered() {
        $this->_get_datatables_query();
        return $this->db->get()->num_rows();
    }
 
    public function count_all() {
        return $this->db->from($this->table)->count_all_results();
    }   

}

/* End of file datatable_model.php */
/* Location: ./application/models/datatable_model.php */
