<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Tverifikasi_model class.
 * 
 * @extends CI_Model
 */
class Tverifikasi_model extends CI_Model {

	public function __construct() {	
		parent::__construct();
		$this->load->database();
	}
    public function showAlju($nourut,$idnya)
    {
               return $this->db->where('stutama', $idnya)
                        ->where('nourut',$nourut)
                        // ->where('staktif',1)
                        ->where('idanggota',$_SESSION['user_id'])
                        ->get('manggotaalamattujuan');
    }

}