<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users_model extends CI_Model
{
    private $table = 'manggota';
    private $tblAlamat = 'manggotaalamattujuan';
    public function __construct()
    {
        parent::__construct();
    }

    public function getInformasi($id) {
        $this->db->where('idanggota', $id);
        return $this->db->get($this->table)->row_array();
    }

    function getDATA($where,$table){      
        return $this->db->select('t1.*')
                        ->where('t1.idanggota',$where)
                        ->get($table.' t1');
    }

    function getDesKecKot($kondisi,$where,$table){      
        return $this->db->select('t1.*')
                        ->where($kondisi,$where)
                        ->get($table.' t1');
    }

    function getProv($table){      
        return $this->db->select('t1.*')
                        // ->where('t1.idanggota',$where)
                        ->get($table.' t1');
    }

    public function checkOldPass($old_password)
    {
        $id = $_SESSION['user_id'];
                $this->db->where('username', $_SESSION['username']);
                $this->db->where('idanggota', $id);
        $this->db->where('passkey', $old_password);
        $query = $this->db->get('manggota');
        if($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    public function getAlamatT7an()
    {
               return $this->db->where('stutama', 0)
                        ->where('idanggota',$_SESSION['user_id'])
                        // ->where('staktif',1)
                        ->order_by('nourut')
                        ->get('manggotaalamattujuan');
    }

    public function showTBLalju()
    {
               return $this->db->where('idanggota',$_SESSION['user_id'])
                        ->order_by('nourut')
                        ->get('manggotaalamattujuan');
    }

    public function showAlju($nourut,$idnya)
    {
               return $this->db->where('stutama', $idnya)
                        ->where('nourut',$nourut)
                        // ->where('staktif',1)
                        ->where('idanggota',$_SESSION['user_id'])
                        ->get('manggotaalamattujuan');
    }

    public function saveNewPass($new_pass)
    {
        $data = array(
               'passkey' => $new_pass
            );
        $this->db->where('idanggota', $_SESSION['user_id']);
        $this->db->update('manggota', $data);
        return true;
    }

    function upDATA($where,$data,$table){
      if($this->db->where($where)->update($table, $data)){
        return true;
      }else{
        $this->error_message = "Penyimpanan Gagal";
        return false;
      }
    }

    function tambahAlamatlain($where,$data,$tblAlamat){
      if($this->db->where($where)->update($tblAlamat, $data)){
        return true;
      }else{
        $this->error_message = "Penyimpanan Gagal";
        return false;
      }
    }

    public function getNOREK()
    {
        return $this->db->select('t1.norekening,
                                  t1.namabank,
                                  t1.cabangbank')
                        ->where('t1.idanggota',$_SESSION['user_id'])
                        ->get('manggota t1');
    }
    function dataStatus($id){
        return $this->db->query("select staktif from manggotaalamattujuan where nourut ='".$id."' and idanggota='".$_SESSION['user_id']."' ")
                            ->row()->staktif;
    }
  function gantiStatus($id){ 
        $status=$this->db->query("select staktif from manggotaalamattujuan where nourut ='".$id."' and idanggota='".$_SESSION['user_id']."' ")->row()->staktif;
    
  if($status==0){$status=1;}else if($status==1){$status=0;}

        $data = array('staktif' => $status);
        return $this->db->where('nourut',$id)
                 ->where('idanggota',$_SESSION['user_id'])
                 ->update('manggotaalamattujuan',$data);
  }

}
