<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendapatan_Model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getBonusAnggota($id,$tipe,$periode) {
        return $this->db->select('SUM(t1.nominalbonus) as Bonus')
                                ->where('t1.idanggota',$id)
                                ->where('t1.periodetransaksi',$periode)
                                ->where('t1.tipebonus',$tipe)
                                ->group_by('t1.idanggota')
                                ->get('manggotabonus t1');
    }

    function getBonusLoyalti($id,$periode) {
        return $this->db->select('SUM(t1.bloyalti) as Bonus')
                                ->where('t1.idanggota',$id)
                                ->where('t1.periodetransaksi',$periode)
                                ->group_by('t1.idanggota')
                                ->get('tbonusloyalti t1');
    }


}