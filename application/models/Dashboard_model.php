<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTBonus()
    {
        $this->db->select('SUM(tjual.bsponsor) AS tbsponsor,
          SUM(tjual.bpartner1) AS tbpartner1,
          SUM(tjual.bpartner2) AS tbpartner2,
          SUM(tjual.bloyalti) AS tbloyalti');
        $this->db->where('tjual.periodetransaksi', date("Ym"));
        $query = $this->db->get('tjual');
        return $query->row();
    }

    function cekidanggota($kondisi,$where,$table){      
        return $this->db->select('*')
                        ->where($kondisi,$where)
                        ->get($table.' t1');
    }

    public function getTmemspon($start,$ord)
    {
        return $this->db->select('t2.namaanggota,t2.*,
                                 Count(t3.idtransaksi) AS totalmemspon')
                        ->join('`tjual` as `t1`','`t2`.`idanggota` = `t1`.`idanggota`')
                        ->join('`tjualdetail` as `t3`','`t3`.`idtransaksi` = `t1`.`idtransaksi`')
                        ->where('`t2`.`idsponsor`', $_SESSION['user_id'])
                        ->where('LEFT(`t2`.`idanggota`,1)', 'B')
                        ->group_by('`t2`.`idanggota`')
                        ->order_by('totalmemspon', $ord)
                        ->limit(5, $start)
                        ->get('`manggota` t2')
                        ->result();
    }


    // function memberSponsorPembelianTerbanyak(){
    //     $query = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual
    //         INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota)
    //         INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi)
    //         WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
    //         GROUP BY tjual.idanggota
    //         ORDER BY totaltransaksi DESC LIMIT 10";

    //     return $this->db->query($query);
    // }

    public function getTpartspon($start,$ord)
    {
        return $this->db->select('t2.namaanggota,t2.*,
                                 Count(t3.idtransaksi) AS totalpartner1,
                                 Count(t3.idtransaksi) AS totalpartner2')
                        ->join('`tjual` as `t1`','`t2`.`idanggota` = `t1`.`idanggota`')
                        ->join('`tjualdetail` as `t3`','`t3`.`idtransaksi` = `t1`.`idtransaksi`')
                        ->where('`t2`.`idsponsor`', $_SESSION['user_id'])
                        ->where('LEFT(`t2`.`idanggota`,1)', 'A')
                        ->group_by('`t2`.`idanggota`')
                        ->order_by('totalpartner1', $ord)
                        ->order_by('totalpartner2', $ord)
                        ->limit(5, $start)
                        ->get('`manggota` t2')
                        ->result();
    }
    function partnerSponsorPenjualanTerbanyak(){
        $query = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual
            INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota)
            INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi)
            WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
            GROUP BY tjual.idanggotapartner
            ORDER BY totaltransaksi DESC LIMIT 10";

        return $this->db->query($query);
    }

    public function getTpartnerFav($start)
    {
        return $this->db->select('manggota.namaanggota,manggota.*,
                                  Count(tjualdetail.idtransaksi) AS totalditransaksi')
                        ->join('manggota','manggota.idanggota=tjual.idanggotapartner')
                        ->join('tjualdetail','tjualdetail.idtransaksi=tjual.idtransaksi')
                        ->where('tjual.idanggota', $_SESSION['user_id'])
                        ->group_by('manggota.idanggota')
                        ->order_by('totalditransaksi', 'desc')
                        ->limit(5, $start)
                        ->get('tjual')
                        ->result();
    }

    public function getTprodFav($start,$ord)
    {
        return $this->db->select('t2.namaproduk,t2.*,
                                  Count(t3.idtransaksi) AS totalbeli,
                                  t3.*')
                        ->join('tjualdetail t3','t3.idtransaksi=t1.idtransaksi')
                        ->join('manggotaproduk t2','t2.idproduk=t3.idproduk')
                        ->where('t1.idanggota', $_SESSION['user_id'])
                        ->group_by('t3.idproduk')
                        ->order_by('totalbeli', $ord)
                        ->limit(5, $start)
                        ->get('tjual t1')
                        ->result();
    }

    function memberSponsorPembelianSedikt(){
        $query = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual
            INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota)
            INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi)
            WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
            GROUP BY tjual.idanggota
            ORDER BY totaltransaksi ASC LIMIT 10";

        return $this->db->query($query);
    }

    // function partnerSponsorPenjualanTerbanyak(){
    //     $query = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual
    //         INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota)
    //         INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi)
    //         WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
    //         GROUP BY tjual.idanggotapartner
    //         ORDER BY totaltransaksi DESC LIMIT 10";

    //     return $this->db->query($query);
    // }

    function partnerSponsorPenjualanSedikit(){
        $query = "SELECT tjual.*, manggota.idsponsor, SUM(tjualdetail.hargajual * tjualdetail.jumlahjual) As totaltransaksi FROM tjual
            INNER JOIN manggota ON(manggota.idanggota=tjual.idanggota)
            INNER JOIN tjualdetail ON(tjualdetail.idtransaksi = tjual.idtransaksi)
            WHERE manggota.idsponsor = '".$this->session->userdata('idanggota')."'
            GROUP BY tjual.idanggotapartner
            ORDER BY totaltransaksi ASC LIMIT 10";

        return $this->db->query($query);
    }
    function getNamaPartner($kondisi,$where,$table){      
        return $this->db->where($kondisi,$where)
                        ->get($table.' t1');
    }


    public function getTransaksiTerbanyak($start)
    {
        $this->db->select('tjual.idanggotapartner,manggota.namaanggota, COUNT(tjual.idanggotapartner) AS totaltransaksi');
        $this->db->join('manggota', 'manggota.idanggota = tjual.idanggotapartner');
        $this->db->where('tjual.idanggota', $_SESSION['user_id']);
        $this->db->order_by('COUNT(tjual.idtransaksi)', 'DESC');
        $this->db->group_by('tjual.idanggota');
        $this->db->limit(5, $start);
        $query = $this->db->get('tjual');
        return $query->result();
    }

}
