<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Lbonusloyalti_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($bulan, $tahun)
    {
        $this->db->select('tjual.idanggota, manggota.namaanggota, SUM(tjual.bloyalti) AS tbloyalti');
        $this->db->join('manggota', 'manggota.idanggota = tjual.idanggota');
        $this->db->order_by('SUM(tjual.bloyalti)', 'DESC');
        $this->db->group_by('tjual.idanggota');
        $this->db->where('tjual.periodetransaksi', $tahun.$bulan);
        $query = $this->db->get('tjual');
        return $query->result();
    }
}
