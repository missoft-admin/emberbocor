<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mgallery_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $this->db->where('sttampil', '1');
        $query = $this->db->get('mgallery');
        return $query->result();
    }

    public function getSpecified($idgallery)
    {
        $this->db->where('idgallery', $idgallery);
        $query = $this->db->get('mgallery');
        return $query->row();
    }

    public function saveData()
		{
				$this->tanggalinput 	= date("Y-m-d");
				$this->judulfoto 	    = $_POST['judulfoto'];
				$this->deskripsi 	    = $_POST['deskripsi'];
				$this->sttampil 		  = 1;
				$this->inputoleh 			= 0;

        if($this->upload()){
				  $this->db->insert('mgallery', $this);
					return true;
				}else{
					$this->error_message = "Penyimpanan Gagal";
					return false;
				}
	  }

	  public function updateData()
		{
        $this->judulfoto 	    = $_POST['judulfoto'];
        $this->deskripsi 	    = $_POST['deskripsi'];
        $this->sttampil 		  = 1;
        $this->inputoleh 			= 0;

        if($this->upload(true)){
				  $this->db->update('mgallery', $this, array('idgallery' => $_POST['idgallery']));
					return true;
				}else{
					$this->error_message = "Penyimpanan Gagal";
					return false;
				}
	  }

    public function softDelete($idgallery)
    {
        $this->sttampil = 0;

        if ($this->db->update('mgallery', $this, array('idgallery' => $idgallery))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }

    function upload($update = false){
  		if(isset($_FILES['foto'])){
  			if($_FILES['foto']['name'] != ''){
  				$config['upload_path'] = './assets/files/gallery/';
          $config['allowed_types'] = 'jpg|jpeg|bmp|tiff|png';
  				$config['encrypt_name']  = TRUE;

  				$this->load->library('upload', $config);

          $this->upload->initialize($config);

  				if ($this->upload->do_upload('foto')){
  					$image_upload = $this->upload->data();
  					$this->foto = $image_upload['file_name'];

  					if($update == true) $this->removeImage($_POST['idgallery']);

  					return true;
  				}else{
            print_r($this->upload->display_errors());exit();
  					$this->error_message = $this->upload->display_errors();
  					return false;
  				}
  			}else{
  				return true;
  			}
  		}else{
  			return true;
  		}

  	}

  	function removeImage($idgallery){
  		$row = $this->getSpecifiedEntries($idgallery);
  		if(file_exists('./assets/files/gallery/'.$row->foto) && $row->foto !='') {
  			unlink('./assets/files/gallery/'.$row->foto);
  		}
  	}
}
