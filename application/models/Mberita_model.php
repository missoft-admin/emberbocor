<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mberita_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll()
    {
        $this->db->where('sttampil', '1');
        $query = $this->db->get('mberita');
        return $query->result();
    }

    public function getSpecified($idberita)
    {
        $this->db->where('idberita', $idberita);
        $query = $this->db->get('mberita');
        return $query->row();
    }

    public function saveData()
		{
				$this->tanggalinput 	= date("Y-m-d");
				$this->tanggaltampil 	= $_POST['tanggaltampil'];
				$this->judulberita 	  = $_POST['judulberita'];
				$this->isiberita 	    = $_POST['isiberita'];
				$this->sttampil 		  = 1;
				$this->inputoleh 			= 0;

				if($this->db->insert('mberita', $this)){
					return true;
				}else{
					$this->error_message = "Penyimpanan Gagal";
					return false;
				}
	  }

	  public function updateData()
		{
				$this->tanggaltampil 	= $_POST['tanggaltampil'];
				$this->judulberita 	  = $_POST['judulberita'];
				$this->isiberita 	    = $_POST['isiberita'];
				$this->sttampil 		  = 1;

				if($this->db->update('mberita', $this, array('idberita' => $_POST['idberita']))){
					return true;
				}else{
					$this->error_message = "Penyimpanan Gagal";
					return false;
				}
	  }

    public function softDelete($idberita)
    {
        $this->sttampil = 0;

        if ($this->db->update('mberita', $this, array('idberita' => $idberita))) {
            return true;
        } else {
            $this->error_message = "Penyimpanan Gagal";
            return false;
        }
    }
}
