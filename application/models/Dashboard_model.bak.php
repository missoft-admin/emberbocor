<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getTBonus()
    {
        $this->db->select('SUM(tjual.bsponsor) AS tbsponsor,
          SUM(tjual.bpartner1) AS tbpartner1,
          SUM(tjual.bpartner2) AS tbpartner2,
          SUM(tjual.bloyalti) AS tbloyalti');
        $this->db->where('tjual.periodetransaksi', date("Ym"));
        $query = $this->db->get('tjual');
        return $query->row();
    }

    function cekidanggota($kondisi,$where,$table){      
        return $this->db->select('*')
                        ->where($kondisi,$where)
                        ->get($table.' t1');
    }

    public function getTmemspon($start,$ord,$periode)
    {
        return $this->db->select('manggota.namaanggota,manggota.*,
                                 Count(tjual.idtransaksi) AS totalmemspon')
                        ->join('manggota','manggota.idanggota = tjual.idmemberbsponsor')
                        ->where('tjual.idanggota', $_SESSION['user_id'])
                        ->where('tjual.periodetransaksi', $periode)
                        ->group_by('manggota.namaanggota')
                        ->order_by('totalmemspon', $ord)
                        ->limit(5, $start)
                        ->get('tjual')
                        ->result();
    }

    public function getTpartspon($start,$ord,$periode)
    {
        return $this->db->select('manggota.namaanggota,manggota.*,
                                 Count(tjual.idtransaksi) AS totalpartner1,
                                 Count(tjual.idtransaksi) AS totalpartner2')
                        ->join('manggota','manggota.idanggota = tjual.idmemberbpartner1 AND manggota.idanggota = tjual.idmemberbpartner2')
                        ->where('tjual.idanggota', $_SESSION['user_id'])
                        ->where('tjual.periodetransaksi', $periode)
                        ->group_by('manggota.namaanggota')
                        ->order_by('totalpartner1', $ord)
                        ->order_by('totalpartner2', $ord)
                        ->limit(5, $start)
                        ->get('tjual')
                        ->result();
    }

    public function getTpartnerFav($start,$periode)
    {
        return $this->db->select('manggota.namaanggota,manggota.*,
                                  Count(tjual.idtransaksi) AS totalditransaksi')
                        ->join('manggota','manggota.idanggota=tjual.idanggotapartner')
                        ->where('tjual.idanggota', $_SESSION['user_id'])
                        ->where('tjual.periodetransaksi', $periode)
                        ->group_by('manggota.namaanggota')
                        ->order_by('totalditransaksi', 'desc')
                        ->limit(5, $start)
                        ->get('tjual')
                        ->result();
    }

    public function getTprodFav($start,$ord,$periode)
    {
        return $this->db->select('manggotaproduk.namaproduk,manggotaproduk.*,
                                  Count(tjual.idtransaksi) AS totalbeli')
                        ->join('manggotaproduk','manggotaproduk.idproduk=tjual.idproduk')
                        ->where('tjual.idanggota', $_SESSION['user_id'])
                        ->where('tjual.periodetransaksi', $periode)
                        ->group_by('manggotaproduk.namaproduk')
                        ->order_by('totalbeli', $ord)
                        ->limit(5, $start)
                        ->get('tjual')
                        ->result();
    }

    function getNamaPartner($kondisi,$where,$table){      
        return $this->db->where($kondisi,$where)
                        ->get($table.' t1');
    }


    public function getTransaksiTerbanyak($start)
    {
        $this->db->select('tjual.idanggotapartner,manggota.namaanggota, COUNT(tjual.idanggotapartner) AS totaltransaksi');
        $this->db->join('manggota', 'manggota.idanggota = tjual.idanggotapartner');
        $this->db->where('tjual.idanggota', $_SESSION['user_id']);
        $this->db->order_by('COUNT(tjual.idtransaksi)', 'DESC');
        $this->db->group_by('tjual.idanggota');
        $this->db->limit(5, $start);
        $query = $this->db->get('tjual');
        return $query->result();
    }

}
