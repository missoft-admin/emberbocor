<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('database',
							'session',
							'parser',
							'upload',
							'encryption',
							'encrypt',
							'form_validation',
							'email',
							// 'Datatables',
							'SSP'
						);
$autoload['helper'] = array('url',
							'form',
							'html',
							'text',
							'path',
							'dbfunc',
							'main'
						);
$autoload['drivers'] = array();
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array();
