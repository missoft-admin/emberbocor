<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'dashboard';

//link authentication
$route['SignUp'] 		= 'auth/signup/$1';
$route['SignIn'] 		= 'auth/signin/$1';
$route['Forgot'] 		= 'auth/forgot_password/$1';
	//link proses authentication
	$route['signup_proses'] = 'auth/proses_signup/$1';
	$route['signin_proses'] = 'auth/do_login/$1';
	$route['signout'] 		= 'auth/do_logout/$1';

//link dashboard
$route['riwayat-Order'] 	= 'Dashboard/getRiwayat/$1';
$route['riwayat-Orderan'] 	= 'Dashboard/getRiwayatOrder/$1';

//link riwayat belanja
$route['r/rekap-Belanja'] 		= 'Trekapbelanja/historibelanja/$1';
$route['r/verifikasi-Bayar'] 	= 'Trekapbelanja/verifikasipembayaran/$1';
$route['r/verifikasi-Terima'] 	= 'Trekapbelanja/verifikasipenerimaan/$1';
	//get data riwayat Belanja
	$route['r/get-rekap-Belanja'] 	= 'Trekapbelanja/getRiwayatOrder/$1';
	$route['r/get-rekap-Belanja/detailtrans/(:any)'] 	= 'Trekapbelanja/detailtrans/$1';
	$route['r/get-rebel-vbayar'] 	= 'Trekapbelanja/getROvbayar/$1';
	$route['r/get-rebel-vterima'] 	= 'Trekapbelanja/getROvterima/$1';
//form input data verifikasi
$route['r/verifikasi-Bayar/(:any)'] 	= 'Trekapbelanja/formverifikasibayar/$1';
$route['r/verifikasi-Terima/(:any)'] 	= 'Trekapbelanja/formverifikasiterima/$1';

//link Verifikasi
$route['v/verifikasi-Pendaftaran'] 		= 'Tverifikasi/verifikasiBayarPendaftaran/$1';
$route['d/verifikasi-Bayar'] 			= 'Tverifikasi/getJENAL/$1';
	//link proses verifikasi
	$route['v/verifikasi-Pendaftaran/proses'] 	= 'Tverifikasi/prosesInputBayarPendaftaran/$1';
	$route['v/verifikasi-Bayar/proses'] 	= 'Tverifikasi/prosesInputBayarProduk/$1';
	$route['v/verifikasi-Bayar/gantiEkspedisi'] 	= 'Tverifikasi/gantiEkspedisi/$1';
	$route['v/verifikasi-Bayar/BHpesanan'] 	= 'Tverifikasi/BHpesanan/$1';
	$route['v/verifikasi-Terima/proses'] 	= 'Tverifikasi/prosesInputTerimaProduk/$1';
	// $route['v/verifikasi-Pendaftaran'] = 'Tverifikasi/verifikasiBayarPendaftaran/$1';
	// $route['v/verifikasi-Pendaftaran'] = 'Tverifikasi/verifikasiBayarPendaftaran/$1';

//link pendapatan
$route['p/sponsor'] 				= 'Lpendapatan/sponsor/$1';
$route['p/royalti1'] 			= 'Lpendapatan/partnership1/$1';
$route['p/royalti2'] 			= 'Lpendapatan/partnership2/$1';
$route['p/loyalti'] 				= 'Lpendapatan/loyalti/$1';
	//link get data pendapatan
	$route['p/pendapatan-Sponsor'] 		= 'Lpendapatan/getPendapatanSponsor_json/$1';
	$route['p/pendapatan-Partnership1'] = 'Lpendapatan/getPendapatanPartnership1_json/$1';
	$route['p/pendapatan-Partnership2'] = 'Lpendapatan/getPendapatanPartnership2_json/$1';
	$route['p/pendapatan-Loyalti'] 		= 'Lpendapatan/getPendapatanLoyalti_json/$1';
	$route['p/totalcashback'] 		= 'Lpendapatan/totalpendapatan/$1';

// $route['p/Total-pendapatan'] = 'Lpendapatan/getTotalPendapatan_json/$1';

//link users
$route['u/users/settings/datapribadi'] 	= 'MusersSetting/tampildatapribadi/$1';
$route['u/users/settings/datarekening'] = 'MusersSetting/tampildatarekening/$1';
$route['u/users/settings/datapassword'] = 'MusersSetting/editdatapassword/$1';
$route['u/users/settings/anggotarekomendasi'] = 'MusersSetting/tampilpetajaringan/$1';
	//link proses database users
	$route['u/users/newAlamat'] 		= 'MusersSetting/saveEditAlamatBaru/$1';
	$route['s/users/upDATA'] 		= 'MusersSetting/upDATA/$1';
	$route['u/users/changepwd'] 	= 'MusersSetting/changePWD/$1';
	$route['u/users/changeSTal'] 	= 'MusersSetting/gantiStatAl/$1';
		//link get data users
		$route['u/users/GETpenjual'] 	= 'MusersSetting/getPetaJaringanPenjual/$1';
		$route['u/users/GETpembeli'] 	= 'MusersSetting/getPetaJaringanPembeli/$1';
		$route['u/users/GETprofil'] 	= 'MusersSetting/getPROFIL/$1';
		$route['u/users/GETrek'] 		= 'MusersSetting/getREK/$1';
		$route['u/users/GETprovinsi'] 	= 'MusersSetting/getPROV/$1';
		$route['u/users/GETkota'] 		= 'MusersSetting/getKOTA/$1';
		$route['u/users/GETkecamatan'] 	= 'MusersSetting/getKEC/$1';
		$route['u/users/GETdesa'] 		= 'MusersSetting/getDESA/$1';
		$route['u/users/GETkopos'] 		= 'MusersSetting/getKOPOS/$1';
		$route['u/users/GETselectal'] 	= 'MusersSetting/selectJenal/$1';
		$route['u/users/GETjenal'] 		= 'MusersSetting/getJENAL/$1';

$route['404_override'] = 'notfound';
$route['translate_uri_dashes'] = FALSE;
